# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from ckeditor.widgets import CKEditorWidget
from django import forms

# from django.apps import apps
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.db import transaction

# from django.forms.widgets import RadioFieldRenderer
try:
    from django.utils.encoding import python_2_unicode_compatible
except ImportError:
    from django.utils.six import python_2_unicode_compatible
from django.utils.html import format_html, html_safe
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.utils import formats
from django.forms.utils import flatatt
from ckeditor.fields import RichTextFormField as RichTextFormFieldCore
from phonenumber_field.formfields import PhoneNumberField
from phonenumber_field.phonenumber import to_python


from ..models import FormSubmit, EmailLog, FormEntry, FieldEntry, Choice
from ..utils import get_submitter
from ..models.widget import (
    CustomRadioWidget,
    RadioChoiceInput,
    MultipleChoiceWithInputWidget,
    DynamicChoiceWidget,
)

# FormSubmit = apps.get_model('form_builder', 'FormSubmit')
# EmailLog = apps.get_model('form_builder', 'EmailLog')
# FormEntry = apps.get_model('form_builder', 'FormEntry')
# FieldEntry = apps.get_model('form_builder', 'FieldEntry')


@html_safe
@python_2_unicode_compatible
class ChoiceFieldRenderer(object):
    """
    An object used by RadioSelect to enable customization of radio widgets.
    """

    choice_input_class = None
    outer_html = "<ul{id_attr}>{content}</ul>"
    inner_html = "<li>{choice_value}{sub_widgets}</li>"

    def __init__(self, name, value, attrs, choices):
        self.name = name
        self.value = value
        self.attrs = attrs
        self.choices = choices

    def __getitem__(self, idx):
        choice = self.choices[idx]  # Let the IndexError propagate
        return self.choice_input_class(
            self.name, self.value, self.attrs.copy(), choice, idx
        )

    def __str__(self):
        return self.render()

    def render(self):
        """
        Outputs a <ul> for this set of choice fields.
        If an id was given to the field, it is applied to the <ul> (each
        item in the list will get an id of `$id_$i`).
        """
        id_ = self.attrs.get("id")
        output = []
        for i, choice in enumerate(self.choices):
            choice_value, choice_label = choice
            if isinstance(choice_label, (tuple, list)):
                attrs_plus = self.attrs.copy()
                if id_:
                    attrs_plus["id"] += "_{}".format(i)
                sub_ul_renderer = self.__class__(
                    name=self.name,
                    value=self.value,
                    attrs=attrs_plus,
                    choices=choice_label,
                )
                sub_ul_renderer.choice_input_class = self.choice_input_class
                output.append(
                    format_html(
                        self.inner_html,
                        choice_value=choice_value,
                        sub_widgets=sub_ul_renderer.render(),
                    )
                )
            else:
                w = self.choice_input_class(
                    self.name, self.value, self.attrs.copy(), choice, i
                )
                output.append(
                    format_html(
                        self.inner_html, choice_value=force_text(w), sub_widgets=""
                    )
                )
        return format_html(
            self.outer_html,
            id_attr=format_html(' id="{}"', id_) if id_ else "",
            content=mark_safe("\n".join(output)),
        )


class RadioFieldRenderer(ChoiceFieldRenderer):
    choice_input_class = RadioChoiceInput


class RestFormClass(forms.Form):
    """
    Used to instantiate a RestFormClass from form_builder.models.form.FormModel
    """

    def __init__(self, *arg, **kwarg):
        super(RestFormClass, self).__init__(*arg, **kwarg)
        self.empty_permitted = False

    def clean(self):
        cleaned_data = super().clean()
        file_names = []
        file_fields_list = list(
            filter(lambda name: "_files" in name, self.request.POST.keys())
        )
        if file_fields_list:
            for file_field_name in file_fields_list:
                files_id = [
                    int(file_id)
                    for file_id in self.request.POST.get(file_field_name).split(",")
                ]
            file_names = [
                o.field_name for o in FieldEntry.objects.filter(id__in=files_id)
            ]

        all_fields_values = []
        for key, value in self.request.POST.items():
            all_fields_values.append(value)
        for field_name, a_field in self.fields.items():
            if self.form_model.fields.filter(slug=field_name).exists():
                hidden_by_default = (
                    self.form_model.fields.filter(slug=field_name)
                    .first()
                    .hidden_by_default
                )
                if not hidden_by_default and (
                    a_field.required
                    or (
                        hasattr(a_field.widget, "attrs")
                        and "initial_required" in a_field.widget.attrs
                        and a_field.widget.attrs["initial_required"]
                    )
                ):
                    if not field_name in file_names and (
                        field_name not in cleaned_data or not cleaned_data[field_name]
                    ):
                        self.add_error(field_name, _("This field is required"))
                elif hidden_by_default and (
                    a_field.required
                    or (
                        hasattr(a_field.widget, "attrs")
                        and "initial_required" in a_field.widget.attrs
                        and a_field.widget.attrs["initial_required"]
                    )
                ):
                    required = False
                    for a_choice in Choice.objects.filter(
                        trigger_field__slug=field_name
                    ).values_list("id", flat=True):
                        required = True if str(a_choice) in all_fields_values else False
                        if required:
                            break
                    if (
                        required
                        and not field_name in file_names
                        and (
                            field_name not in cleaned_data
                            or not cleaned_data[field_name]
                        )
                    ):
                        self.add_error(field_name, _("This field is required"))
        return cleaned_data

    def save_field_entries(self, form_entry):
        form_fields = self.form_model.get_fields().non_files()
        form_fields = form_fields.filter(slug__in=self.cleaned_data.keys())
        field_entries = [
            FieldEntry(
                form=form_entry.form,
                entry=form_entry,
                form_field=form_field,
                field_name=form_field.slug,
                value=self.cleaned_data.get(form_field.slug)
                if self.cleaned_data.get(form_field.slug)
                or self.cleaned_data.get(form_field.slug) == 0
                else "NULL",
                submitter=get_submitter(self.request),
                token=form_entry.token,
            )
            for form_field in form_fields
        ]

        FieldEntry.objects.bulk_create(field_entries)

    def save_file_entries(self, form_entry, files):
        if files:
            file_field_ids = (
                self.form_model.get_fields().files().values_list("pk", flat=True)
            )
            file_entries = FieldEntry.objects.filter(
                id__in=[int(file) for file in files.split(",")],
            ).update(entry=form_entry)

    @transaction.atomic
    def save(self, parent=None, files=None, update=False):
        submitter = get_submitter(self.request)
        # if update and parent:
        #     form_entry = parent
        #     form_entry.request = self.request
        # elif update and not parent:

        if update and not parent:
            form_entry = FormEntry.objects.get(
                submitter=submitter, token=self.token, form=self.form_model,
            )
            form_entry.request = self.request
            # Removes old field entries
            form_entry.field_entries.fields().delete()
            # FormEntry.objects.filter(
            #     parent=form_entry,
            #     submitter=submitter,
            #     token=self.token,
            # ).delete()
        else:
            form_entry = FormEntry.objects.create_for_form(
                form=self.form_model,
                submitter=submitter,
                token=self.token,
                request=self.request,
            )
        if parent:
            form_entry.parent = parent
            form_entry.save()
        else:
            if self.form_model.is_root():
                form_entry.parent = form_entry
                form_entry.save()
            else:
                try:
                    form_entry.parent = FormEntry.objects.filter(
                        submitter=submitter,
                        token=self.token,
                        form=self.form_model.get_root(),
                    ).first()
                    form_entry.save()
                except Exception as e:
                    print(e)

        self.save_field_entries(form_entry)
        self.save_file_entries(form_entry, files)

        # if it is last form(Which doesn't have next form) We need to create entry in form submit
        next_form = self.form_model.get_next(submitter=submitter, token=self.token)
        if not next_form:
            form_entries = FormEntry.objects.filter(token=self.token)
            if form_entries.filter(form_submit__isnull=True).exists():
                form_submit = FormSubmit.objects.create(
                    submitted=True, submitter_user=submitter, form=self.form_model
                )
                form_entries = FormEntry.objects.filter(
                    token=self.token, submitter=submitter, form_submit__isnull=True
                ).update(form_submit=form_submit)
                # form_submit.after_submit(self.form_model, request)
            # else:
            #     form_submit = form_entry.first().form_submit

        return form_entry


class FileUploadForm(forms.ModelForm):
    class Meta:
        model = FieldEntry
        fields = ("field_name", "file")

    def clean_field(self):
        entry = self.cleaned_data.get("entry")
        files = entry.form.get_fields().files()
        return files.filter(
            form_field__slug=self.cleaned_data.get("field_name")
        ).first()


class RadioChoiceInputWithClass(RadioChoiceInput):
    def __init__(self, name, value, attrs, choice, index):
        trigger = choice[2]
        if "class" in attrs and trigger:
            attrs["class"] += " other-choice-trigger"
        elif trigger:
            attrs["class"] = "other-choice-trigger"
        super(RadioChoiceInputWithClass, self).__init__(
            name, value, attrs, choice, index
        )


class RadioFieldRendererWithClass(RadioFieldRenderer):
    choice_input_class = RadioChoiceInputWithClass

    def render(self):
        """
        Outputs a <ul> for this set of choice fields.
        If an id was given to the field, it is applied to the <ul> (each
        item in the list will get an id of `$id_$i`).
        """
        id_ = self.attrs.get("id")
        output = []
        for i, choice in enumerate(self.choices):
            choice_value, choice_label = choice[0], choice[1]
            if isinstance(choice_label, (tuple, list)):
                attrs_plus = self.attrs.copy()
                if id_:
                    attrs_plus["id"] += "_{}".format(i)
                sub_ul_renderer = self.__class__(
                    name=self.name,
                    value=self.value,
                    attrs=attrs_plus,
                    choices=choice_label,
                )
                sub_ul_renderer.choice_input_class = self.choice_input_class
                output.append(
                    format_html(
                        self.inner_html,
                        choice_value=choice_value,
                        sub_widgets=sub_ul_renderer.render(),
                    )
                )
            else:
                w = self.choice_input_class(
                    self.name, self.value, self.attrs.copy(), choice, i
                )
                output.append(
                    format_html(
                        self.inner_html, choice_value=force_text(w), sub_widgets=""
                    )
                )
        return format_html(
            self.outer_html,
            id_attr=format_html(' id="{}"', id_) if id_ else "",
            content=mark_safe("\n".join(output)),
        )


class ChoiceWithOtherWidget(forms.MultiWidget):
    """MultiWidget for use with ChoiceWithOtherField."""

    def __init__(self, choices):
        widgets = [
            # forms.RadioSelect(
            CustomRadioWidget(
                renderer=RadioFieldRendererWithClass,
                choices=choices,
                attrs={"class": "other-choice-button"},
            ),
            forms.TextInput(
                attrs={"class": "inputbox other-choice-text", "style": "display: none;"}
            ),
        ]
        super(ChoiceWithOtherWidget, self).__init__(widgets)

    def decompress(self, value):
        if not value:
            return [None, None]
        return value

    def format_output(self, rendered_widgets):
        return rendered_widgets[0] + "<br>" + rendered_widgets[1]


class ChoiceWithOtherField(forms.MultiValueField):
    """
    ChoiceField with an option for a user-submitted "other" value.
    """

    def __init__(self, *args, **kwargs):
        self.triggers = []
        choices_before = kwargs["choices"]
        choices_after = []
        for choice in choices_before:
            if choice[2]:
                self.triggers.append(str(choice[0]))
            choices_after.append((choice[0], choice[1]))
        kwargs["choices"] = choices_after
        fields = [
            forms.ChoiceField(widget=forms.RadioSelect(), *args, **kwargs),
            forms.CharField(required=False),
        ]
        widget = ChoiceWithOtherWidget(choices=choices_before)
        kwargs.pop("choices")
        self._was_required = kwargs.pop("required", True)
        kwargs[
            "required"
        ] = False  # Disable this to show the user that this field is required if it is
        super(ChoiceWithOtherField, self).__init__(
            widget=widget, fields=fields, *args, **kwargs
        )

    def compress(self, value):
        if self._was_required and (
            not value
            or value[0] in (None, "")
            or (value[0] in self.triggers and not value[1])
        ):
            raise forms.ValidationError(self.error_messages["required"])
        if not value:
            return ""
        if all(value):
            return "; ".join(value)
        else:
            return value[0]


class RadioChoiceField(forms.ChoiceField):
    pass


class EmailEditForm(forms.ModelForm):
    email_subject = forms.CharField(max_length=255)
    email_editor = forms.CharField(
        widget=CKEditorWidget(), label="_(Write your email in this editor"
    )

    class Meta:
        model = FormSubmit
        fields = "__all__"

    def clean_email_editor(self):
        User = get_user_model()
        email_editor = self.cleaned_data["email_editor"]
        email_subject = self.cleaned_data["email_subject"]
        submitter_id = self.instance.submitter.id
        try:
            submitter_obj = User.objects.get(id=submitter_id)
        except User.DoesNotExist:
            raise forms.ValidationError(_("User doesn't exist"))
        EmailLog.objects.create(
            user=submitter_obj, subject=email_subject, body_html=email_editor
        )
        send_mail(
            email_subject,
            email_editor,
            settings.EMAIL_HOST_USER,
            [submitter_obj.email],
            fail_silently=True,
        )
        return email_editor


class SmileFaceRatingFieldWidget(forms.Widget):
    input_type = "smile_Face_rating"


class SmileFaceRatingField(forms.Field):
    def __init__(self, **kwargs):
        widget = SmileFaceRatingFieldWidget()
        super(SmileFaceRatingField, self).__init__(
            widget=widget, label=kwargs.get("label", "")
        )


class StarsRatingFieldWidget(forms.Widget):
    input_type = "stars_rating"


class StarsRatingField(forms.Field):
    def __init__(self, **kwargs):
        widget = StarsRatingFieldWidget()
        super(StarsRatingField, self).__init__(
            widget=widget, label=kwargs.get("label", "")
        )


class KwPhoneNumberField(PhoneNumberField):
    def to_python(self, value):
        if value:
            value = value if value.startswith("+") else "+965" + value
            phone_number = to_python(value)
            if phone_number and not phone_number.is_valid():
                raise ValidationError(self.error_messages["invalid"])
            return phone_number
        else:
            return value


class MultipleChoiceWithInputField(forms.MultipleChoiceField):
    """
    ChoiceField with an option for a user-submitted "other" value.
    """

    def __init__(self, *args, **kwargs):
        self.triggers = []
        choices_before = kwargs["choices"]
        choices_after = []
        for choice in choices_before:
            if choice[2]:
                self.triggers.append(choice[0])
            choices_after.append((choice[0], choice[1]))
        kwargs["choices"] = choices_after
        widget = MultipleChoiceWithInputWidget(choices=choices_before)
        # kwargs.pop('choices')
        self._was_required = kwargs.pop("required", True)
        kwargs[
            "required"
        ] = False  # Disable this to show the user that this field is required if it is
        super(MultipleChoiceWithInputField, self).__init__(
            widget=widget, *args, **kwargs
        )


class TextAreaField(forms.CharField):
    widget = forms.Textarea


class RichTextField(forms.CharField):
    from ckeditor.widgets import CKEditorWidget

    widget = CKEditorWidget


class DynamicChoice(forms.ChoiceField):
    def __init__(self, *args, **kwargs):
        widget = DynamicChoiceWidget()
        super(DynamicChoice, self).__init__(widget=widget, *args, **kwargs)
