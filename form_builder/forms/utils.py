# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from collections import OrderedDict

from .forms import RestFormClass


def form_factory(
    form_model, token, request, update=False, is_formset=False, form_class=RestFormClass
):
    """
    Creates a django.forms.Form class from form_builder.models.form.FormModel
    instance.
    """

    def _get_declared_fields(fields):
        fields = [
            # (field_name, field_class)
            (
                form_field.slug,
                form_field.instantiate_field(
                    token, request, update, is_formset, form_model
                ),
            )
            for form_field in fields
        ]
        try:
            # Old Django uses the cretion_counter to older the fields here and we pass fields already orders
            # to the form_class
            fields.sort(key=lambda x: x[1].creation_counter)
        except:
            # If this is faile, then we are using new Django where the att creation_counter doesn't exists
            pass
        return OrderedDict(fields)

    base = type(
        str("Base"),
        (object,),
        {
            "base_fields": _get_declared_fields(form_model.get_fields()),
            "fields": _get_declared_fields(
                form_model.get_fields()
            ),  # new Django needs fields attr to form_class
            "declared_fields": _get_declared_fields(form_model.get_fields()),
            "form_model": form_model,
            "token": token,
            "request": request,
        },
    )

    form_class = type(
        str("%sForm" % form_model.slug),  # TODO: Update class name
        (base, form_class),
        {},
    )
    return form_class
