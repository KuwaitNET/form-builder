from importlib import import_module

from form_builder.settings import FORM_FACTORY_MODULE
from .forms import FileUploadForm


form_factory = getattr(import_module(FORM_FACTORY_MODULE), "form_factory")
