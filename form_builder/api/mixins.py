# -*- coding: utf-8 -*-

from __future__ import unicode_literals


class FormURLMixin(object):
    lookup_url_kwarg = "path"
    lookup_field = "url_path"


class FormEntryMixin(object):
    def get_serializer_class(self):
        return self.form.opts.serializer_class

    def get_serializer_context(self):
        ctx = super(FormEntryMixin, self).get_serializer_context()
        ctx.update({"form": self.form})
        return ctx
