# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from rest_framework.permissions import BasePermission


class CanCreate(BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.can_create(request.user)
