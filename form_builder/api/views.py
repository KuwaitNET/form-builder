# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import transaction

from rest_framework import mixins, viewsets
from rest_framework.response import Response
from rest_framework import renderers
from rest_framework import status

from form_builder.api.mixins import FormURLMixin, FormEntryMixin

# from form_builder.models.form import FormModel
# from form_builder.models.entry import FieldEntryModel
from form_builder.models import FieldEntry as FieldEntryModel, Form as FormModel
from form_builder.serializers import FormDefinitionSerializer, FileFieldEntrySerializer
from form_builder.api.permissions import CanCreate


class FormDefinitionViewSet(
    mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet
):
    serializer_class = FormDefinitionSerializer

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return FormModel.objects.active()
        else:
            return FormModel.objects.public()


class CreateEntryView(
    FormURLMixin, FormEntryMixin, mixins.CreateModelMixin, viewsets.GenericViewSet
):
    """
    Handles a form submit

    FIELD - field ID
    value - user value
    """

    renderer_classes = (renderers.JSONRenderer,)
    permission_classes = (CanCreate,)

    def get_queryset(self):
        return FormModel.objects.get_for_request(self.request)

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        self.form = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.response_payload, status=status.HTTP_201_CREATED, headers=headers
        )


class UpdateEntryView(
    FormURLMixin, FormEntryMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet
):
    """
    We need a separate view for update, for the case when the user
    can fill the same form multiple times.
    """

    def get_queryset(self):
        return FormModel.objects.auth()

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        self.form = self.get_object()
        serializer = self.get_serializer(self.form, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.response_payload, status=status.HTTP_201_CREATED,)


class FileUploadView(
    FormURLMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    """
    File upload for forms
    """

    queryset = FormModel.objects.active()
    serializer_class = FileFieldEntrySerializer

    def destroy(self, request, *args, **kwargs):
        try:
            FieldEntryModel.objects.get(pk=kwargs.get(self.lookup_url_kwarg)).delete()
        except FieldEntryModel.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        return Response(status=status.HTTP_204_NO_CONTENT)
