# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf.urls import url

from rest_framework.routers import DefaultRouter

from form_builder.constants import FORM_PATH_REGEX
from form_builder.api import views


router = DefaultRouter(trailing_slash=False)
router.register("definition", views.FormDefinitionViewSet, basename="definition")
router.register("file-upload", views.FileUploadView, basename="file_upload")
urlpatterns = router.urls

urlpatterns += [
    # url(r'^definition/{}$'.format(FORM_PATH_REGEX), views.FormDefinitionViewSet.as_view({'get': 'retrieve'})),
    url(
        r"^submit/{}$".format(FORM_PATH_REGEX),
        views.CreateEntryView.as_view({"post": "create"}),
        name="submit",
    ),
    url(
        r"^update/{}$".format(FORM_PATH_REGEX),
        views.UpdateEntryView.as_view({"put": "update"}),
        name="update",
    ),
    url(
        r"^file-upload/{}$".format(FORM_PATH_REGEX),
        views.FileUploadView.as_view({"post": "create", "delete": "destroy"}),
        name="file_upload",
    ),
]
