# Generated by Django 2.2.3 on 2019-08-06 11:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("form_builder", "0001_initial"),
    ]

    operations = [
        migrations.RemoveField(model_name="form", name="moderated",),
        migrations.AlterField(
            model_name="field",
            name="extra_validator",
            field=models.CharField(
                blank=True,
                choices=[
                    (
                        "form_builder.validators.field_validation_example",
                        "Example - Do Nothing",
                    )
                ],
                max_length=190,
                null=True,
                verbose_name="Extra Validators",
            ),
        ),
        migrations.AlterField(
            model_name="form",
            name="extra_validator",
            field=models.CharField(
                blank=True,
                choices=[
                    (
                        "form_builder.validators.form_validation_example",
                        "Example - Do Nothing",
                    )
                ],
                default=None,
                help_text="Add your choice in settings.FORM_EXTRA_VALIDATOR",
                max_length=190,
                null=True,
                verbose_name="Extra Validators",
            ),
        ),
        migrations.AlterField(
            model_name="form",
            name="form_template",
            field=models.CharField(
                choices=[("form_builder/form.html", "Default")],
                help_text="Select form template which will be used to render form.",
                max_length=200,
                verbose_name="form template",
            ),
        ),
        migrations.AlterField(
            model_name="form",
            name="page_template",
            field=models.CharField(
                choices=[("form_builder/single_form_page.html", "Default")],
                help_text="Select page template which will be used to render form.",
                max_length=200,
                verbose_name="page template",
            ),
        ),
        migrations.AlterField(
            model_name="form",
            name="resubmit_page_template",
            field=models.CharField(
                blank=True,
                choices=[
                    (
                        "form_builder/resubmit_form/single_form_resubmit.html",
                        "Single Form Page",
                    )
                ],
                help_text="Select page template which will be used to render the resubmit form.",
                max_length=200,
                null=True,
                verbose_name="Resubmit page template",
            ),
        ),
        migrations.AlterField(
            model_name="form",
            name="success_page_template",
            field=models.CharField(
                choices=[("form_builder/success.html", "Default")],
                help_text="Select page template for default success page.",
                max_length=200,
                verbose_name="success page template",
            ),
        ),
    ]
