# Generated by Django 2.2.4 on 2019-08-21 13:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("form_builder", "0007_auto_20190815_1355"),
    ]

    operations = [
        migrations.AddField(
            model_name="formfield",
            name="field_class",
            field=models.CharField(
                choices=[
                    ("CharField", "Text"),
                    ("EmailField", "Email"),
                    ("URLField", "URL"),
                    ("IntegerField", "Number"),
                    ("PhoneNumberField", "Phone Number"),
                    ("KwPhoneNumberField", "KW Phone Number"),
                    ("DecimalField", "Decimal"),
                    ("BooleanField", "Checkbox"),
                    ("DateField", "Date"),
                    ("DateTimeField", "Date & time"),
                    ("TimeField", "Time"),
                    ("ChoiceField", "Choice"),
                    ("MultipleChoiceField", "Multiple choices"),
                    ("RegexField", "Regex"),
                    ("FileField", "File"),
                    ("ImageField", "Image"),
                    ("ChoiceWithOtherField", "Choice field with optional answer"),
                    ("KWCivilIDNumberField", "Kuwait Civil ID"),
                    ("NoReCaptchaField", "Captcha"),
                    ("SmileFaceRatingField", "Smile Face Rating"),
                    ("StarsRatingField", "Stars Rating"),
                    (
                        "MultipleChoiceWithInputField",
                        "Multiple Choice field with input answer",
                    ),
                ],
                default="CharField",
                max_length=100,
                verbose_name="Field class",
            ),
        ),
        migrations.AddField(
            model_name="formfield",
            name="name",
            field=models.CharField(default="", max_length=64, verbose_name="name"),
        ),
        migrations.AlterField(
            model_name="formfield",
            name="hidden_by_default",
            field=models.BooleanField(default=False, verbose_name="Hidden"),
        ),
        migrations.AlterField(
            model_name="formfield",
            name="is_active",
            field=models.BooleanField(default=True, verbose_name="active?"),
        ),
        migrations.AlterField(
            model_name="formfield",
            name="is_display",
            field=models.BooleanField(default=False, verbose_name="In column"),
        ),
        migrations.AlterField(
            model_name="formfield",
            name="is_filter",
            field=models.BooleanField(default=False, verbose_name="As filter"),
        ),
        migrations.AlterField(
            model_name="formfield",
            name="show_chart",
            field=models.BooleanField(default=True, verbose_name="In Chart?"),
        ),
    ]
