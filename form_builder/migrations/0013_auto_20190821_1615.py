# Generated by Django 2.2.4 on 2019-08-21 16:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("form_builder", "0012_fieldentry_form_field"),
    ]

    operations = [
        migrations.AlterField(
            model_name="choice",
            name="trigger_field",
            field=models.ManyToManyField(
                blank=True,
                related_name="trigger_field",
                to="form_builder.FormField",
                verbose_name="Trigger field",
            ),
        ),
    ]
