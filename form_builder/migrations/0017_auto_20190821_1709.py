# Generated by Django 2.2.4 on 2019-08-21 17:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("form_builder", "0016_auto_20190821_1707"),
    ]

    operations = [
        migrations.AlterUniqueTogether(name="formfield", unique_together=set(),),
    ]
