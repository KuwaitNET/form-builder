# Generated by Django 2.2.4 on 2019-08-21 16:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("form_builder", "0013_auto_20190821_1615"),
    ]

    operations = [
        migrations.RemoveField(model_name="field", name="is_display",),
        migrations.RemoveField(model_name="field", name="is_filter",),
        migrations.AlterField(
            model_name="formfield",
            name="field",
            field=models.OneToOneField(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="form_builder.Field",
                verbose_name="field setting",
            ),
        ),
    ]
