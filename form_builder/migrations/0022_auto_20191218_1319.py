# Generated by Django 2.1 on 2019-12-18 13:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("form_builder", "0021_auto_20191218_1113"),
    ]

    operations = [
        migrations.AddField(
            model_name="formfield",
            name="unique_form_tree",
            field=models.BooleanField(
                default=False,
                help_text="Check to make its value shared between all forms",
                verbose_name="Unique in form tree",
            ),
        ),
        migrations.AlterField(
            model_name="formfield",
            name="field_class",
            field=models.CharField(
                choices=[
                    ("CharField", "Text"),
                    ("TextAreaField", "Text Area"),
                    ("EmailField", "Email"),
                    ("URLField", "URL"),
                    ("IntegerField", "Number"),
                    ("PhoneNumberField", "Phone Number"),
                    ("KwPhoneNumberField", "KW Phone Number"),
                    ("DecimalField", "Decimal"),
                    ("BooleanField", "Checkbox"),
                    ("DateField", "Date"),
                    ("DateTimeField", "Date & time"),
                    ("TimeField", "Time"),
                    ("ChoiceField", "Choice"),
                    ("RadioChoiceField", "Radio Choice"),
                    ("MultipleChoiceField", "Multiple choices"),
                    ("RegexField", "Regex"),
                    ("FileField", "File"),
                    ("ImageField", "Image"),
                    ("ChoiceWithOtherField", "Choice field with optional answer"),
                    ("KWCivilIDNumberField", "Kuwait Civil ID"),
                    ("NoReCaptchaField", "Captcha"),
                    ("SmileFaceRatingField", "Smile Face Rating"),
                    ("StarsRatingField", "Stars Rating"),
                    (
                        "MultipleChoiceWithInputField",
                        "Multiple Choice field with input answer",
                    ),
                ],
                default="CharField",
                max_length=100,
                verbose_name="Field class",
            ),
        ),
    ]
