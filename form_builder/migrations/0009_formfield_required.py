# Generated by Django 2.2.4 on 2019-08-21 13:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("form_builder", "0008_auto_20190821_1324"),
    ]

    operations = [
        migrations.AddField(
            model_name="formfield",
            name="required",
            field=models.BooleanField(default=True, verbose_name="Required"),
        ),
    ]
