function collapse_this(that) {
    jQuery(that).trigger( "click" );
}

$(window).bind("load", function() {
    collapse_this(jQuery('.collapse.expanded')[0]);
});