jQuery(document).ready(function ($) {
    $("input[type='checkbox']").change(function(){
        if (this.checked){
            $('.'+this.value).removeClass('hidden');
            $('.'+this.value).find('input, textarea, select').not(':input[type=file]').prop('required',true);
        }
        if (!this.checked){
            $('.'+this.value).addClass('hidden');
            $('.'+this.value).find('input, textarea, select').not(':input[type=file]').prop('required',false);
        };
        $('input').each(function(){
          if ($(this).prop('readonly')){
            $(this).prop('required',false);
          };
        });
    });
    async function reset_select_hidden_field(){
      $("option").each(function(){
        if (this.value){
          $('.'+this.value).addClass('hidden');
        };
      });
      $("input[type='radio']").each(function(){
        if (this.value){
          $('.'+this.value).addClass('hidden');
        };
      })

    };
    $("select").change(function(){
        reset_select_hidden_field();
        $("select").each(function(){
              var that = this;
              if (that.value){
                $('.'+that.value).removeClass('hidden');
                $('.'+that.value).find('input, textarea, select').not(':input[type=file]').prop('required',true);
              };
              $('input').each(function(){
                if ($(that).prop('readonly')){
                  $(that).prop('required',false);
                };
              });
        });
        $("input[type='radio']").parent(":visible").find("input[type='radio']").trigger("change");
    });
    //<!--clicks accumulate the history radio clicks-->
    var clicks = new Object();
    $("input[type='radio']").change(function(){
        if ( $(this).attr('name') in clicks){
            if (this.checked){
                clicks[$(this).attr('name')].push($(this).val());
            };
        } else {
            clicks[$(this).attr('name')] = [];
            if (this.checked){
                clicks[$(this).attr('name')].push($(this).val());
            };
        };
        if (this.checked){
            //<!--hide previous choice-->
            var field_clicks = clicks[$(this).attr('name')]
            $('.'+field_clicks[field_clicks.length-2]).addClass('hidden');
            $('.'+field_clicks[field_clicks.length-2]).find('input, textarea, select').not(':input[type=file]').prop('required',false);

            //<!--show current choice-->
            $('.'+this.value).removeClass('hidden');
            $('.'+this.value).find('input, textarea, select').not(':input[type=file]').prop('required',true);
        };
        $('input').each(function(){
          if ($(this).prop('readonly')){
            $(this).prop('required',false);
          };
        });
    });
    async function initialization(){
        await reset_select_hidden_field();
        $('.visible_select').find("input[type=radio]").trigger("change");
        $('.visible_select').find("input[type=checkbox]").trigger("change");
        $('.visible_select').find("select").trigger("change");

        $('.unique_form_tree').find("input[type=radio]").trigger("change");
        $('.unique_form_tree').find("input[type=checkbox]").trigger("change");
        $('.unique_form_tree').find("select").trigger("change");
        $('.unique_form_tree.hidden_by_default').hide();
    }
    jQuery(document).ready(function ($) {
        initialization();
    });
    $('input').each(function(){
      if ($(this).prop('readonly')){
        $(this).prop('required',false);
      };
    });
});