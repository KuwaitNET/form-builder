;(function () {
	
	'use strict';
	var ww = $(window).width();
	var isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
			BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
			iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
			Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
			Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
			any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};

	var mobileMenuOutsideClick = function() {

		$(document).click(function (e) {
	    var container = $("#natfund-offcanvas, .js-natfund-nav-toggle");
	    if (!container.is(e.target) && container.has(e.target).length === 0) {

	    	if ( $('body').hasClass('offcanvas') ) {

    			$('body').removeClass('offcanvas');
    			$('.js-natfund-nav-toggle').removeClass('active');

	    	}


	    }
		});

	};


	var offcanvasMenu = function() {

		$('#page').prepend('<div id="natfund-offcanvas" />');
		$('#page').prepend('<a href="#" class="js-natfund-nav-toggle natfund-nav-toggle natfund-nav-white"><i></i></a>');
		var clone1 = $('.menu-1 > ul').clone();
		$('#natfund-offcanvas').append(clone1);
		var clone2 = $('.menu-2 > ul').clone();
		$('#natfund-offcanvas').append(clone2);

		$('#natfund-offcanvas .has-dropdown').addClass('offcanvas-has-dropdown');
		$('#natfund-offcanvas')
			.find('li')
			.removeClass('has-dropdown');

		// Hover dropdown menu on mobile
		$('.offcanvas-has-dropdown').mouseenter(function(){
			var $this = $(this);

			$this
				.addClass('active')
				.find('ul')
				.slideDown(500, 'easeOutExpo');
		}).mouseleave(function(){

			var $this = $(this);
			$this
				.removeClass('active')
				.find('ul')
				.slideUp(500, 'easeOutExpo');
		});


		$(window).resize(function(){

			if ( $('body').hasClass('offcanvas') ) {

    			$('body').removeClass('offcanvas');
    			$('.js-natfund-nav-toggle').removeClass('active');

	    	}
		});
	};


	var burgerMenu = function() {

		$('body').on('click', '.js-natfund-nav-toggle', function(event){
			var $this = $(this);


			if ( $('body').hasClass('overflow offcanvas') ) {
				$('body').removeClass('overflow offcanvas');
			} else {
				$('body').addClass('overflow offcanvas');
			}
			$this.toggleClass('active');
			event.preventDefault();

		});
	};



	var contentWayPoint = function() {
		var i = 0;
		$('.animate-box').waypoint( function( direction ) {

			if( direction === 'down' && !$(this.element).hasClass('animated-fast') ) {

				i++;

				$(this.element).addClass('item-animate');
				setTimeout(function(){

					$('body .animate-box.item-animate').each(function(k){
						var el = $(this);
						setTimeout( function () {
							var effect = el.data('animate-effect');
							if ( effect === 'fadeIn') {
								el.addClass('fadeIn animated-fast');
							} else if ( effect === 'fadeInLeft') {
								el.addClass('fadeInLeft animated-fast');
							} else if ( effect === 'fadeInRight') {
								el.addClass('fadeInRight animated-fast');
							} else {
								el.addClass('fadeInUp animated-fast');
							}

							el.removeClass('item-animate');
						},  k * 200, 'easeInOutExpo' );
					});

				}, 100);

			}

		} , { offset: '95%' } );
	};


	var dropdown = function() {

		$('.has-dropdown').mouseenter(function(){

			var $this = $(this);
			$this
				.find('> .dropdown')
				.css('display', 'block')
				.addClass('animated-fast fadeInUpMenu');

		}).mouseleave(function(){
			var $this = $(this);

			$this
				.find('> .dropdown')
				.css('display', 'none')
				.removeClass('animated-fast fadeInUpMenu');
		});

	};


	var goToTop = function() {

		$('.js-gotop').on('click', function(event){

			event.preventDefault();

			$('html, body').animate({
				scrollTop: $('html').offset().top
			}, 500, 'easeInOutExpo');

			return false;
		});

		$(window).scroll(function(){

			var $win = $(window);
			if ($win.scrollTop() > 200) {
				$('.js-top').addClass('active');
			} else {
				$('.js-top').removeClass('active');
			}

		});

	};


	// Loading page
	var loaderPage = function() {
		$(".natfund-loader").fadeOut("slow");
	};

	var counter = function() {
		$('.js-counter').countTo({
			 formatter: function (value, options) {
	      return value.toFixed(options.decimals);
	    },
		});
	};

	var counterWayPoint = function() {
		if ($('#natfund-counter').length > 0 ) {
			$('#natfund-counter').waypoint( function( direction ) {

				if( direction === 'down' && !$(this.element).hasClass('animated') ) {
					setTimeout( counter , 400);
					$(this.element).addClass('animated');
				}
			} , { offset: '90%' } );
		}
	};

	var sliderMain = function() {

	  	$('#natfund-showcase .flexslider').flexslider({
			animation: "slide",

			easing: "swing",
			direction: "vertical",

			slideshowSpeed: 5000,
			directionNav: true,
			start: function(){
				setTimeout(function(){
					$('.slider-text').removeClass('animated fadeInUp');
					$('.flex-active-slide').find('.slider-text').addClass('animated fadeInUp');
				}, 500);
			},
			before: function(){
				setTimeout(function(){
					$('.slider-text').removeClass('animated fadeInUp');
					$('.flex-active-slide').find('.slider-text').addClass('animated fadeInUp');
				}, 500);
			}

	  	});
	};

	var parallax = function() {
		if ( !isMobile.any() ) {
			$(window).stellar({
				horizontalScrolling: false,
				hideDistantElements: false,
				responsive: true

			});
		}
	};

	var testimonialCarousel = function(){

		var owl = $('.owl-carousel-fullwidth');
		owl.owlCarousel({
			items: 1,
			loop: true,
			margin: 0,
			nav: false,
			dots: true,
			smartSpeed: 800,
			autoHeight: true
		});

	};
	var tooltip = function()
	{
		$('.tooltip').tooltipster({
			animation: 'fade',
			delay: 100,
			interactive: true,
			side: 'left',
			trigger: 'click'
		});
	};
	var search = function(){
		$('a[href="#search"]').on('click', function(event) {
			event.preventDefault();
			$('#search').addClass('open');
			$('#search > form > input[type="search"]').focus();
		});

		$('#search, #search button.close').on('click keyup', function(event) {
			if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
				$(this).removeClass('open');
			}
		});


	};

	var menudropdown = function (){
		$(".account-menu-dd").text($(".left-menu .left-nav > li.active a").text());
	    $(".menu-dropdown").on("click", function (e) {
	        e.preventDefault();
	        if ($(window).width() < 992) {
		        $(this).toggleClass("menu-open");
		        $(this).parents(".natfund-secondary-nav").find("ul.list-inline").slideToggle();
		        $(this).parents(".menu-dropdown-wrapper").find(".slideable").slideToggle();
	    	}
	    });
	};
	var customscroll = function()
	{
		$(".custom-scroll").mCustomScrollbar();
		var textareaLineHeight=parseInt($(".textarea-wrapper textarea").css("line-height"));

		$(".textarea-wrapper").mCustomScrollbar({
			scrollInertia:0,
			theme:"dark-3",
			advanced:{autoScrollOnFocus:false},
			mouseWheel:{disableOver:["select","option","keygen","datalist",""]},
			keyboard:{enable:false},
			snapAmount:textareaLineHeight
		});

		var textarea=$(".textarea-wrapper textarea"),textareaWrapper=$(".textarea-wrapper"),textareaClone=$(".textarea-wrapper .textarea-clone");

		textarea.bind("keyup keydown",function(e){
			var $this=$(this),textareaContent=$this.val(),clength=textareaContent.length,cursorPosition=textarea.getCursorPosition();
			textareaContent="<span>"+textareaContent.substr(0,cursorPosition)+"</span>"+textareaContent.substr(cursorPosition,textareaContent.length);
			textareaContent=textareaContent.replace(/\n/g,"<br />");
			textareaClone.html(textareaContent+"<br />");
			$this.css("height",textareaClone.height());
			var textareaCloneSpan=textareaClone.children("span"),textareaCloneSpanOffset=0,
				viewLimitBottom=(parseInt(textareaClone.css("min-height")))-textareaCloneSpanOffset,viewLimitTop=textareaCloneSpanOffset,
				viewRatio=Math.round(textareaCloneSpan.height()+textareaWrapper.find(".mCSB_container").position().top);
			if(viewRatio>viewLimitBottom || viewRatio<viewLimitTop){
				if((textareaCloneSpan.height()-textareaCloneSpanOffset)>0){
					textareaWrapper.mCustomScrollbar("scrollTo",textareaCloneSpan.height()-textareaCloneSpanOffset-textareaLineHeight);
				}else{
					textareaWrapper.mCustomScrollbar("scrollTo","top");
				}
			}
		});

		$.fn.getCursorPosition=function(){
			var el=$(this).get(0),pos=0;
			if("selectionStart" in el){
				pos=el.selectionStart;
			}else if("selection" in document){
				el.focus();
				var sel=document.selection.createRange(),selLength=document.selection.createRange().text.length;
				sel.moveStart("character",-el.value.length);
				pos=sel.text.length-selLength;
			}
			return pos;
		}
	};

	var utilities = function(){

		$(document).on('click', ".education .list-inline > li", function(e) {
			$(".education .list-inline > li").removeClass("active");
			$(this).addClass("active");
		});

		$(".panel-heading a.en").on("click", function(e){
			$(this).parents().find(".panel-heading").toggleClass("open");
			$(this).parents().siblings().find(".open").removeClass("open")
		});
		$('input[type=file]:not(.multifile)').change(function(e){
			var parent = $(this).parents(".form-group");
			$(parent).find(".form-control").val($(this).val());
		});

		if(ww>1024){
			$(".member-item .image, .member-item .member-detail p a").on("mouseover", function(){
				$(this).parents(".member-item").addClass("setHover");
			});
			$(".member-item .image, .member-item .member-detail p a").on("mouseleave", function(){
				$(this).parents(".member-item").removeClass("setHover");
			});
		}

		$(document).on('click', ".show-pass", function(e) {
            e.preventDefault();
            var elm = $(this).parent().find(".form-control");
            if(elm.attr("type")=="text"){
                elm.attr("type","password");
            }else{
                elm.attr("type","text");
            }
        });
		if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
			$('.selectpicker').selectpicker('mobile');
		}

		$(".natfund-widget h3").on("click", function(e){
			if($(window).width() < 500) {
				$(this).parent().find(".natfund-footer-links").slideToggle();
			}
		});
		$(window).resize(function(e){
			if($(window).width() > 500) {
				$(".natfund-footer-links").show();
			}
			else {
				$(".natfund-footer-links").hide();
			}
		});

	};

    var configureModal = function () {
        $("body").on("click", "*[data-toggle='modal']", function () {
            var next_url = $(this).data('next-url');
            if (next_url) {
                var current_url = $('#login-form').attr('action').split('?')[0];
                $('#login-form').attr('action', current_url + "?next=" + next_url)
            }

            $("body").addClass("remove-scroll");
        });
        $(".modal").on("hidden.bs.modal", function () {
            $(".default-modal .modal-body").removeClass("show");
            $(".default-modal .modal-body").empty();
            $(".default-modal").removeClass("large");
            $("body").removeClass("remove-scroll");
        });
    };
	$(function(){
		mobileMenuOutsideClick();
		offcanvasMenu();
		burgerMenu();
		contentWayPoint();
		dropdown();
		goToTop();
		loaderPage();
		counterWayPoint();
		counter();
		parallax();
		sliderMain();
		testimonialCarousel();
		tooltip();
		search();
		utilities();
		menudropdown();
		customscroll();
		configureModal();
	});


}());
var lazyload = {
	load: function (wrapper, dataURL) {
		$(".marker-end")
			.on('lazyshow', function () {
				if ($("#loadmorecount").val() < 5) {
                    var URL = dataURL + '?page=' + $("#loadmorecount").val();
					$.ajax({
						url: URL,
						dataType: "html",
						success: function (responseText) {
							var 	html_text = $.parseJSON(responseText).html;
							if (html_text != "") {
								$(wrapper).append($.parseHTML(html_text));
								$(window).lazyLoadXT();
								$('.marker-end').lazyLoadXT({visibleOnly: false, checkDuplicates: false});
								$("#loadmorecount").val(parseInt($("#loadmorecount").val()) + 1);
							} else {
								$('.marker-end').hide();
							}
						},
						complete: function () {

						}
					})
				} else {
					$('.marker-end').hide();
				}
			})
			.lazyLoadXT({visibleOnly: false});
	}
};
function selectFileExtention(value, list) {
    var ext = value.split(".")[1];
    $(".docs-file-list > .MultiFile-label").removeClass("new");
    setTimeout(function () {
        if (ext == "pdf") {
            $('<i class="fa fa-file-pdf-o" aria-hidden="true"></i>').insertBefore($(list).find("> .MultiFile-label.new .file-extention span"));
            $(list).find("> .MultiFile-label.new .file-extention").css("color", "red");
        }
        if (ext == "doc" || ext == "docx") {
            $('<i class="fa fa-file-word-o" aria-hidden="true"></i>').insertBefore($(list).find("> .MultiFile-label.new .file-extention span"));
            $(list).find("> .MultiFile-label.new .file-extention").css("color", "red");
        }
        if (ext == "xls" || ext == "xlsx") {
            $('<i class="fa fa-file-excel-o" aria-hidden="true"></i>').insertBefore($(list).find("> .MultiFile-label.new .file-extention span"));
            $(list).find("> .MultiFile-label.new .file-extention").css("color", "red");
        }
        $(list).find("> .MultiFile-label.new .file-extention span").text(ext);
    }, 100);
}




$(document).ready(function () {
    $("#contact-us-form").validate({
        highlight: function (element, errorClass, validClass) {
            $(element).parent().addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parent().removeClass("has-error");
        }
    });
    $("#address-dropdown").change(function () {
        var url = $('option:selected', this).data('url');
        window.location.href = url;
    });

});
$.DjangoAjaxHandler2({
    submitBtn: '#contact-us-btn',
    resetSubmitEvents: true,
    form_selector: '#contact-us-form',
    canSubmit: function (form, instance) {
        $(instance.options.submitBtn).button('reset');
        // we will check valid method from validation
        return $(instance.form).valid();
    },
    afterResponse: function(form, payload){
        grecaptcha.reset();
    }

});