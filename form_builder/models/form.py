# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from random import randint

from adminsortable.models import Sortable
from autoslug import AutoSlugField
from ckeditor.fields import RichTextField
from django.conf import settings
from django.core.exceptions import ValidationError, MultipleObjectsReturned
from django.apps import apps
from django.contrib.auth import get_user_model

try:
    from django.shortcuts import reverse
except ImportError:
    from django.core.urlresolvers import reverse
from django.core.validators import MaxValueValidator
from django.db import models
from django.db.models import Q

try:
    from django.utils.encoding import python_2_unicode_compatible
except ImportError:
    from django.utils.six import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from treebeard.mp_tree import MP_Node

from form_builder import settings as form_settings
from ..utils import get_submitter, reverse_no_i18n
from .field import AbstractFieldFormMixin

User = get_user_model()


class FormFieldQuerySet(models.QuerySet):
    def active(self):
        return self.filter(is_active=True)

    def files(self):
        return self.filter(field_class__in=form_settings.FILE_CLASSES)

    def non_files(self):
        return self.exclude(field_class__in=form_settings.FILE_CLASSES)


class FormFieldManager(models.Manager):
    def get_queryset(self):
        return FormFieldQuerySet(self.model, using=self._db)

    def active(self):
        return self.get_queryset().active()

    def files(self):
        return self.get_queryset().files()

    def non_files(self):
        return self.get_queryset().non_files()


@python_2_unicode_compatible
class AbstractFormField(AbstractFieldFormMixin, Sortable):
    (
        TEXT,
        TEXTAREA,
        RADIO_BUTTON,
        RADIO_BUTTON_INLINE,
        PASSWORD,
        HIDDEN,
        CHECKBOX,
        CHECKBOX_MULTIPLE,
        OPTIONAL_CHOICE,
    ) = (
        "text",
        "textarea",
        "radio",
        "radio_inline",
        "password",
        "hidden",
        "checkbox",
        "checkbox_multiple",
        "optional_choice",
    )

    WIDGET_CHOICES = (
        (TEXT, _("Text")),
        (TEXTAREA, _("Textarea")),
        (RADIO_BUTTON, _("Radio button")),
        (RADIO_BUTTON_INLINE, _("Radio button inline")),
        (PASSWORD, _("Password")),
        (CHECKBOX, _("Checkbox")),
        (CHECKBOX_MULTIPLE, _("Checkbox Multiple")),
        (OPTIONAL_CHOICE, _("Optional Choice")),
        (HIDDEN, _("Hidden")),
    )

    form = models.ForeignKey(
        "form_builder.Form",
        verbose_name=_("form"),
        related_name="fields",
        on_delete=models.CASCADE,
    )
    field = models.ForeignKey(
        "form_builder.Field",
        null=True,
        blank=True,
        related_name="formfield",
        verbose_name=_("field setting"),
        on_delete=models.SET_NULL,
    )
    is_active = models.BooleanField(_("active?"), default=True)
    show_chart = models.BooleanField(_("In Chart?"), default=False)
    is_filter = models.BooleanField(_("As filter"), default=False)
    is_display = models.BooleanField(_("In column"), default=False)
    hidden_by_default = models.BooleanField(_("Hidden"), default=False)
    unique_form_tree = models.BooleanField(
        _("Unique in form tree"),
        default=False,
        help_text=_("Check to make its value shared between all forms"),
    )

    slug = models.CharField(_("Slug"), max_length=64, default="")
    field_class = models.CharField(
        _("Field class"),
        choices=form_settings.FIELD_CLASSES,
        max_length=100,
        default="CharField",
    )
    required = models.BooleanField(_("Required"), default=True)
    label = models.CharField(
        _("Label"), default="", max_length=form_settings.LABEL_MAX_LENGTH,
    )

    objects = FormFieldManager()

    def __str__(self):
        return self.slug

    def clean(self):
        import re

        pattern = re.compile("^([a-z0-9_])*$")
        if not pattern.match(self.slug):
            raise ValidationError(
                _("Please enter a valid name, English small letters, numbers or _")
            )
        super(AbstractFormField, self).clean()

    class Meta:
        verbose_name = _("Form Field")
        verbose_name_plural = _("Form Fields")
        ordering = ("order",)
        abstract = True


class FormQuerySet(models.QuerySet):
    def active(self):
        return self.filter(is_active=True)

    def public(self):
        return self.filter(login_required=False)

    def auth(self):
        return self.filter(login_required=True)


class FormManager(models.Manager):
    def get_queryset(self):
        return FormQuerySet(self.model, using=self._db)

    def active(self):
        return self.get_queryset().active()

    def public(self):
        return self.active().public()

    def auth(self):
        return self.active().auth()

    def get_for_submitter(self, submitter):
        """
        :param submitter: settings.AUTH_USER_MODEL instance
        :return: forms which has the entries submitted by submitter
        """
        return self.filter()

    def get_for_request(self, request):
        return self.active()

    def get_from_path(self, path):
        try:
            return self.get(url_path=path)
        except self.model.DoesNotExist:
            return None


@python_2_unicode_compatible
class AbstractFormType(models.Model):
    name = models.CharField(_("Form Type"), max_length=50)
    users = models.ManyToManyField(User, verbose_name=_("Permitted users"), blank=True)
    groups = models.ManyToManyField(
        "auth.Group", verbose_name=_("Permitted Groups"), blank=True
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Form Type")
        verbose_name_plural = _("Form Types")
        abstract = True


@python_2_unicode_compatible
class AbstractForm(MP_Node):
    """
    A form can have fields or no, or it can be used for
    navigational purposes.

    We can have the following cases:

    1. One form with fields
        - the submit method will called from the form directly

    2. One parent form and one or multiple children forms
        - moderated has to be False on all children
    """

    ENTRY_CONDITION = (
        ("single", _("One Time")),
        ("multiple", _("Multiple")),
        ("sequential", _("sequential")),
    )
    entry_condition = models.CharField(
        _("Entry Condition"),
        max_length=50,
        choices=ENTRY_CONDITION,
        default="single",
        null=True,
        blank=True,  # Accepting null is a transient state to be able to import NF DB
        help_text=_("Respected only if login required is enabled"),
    )
    days_next_submit = models.IntegerField(
        default=0, verbose_name=_("Days between submissions")
    )
    is_active = models.BooleanField(_("is active?"), default=True, db_index=True)
    login_required = models.BooleanField(
        _("login required"),
        default=True,
        db_index=True,
        help_text=_("If checked, only registered users can view and fill " "the form."),
    )

    name = models.CharField(_("name"), max_length=50)
    slug = AutoSlugField(
        _("slug"),
        max_length=50,
        unique=True,
        populate_from="name",
        editable=form_settings.EDITABLE_SLUG,
    )
    description = RichTextField(
        _("description"),
        blank=True,
        default="",
        help_text=_("Describe for what is being used this form."),
    )
    resubmission_intro = RichTextField(
        _("Resubmission Introduction"),
        blank=True,
        default="",
        help_text=_("Resubmission Introduction"),
    )
    success_message = RichTextField(
        _("success message"),
        blank=True,
        default="",
        help_text=_("Message showed to the user after submit."),
    )

    notify_users = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        verbose_name=_("notify Users"),
        blank=True,
        related_name="+",
        help_text=_(
            "Users which shall be notified on form entry submit. "
            "Leave empty for if notifying is not required."
        ),
    )
    notify_groups = models.ManyToManyField(
        "auth.Group", verbose_name=_("Notified Groups"), blank=True
    )
    notify_emails = models.CharField(
        verbose_name=_("Notify Emails"),
        null=True,
        blank=True,
        max_length=200,
        help_text=_(
            "Emails which shall be notified on form entry submit. "
            "Leave empty for if notifying is not required."
            " Add more than one sparated by ;"
        ),
    )
    send_email = models.BooleanField(
        _("send email"),
        default=False,
        help_text=_(
            "If checked, the person entering the form will be sent " "an email"
        ),
    )
    send_email_to_submitter = models.BooleanField(
        _("Send Email To Submitter"),
        default=False,
        help_text=_("If checked, the email template will sent to the submitter"),
    )
    email_from_is_email_field = models.BooleanField(
        _("Email From Is Email Field"),
        default=False,
        help_text=_("If checked, the FROM email will be the user email address"),
    )
    send_files_email = models.BooleanField(
        _("Attach Files to the Email"),
        default=False,
        help_text=_(
            "If checked, the notification email will include files of the form and its parents as attachments"
        ),
    )
    email_field = models.OneToOneField(
        "form_builder.FormField",
        verbose_name=_("email field"),
        null=True,
        blank=True,
        related_name="+",
        on_delete=models.SET_NULL,
        help_text=_(
            "Required only for guest users. Email field where to "
            "notify the submitter is guest"
        ),
    )
    form_builder_email_template = models.ForeignKey(
        "form_builder.EmailTemplate",
        verbose_name=_("email template"),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_(
            "Email template used to notify user on form submit. "
            "Required only if send email is checked."
        ),
    )
    verification_code_email_template = models.ForeignKey(
        "form_builder.EmailTemplate",
        verbose_name=_("email template for verification code"),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="verification_email_template",
        help_text=_("Email template used to send user his verification code. "),
    )
    submit_cancel_email_template = models.ManyToManyField(
        "form_builder.EmailTemplate",
        verbose_name=_("Email template @ submit cancellation"),
        related_name="submit_cancel_email_template",
        help_text=_("Email template used to send when cancel submission. "),
        blank=True,
    )
    url_path = models.CharField(
        _("URL path"),
        help_text=_("Full URL path"),
        max_length=1000,
        db_index=True,
        blank=True,
        default="",
    )
    page_template = models.CharField(
        _("page template"),
        choices=form_settings.FORM_BUILDER_PAGE_TEMPLATES,
        help_text=_("Select page template which will be used to render form."),
        max_length=200,
        null=True,
        blank=True,
        default="form_builder/single_form/single_form_page.html",
    )
    success_page_template = models.CharField(
        _("success page template"),
        choices=form_settings.FORM_BUILDER_SUCCESS_PAGE_TEMPLATES,
        help_text=_("Select page template for default success page."),
        max_length=200,
        null=True,
        blank=True,
    )
    form_template = models.CharField(
        _("form template"),
        choices=form_settings.FORM_BUILDER_FORM_TEMPLATES,
        help_text=_(
            "Select form template which will be used to render form. OPTIONAL FOR CUSTOMIZATION USE"
        ),
        null=True,
        blank=True,
        max_length=200,
    )
    resubmit_page_template = models.CharField(
        _("Resubmit page template"),
        choices=form_settings.FORM_BUILDER_RESUBMIT_PAGE_TEMPLATES,
        help_text=_(
            "Select page template which will be used to render the resubmit form."
        ),
        max_length=200,
        null=True,
        blank=True,
    )
    # is_inline_formset = models.BooleanField(
    #     _('inline formset'),
    #     default=False,
    #     help_text=_('This form will become a parent form for a set of '
    #                 'formsets. <br><b>NOTE: Children\'s of this form will '
    #                 'not be considered steps anymore and will be formset for '
    #                 'the current form. </b><br>If you wish to have a next '
    #                 'step after an inline formset add the form as a sibling '
    #                 'after the inline formset.')
    # )
    is_formset = models.BooleanField(
        _("is formset"),
        default=False,
        help_text=_(
            "Useful to create and handling a set of forms on the " "same page."
        ),
    )
    formset_can_delete = models.BooleanField(
        _("can delete"),
        default=True,
        help_text=_("Define if users are allowed to select forms for " "deletion."),
    )
    formset_can_order = models.BooleanField(
        _("can order"),
        default=False,
        help_text=_("Define if users will be able to change order of forms."),
    )
    formset_max_num = models.PositiveSmallIntegerField(
        _("max number of forms"),
        default=10,
        help_text=_("Limit the form number user can add and submit."),
        validators=[MaxValueValidator(1000)],
    )
    formset_min_num = models.PositiveSmallIntegerField(
        _("min number of forms"),
        default=0,
        help_text=_("Limit the form number user can add and submit."),
    )

    permission = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        verbose_name=_("Permission Users"),
        related_name="permitted_users",
        blank=True,
    )

    use_verification = models.BooleanField(
        default=False, help_text=_("Select to enable pre-defined verification code")
    )
    enable_edit_submit = models.BooleanField(
        default=False, help_text=_("Select to enable Editing to the submitted records")
    )
    filter_choice = models.BooleanField(
        verbose_name=_("Filter for all choice fields"),
        default=True,
        help_text=_("In Submission page, add filter to all choice fields"),
    )
    display_choice = models.BooleanField(
        verbose_name=_("Display for all choice fields"),
        default=True,
        help_text=_("In Submission page, add all choice fields to list_display"),
    )
    in_user_profile = models.BooleanField(
        verbose_name=_("Show in User Profile"), default=False
    )

    workflow = models.ForeignKey(
        "form_builder.Workflow",
        verbose_name=_("Workflow"),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_("Workflow attached to this form "),
    )

    print_template = models.CharField(
        _("Print Template"),
        null=True,
        blank=True,
        choices=form_settings.FORM_BUILDER_PRINT_TEMPLATES,
        max_length=100,
    )

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)
    form_type = models.ForeignKey(
        "form_builder.FormType",
        verbose_name=_("Form Type"),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )
    extra_validator = models.CharField(
        max_length=190,
        choices=form_settings.FORM_EXTRA_VALIDATOR,
        default=None,
        null=True,
        blank=True,
        verbose_name=_("Extra Validators"),
        help_text=_("Add your choice in settings.FORM_EXTRA_VALIDATOR"),
    )
    custom_action = models.CharField(
        max_length=190,
        choices=form_settings.FORM_CUSTOM_ACTION,
        null=True,
        blank=True,
        verbose_name=_("Custom Action"),
    )
    custom_action_at_creation = models.CharField(
        max_length=190,
        choices=form_settings.FORM_CUSTOM_ACTION_AT_CREATION,
        null=True,
        blank=True,
        verbose_name=_("Custom Action At Form Creation"),
    )

    is_available_script = models.CharField(
        max_length=190,
        choices=form_settings.FORM_IS_AVAILABLE,
        null=True,
        blank=True,
        verbose_name=_("Is Available Script"),
    )
    not_available_template = models.CharField(
        _("Not Available page template"),
        help_text=_("Select page template for default Not Available page."),
        max_length=200,
        null=True,
        blank=True,
        choices=form_settings.FORM_BUILDER_NOT_AVAILABLE_PAGE_TEMPLATES,
    )

    objects = FormManager()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Form")
        verbose_name_plural = _("Forms")
        ordering = ["path"]
        abstract = True

    def clean(self):
        super(AbstractForm, self).clean()
        if self.send_email and self.form_builder_email_template is None:
            raise ValidationError(
                _("If send email is checked an email template must be " "provided!")
            )

        if self.is_available_script and not self.not_available_template:
            raise ValidationError(
                _(
                    "You can't select Validation Script to the Form without selecting a valid "
                    "Template for not Validation condition"
                )
            )

        if self.send_files_email and not self.send_email:
            raise ValidationError(
                _(
                    "You can't select Send attachment files without select send email itself"
                )
            )

        if self.entry_condition and not self.login_required:
            raise ValidationError(
                _("You can't choose Entry Condition unless Login is required")
            )

        if not self.is_root and (
            self.login_required and not self.get_root().login_required
        ):
            raise ValidationError(
                _(
                    "You can't checked login required field because (%s) "
                    "doesn't have login required "
                )
                % format(self.get_root().name)
            )

        if self.email_from_is_email_field and not self.email_field:
            raise ValidationError(
                _(
                    "You can't select Email From Is Email Field without choosing the  send email field"
                )
            )

        if self.email_field and (
            not self.email_field.required
            or "EmailField" not in self.email_field.field_class
        ):
            raise ValidationError(
                _(
                    "The email Field should be required and should be configured as Field Class = Email"
                )
            )

    def get_fields(self):
        return self.fields.active()

    def get_path(self):
        return "/".join([form.slug for form in self.get_ancestors()] + [self.slug])

    def delete(self, *args, **kwargs):
        forms = self.__class__.get_tree(self)
        forms.delete()

    def is_wizard(self):
        """
        Define if form has multiple steps.
        :returns: bool
        """
        return self.get_children().exists()

    def has_formset_childrens(self):
        """
        Define if form has multiple steps.
        :returns: bool
        """
        return self.get_children().filter(is_formset=True).exists()

    def is_single(self):
        """
        Define if form has a single step.
        :return: bool
        """
        return not self.is_wizard() and not self.get_ancestors().exists()

    def get_next(self, submitter, token):
        """
        Navigates through tree nodes and checks conditionals before retrieving
        next from.

        NOTE
        It uses the get_hierarchical_next() method to navigate through nodes
        which is limited to only 4 children's.

        How it works:
            1) If redirect_to defined, use it
            2) If there is no FormEntry for current user/token return next
            step from hierarchy.
            3) If FormEntry exist, check conditionals and if they are met
            go to the step returned by condition.
            4) If no conditions are met go to next step from hierarchy.

        """
        FormEntry = apps.get_model(app_label="form_builder", model_name="FormEntry")
        try:
            form_entry = FormEntry.objects.filter(
                Q(submitter=submitter) & Q(token=token) & Q(form=self)
            ).first()
        except FormEntry.DoesNotExist:
            return self
        else:
            for conditional in self.conditional_redirects.all():
                next_form = conditional.check_fields_conditions(
                    form_entry.get_fields_to_dict()
                )
                if next_form:
                    return next_form
        all_forms = self.__class__.get_tree(self.get_root())
        condition_exclude_ids = [o.id for o in self.exclude_form_list(submitter, token)]
        all_forms = (
            all_forms.active()
            .exclude(is_formset=True)
            .exclude(id__in=condition_exclude_ids)
        )
        current_pos = None
        for index, form in enumerate(all_forms):
            if self.is_formset:
                this_form = self.get_parent()
            else:
                this_form = self

            if form == this_form:
                current_pos = index + 1
                break
        if current_pos:
            try:
                next_form = all_forms[current_pos]
                return next_form
            except IndexError:
                return

    def exclude_form_list(self, submitter, token):
        """
        It will exclude condition redirect forms except user selected form
        """
        FormEntry = apps.get_model(app_label="form_builder", model_name="FormEntry")
        exclude_form_list = []
        all_form = self.get_root().get_descendants()
        for form in all_form:
            try:
                form_entry = FormEntry.objects.get(
                    Q(submitter=submitter) & Q(token=token) & Q(form=form)
                )
            except MultipleObjectsReturned:
                pass
            except FormEntry.DoesNotExist:
                pass
            else:
                for conditional in form.conditional_redirects.all():
                    next_form = conditional.check_fields_conditions(
                        form_entry.get_fields_to_dict()
                    )
                    if not next_form:
                        for inner_form in form.get_descendants():
                            if inner_form.id == conditional.redirect_to.id:
                                exclude_form_list.append(inner_form)

        return exclude_form_list

    def get_hierarchical_next(self, submitter, token):
        """
        Navigates through tree nodes only. It does not care about
        form conditionals.


            1) If form is has children's return first active child.
            2) Else if form is not root of tree and has sibling return
        first sibling.
            3) Navigates back up on tree to the root by checking siblings of
        parent.

        ie.
                                                GrandChildX
                                              /
                                GrandChildOne
                              /
                    ChildOne  - GrandChildTwo - GrandChildY
                   /          \
                  /             GrandChildThree
                 /
                /               GrandChildFor
               /              /
        Parent ---- ChildTwo
               \              \ GrandChildFive
                \
                 \
                    ChildThree
        """
        FormEntry = apps.get_model(app_label="form_builder", model_name="FormEntry")

        def _get_branch_sibling(node):
            if not node.is_root():
                form_entries_pks = FormEntry.objects.filter(
                    Q(submitter=submitter) & Q(token=token)
                ).values_list("form_id", flat=True)

                sibling = node.get_siblings().active().exclude(is_formset=True)
                sibling = sibling.exclude(path=node.path)
                sibling = sibling.exclude(pk__in=form_entries_pks)
                sibling = sibling.first()

                if sibling:
                    # if sibling.login_required and not submitter.is_authenticated:
                    #     return None
                    return sibling
                else:
                    return _get_branch_sibling(node.get_parent())

        for form in self.get_descendants().active().exclude(is_formset=True):
            # if form.login_required and not submitter.is_authenticated:
            #     return
            return form

        return _get_branch_sibling(self)

    def get_previous(self, submitter, token):
        """
        Get previous step of the current form.

            1. If form is root return None.
            2. If form has FormEntry retrieve previous by filtering using
        current form if it doesn't return last form entry.
        :return: FormObj
        """
        FormEntry = apps.get_model(app_label="form_builder", model_name="FormEntry")
        if self.is_root():
            return

        qs = FormEntry.objects.filter(submitter=submitter, token=token)
        qs = qs.order_by("pk")

        if qs.exists():
            try:
                form_entry = qs.get(form=self)
            except FormEntry.DoesNotExist:
                return qs.last().form
            else:
                return qs.filter(pk__lt=form_entry.pk).last().form

    def count_required_steps(self):
        """
        Count total required steps.
        :return: int
        """
        return self.get_root().get_descendants().active().count()

    def get_forms_entires(self, request, token):
        FormEntry = apps.get_model(app_label="form_builder", model_name="FormEntry")
        return FormEntry.objects.filter(submitter=get_submitter(request), token=token)

    def calculate_progress(self, request, forms, token):
        """
            Calculate the overall progress of forms
            :return float
        """
        user_entries = self.get_forms_entires(request, token)
        percentage_progress = None
        if forms:
            percentage_progress = round(len(user_entries) * 100 / len(forms))
        return percentage_progress

    def has_fields(self):
        """
        Define if form has available fields.
        :return: bool
        """
        return self.fields.active().exists()

    def has_required_files(self):
        return (
            self.fields.active()
            .filter(required=True, field_class__in=form_settings.FIELD_CLASSES)
            .exists()
        )

    def has_previous(self):
        return not self.is_root()

    def get_absolute_url(self):
        return reverse("form_builder:create", args=(self.url_path,))

    def get_absolute_url_no_i18n(self):
        return reverse_no_i18n("form_builder:create", args=(self.url_path,))

    def get_absolute_submission_url_no_i18n(self):
        return (
            reverse_no_i18n("admin:form_builder_formsubmit_changelist")
            + "?form__id__exact="
            + str(self.id)
        )

    def get_absolute_entry_url_no_i18n(self, entry):
        return reverse_no_i18n(
            "form_builder:submission_form_detail_admin",
            args=(self.url_path, entry.form_submit.id),
        )

    def is_last(self, submitter, token):
        return not bool(self.get_next(submitter=submitter, token=token))

    def get_definition_url(self):
        return reverse("form_builder:api:definition-detail", args=(self.pk,))

    def get_previous_url(self, submitter, token):
        previous_step = self.get_previous(submitter=submitter, token=token)
        if previous_step:
            return previous_step.get_absolute_url()

    def get_redirect_url(
        self,
        request,
        submitter,
        token,
        next_form,
        entry_pks=None,
        custom_next_url=None,
    ):
        """
        Rules:
            1) If custom next url then redirect to Custom Next Url
            2) If Check if the form (Form model Instance) is wizard redirect
        to the next form in the wizard.
            3) Else redirect to success page.
            4) If it is last from then create entry in form submit
        """
        FormEntry = apps.get_model(app_label="form_builder", model_name="FormEntry")
        FormSubmit = apps.get_model(app_label="form_builder", model_name="FormSubmit")
        next_url = None
        if next_form:
            next_url = next_form.get_absolute_url()

        if custom_next_url:
            next_url = custom_next_url

        # if it is last form(Which doesn't have next form) We need to create entry in form submit
        if not next_form:
            try:
                root_form = FormEntry.objects.get(
                    submitter=submitter, token=token, form__depth=1
                )
            except FormEntry.DoesNotExist:
                pass
            else:
                try:
                    del request.session["{}-token".format(root_form.form.name)]
                except KeyError:
                    pass
            if entry_pks:
                form_entry = FormEntry.objects.filter(pk__in=entry_pks)
                if form_entry.filter(form_submit__isnull=True).exists():
                    form_submit = FormSubmit.objects.create(
                        submitted=True, submitter_user=submitter, form=self
                    )
                    form_entry = FormEntry.objects.filter(
                        token=token, submitter=submitter, form_submit__isnull=True
                    ).update(form_submit=form_submit)
                    form_submit.after_submit(self, request)
                else:
                    form_submit = form_entry.first().form_submit
                return reverse(
                    "form_builder:success", args=(self.url_path, form_submit.pk)
                )
        return next_url

    def save(self, *args, **kwargs):
        super(AbstractForm, self).save(*args, **kwargs)
        # TODO: validate the parent form is not a formset if current is formset
        if self.url_path != self.get_path():
            self.url_path = self.get_path()
            kwargs["force_insert"] = False
            super(AbstractForm, self).save(*args, **kwargs)


# FormModel = deferred.MaterializedModel(AbstractForm)


class UploadTo:
    def __init__(self, name):
        self.name = name

    def __call__(self, instance, filename):
        if hasattr(instance, "form") and instance.form and instance.form != "":
            ref = instance.form.id
        else:
            ref = randint(1000000000, 9999999999)
        return "{}/{}/{}/{}/{}".format(
            "form_builder", type(instance).__name__, self.name, ref, filename
        )

    def deconstruct(self):
        return "form_builder.models.form.UploadTo", [self.name], {}


class PDFForFormAbstract(models.Model):
    pdf = models.FileField(upload_to=UploadTo("pdf"))
    form = models.ForeignKey(
        "form_builder.Form",
        verbose_name=_("form"),
        on_delete=models.CASCADE,
        limit_choices_to={"is_active": True},
    )
    button_text = models.CharField(max_length=500, blank=True)

    def __str__(self):
        form_name = []
        if hasattr(self.form, "name_en") and self.form.name_en:
            form_name.append(self.form.name_en)
        if (
            hasattr(self.form, "name_ar")
            and self.form.name_ar
            and self.form.name_en != self.form.name_ar
        ):
            form_name.append(self.form.name_ar)
        return " / ".join(form_name)

    class Meta:
        verbose_name = _("File for form")
        verbose_name_plural = _("Files for form")
