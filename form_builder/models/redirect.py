from six import with_metaclass, python_2_unicode_compatible

from django.db import models
from django.core.validators import ValidationError
from django.utils.translation import ugettext_lazy as _

from form_builder import deferred


@python_2_unicode_compatible
class AbstractConditionalRedirect(models.Model):
    OPERATORS = (("and", _("AND")), ("or", _("OR")))

    form = models.ForeignKey(
        "form_builder.Form",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="conditional_redirects",
    )
    operator = models.CharField(
        verbose_name=_("logical operator"),
        choices=OPERATORS,
        max_length=3,
        help_text=_("Designates logical operator to be used on added " "conditions."),
    )
    description = models.TextField(verbose_name=_("description"), blank=True)
    redirect_to = models.ForeignKey(
        "form_builder.Form", null=True, blank=True, on_delete=models.SET_NULL
    )
    force_redirect = models.BooleanField(
        verbose_name=_("force redirect"),
        default=False,
        help_text=_("If enabled, will skip conditions checks."),
    )

    def __str__(self):
        return u"{}Redirect from {} to {}".format(
            "[FORCE] " if self.force_redirect else "", self.form, self.redirect_to
        )

    class Meta:
        verbose_name = _("Conditional redirect")
        verbose_name_plural = _("Conditional redirects")
        abstract = True

    def check_fields_conditions(self, fields):
        """
        :param fields: dictionary {'field_name': 'value'}
        :return: FormModel object
        """

        if self.force_redirect:
            return self.redirect_to

        conditions = self.conditions.filter(field_name__in=fields.keys())

        if not conditions:
            return

        checks = []
        for condition in conditions:
            checks.append(condition.result(fields))

        if (
            (self.operator == "and" and all(checks))
            or self.operator == "or"
            and any(checks)
        ):
            return self.redirect_to


# ConditionalRedirectModel = deferred.MaterializedModel(AbstractConditionalRedirect)


@python_2_unicode_compatible
class AbstractCondition(models.Model):
    """
    Defines a condition for conditional redirect.
    """

    OPERATORS = (
        ("eq", "="),
        ("neq", "!="),
        ("gt", ">"),
        ("lt", "<"),
        ("gte", ">="),
        ("lte", "<="),
    )

    redirect = models.ForeignKey(
        "form_builder.ConditionalRedirect",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="conditions",
    )
    field_name = models.CharField(verbose_name=_("field name"), max_length=100)
    operator = models.CharField(
        verbose_name=_("operator"), choices=OPERATORS, max_length=3
    )
    value_of = models.CharField(
        verbose_name=_("value of field"),
        max_length=100,
        blank=True,
        help_text=_("Name of the second field in the form to validate " "against."),
    )
    value = models.CharField(verbose_name=_("value"), max_length=100, blank=True)

    def __str__(self):
        return self.field_name

    class Meta:
        verbose_name = _("Condition")
        verbose_name_plural = _("Conditions")
        abstract = True

    def result(self, data):
        """
        Compares value of form field with name equal to self.field_name
        to a pre-defined self.value or a value of a form field with name
        self.value_of.
        :param data: dict
        :return: bool
        """
        if self.value_of and self.value_of in data:
            expected_value = data[self.value_of]
        else:
            expected_value = self.value
        value = data[self.field_name]
        if self.operator == "eq":
            return value == expected_value
        elif self.operator == "neq":
            return value != expected_value
        elif self.operator == "gt":
            return value > expected_value
        elif self.operator == "lt":
            return value < expected_value
        elif self.operator == "gte":
            return value >= expected_value
        elif self.operator == "lte":
            return value <= expected_value
        return False

    def clean(self):
        if self.value_of and self.value:
            raise ValidationError(
                _(
                    "Do not use `{value_of}` and `{value}` fields in "
                    "the same time.".format(
                        value_of=self._meta.get_field("value_of").verbose_name,
                        value=self._meta.get_field("value").verbose_name,
                    )
                )
            )


# ConditionModel = deferred.MaterializedModel(AbstractCondition)
