from random import randint

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.template import Template, TemplateSyntaxError, TemplateDoesNotExist
from django.core.exceptions import ValidationError
from django.template import engines, Context, Template
from django.core.validators import validate_email

try:
    from django.utils.encoding import python_2_unicode_compatible
except ImportError:
    from django.utils.six import python_2_unicode_compatible
# from django.core.mail import send_mail
from django.conf import settings
from django.contrib.sites.models import Site
from six import with_metaclass

try:
    from django.utils.encoding import force_text
except ImportError:
    from django.utils.text import force_text

from ckeditor.fields import RichTextField

if "mailer" in settings.INSTALLED_APPS:
    from mailer import send_mail, send_html_mail
else:
    from django.core.mail import send_mail

from form_builder import deferred


def validate_template_syntax(source):
    """
    Basic Django Template syntax validation. This allows for robuster template
    authoring.
    """
    try:
        Template(source)
    except (TemplateSyntaxError, TemplateDoesNotExist) as err:
        raise ValidationError(force_text(err))


@python_2_unicode_compatible
class AbstractEmailTemplate(models.Model):
    name = models.CharField(
        _("Name"), max_length=255, help_text=_("e.g: 'welcome_email'")
    )
    description = models.TextField(
        _("Description"),
        blank=True,
        null=True,
        help_text=_("Description of this template."),
    )
    created = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)
    subject = models.CharField(
        max_length=255,
        blank=True,
        verbose_name=_("Subject"),
        validators=[validate_template_syntax],
    )
    content = models.TextField(
        blank=True, verbose_name=_("Content"), validators=[validate_template_syntax]
    )
    # html_content = models.TextField(blank=True,
    #                                 verbose_name=_("HTML content"), validators=[validate_template_syntax])
    html_content = RichTextField(
        blank=True,
        verbose_name=_("HTML content"),
        validators=[validate_template_syntax],
    )
    email_from = models.EmailField(
        verbose_name=_("Email From Address"), null=True, blank=True
    )

    notify_users = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        verbose_name=_("notify Users"),
        blank=True,
        help_text=_(
            "Users which shall be notified "
            "Leave empty for if notifying is not required."
        ),
    )
    notify_emails = models.CharField(
        verbose_name=_("Notify Emails"),
        null=True,
        blank=True,
        max_length=150,
        help_text=_(
            "Emails which shall be notified "
            "Leave empty for if notifying is not required."
            " Add more than one sparated by ;"
        ),
    )

    # language = models.CharField(max_length=12,
    #                             verbose_name=_("Language"),
    #                             help_text=_("Render template in alternative language"),
    #                             default='', blank=True)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True

    def render_body(self, ctx=None):
        if not ctx:
            ctx = {}
        return Template(self.content).render(Context(ctx))

    def render_subject(self, ctx=None):
        if not ctx:
            ctx = {}
        return Template(self.subject).render(Context(ctx))

    def send_mail(self, ctx=None, send_to=[]):
        if not ctx:
            ctx = {}
        django_engine = engines["django"]

        html_content_template = django_engine.from_string(self.html_content)
        html_content_rendered = html_content_template.render(ctx)

        content_template = django_engine.from_string(self.content)
        content_rendered = content_template.render(ctx)

        subject_template = django_engine.from_string(self.subject)
        subject_rendered = subject_template.render(ctx)

        notified_emails = send_to
        for user in self.notify_users.all():
            notified_emails.append(user.email)

        if self.notify_emails:
            for send_to in self.notify_emails.split(";"):
                try:
                    validate_email(send_to)
                    notified_emails.append(send_to)
                except:
                    pass
        DEFAULT_FROM_EMAIL = (
            settings.DEFAULT_FROM_EMAIL
            if hasattr(settings, "DEFAULT_FROM_EMAIL")
            else None
        )
        email_from = self.email_from if self.email_from else DEFAULT_FROM_EMAIL
        try:
            send_html_mail(
                subject_rendered,
                content_rendered,
                html_content_rendered,
                email_from,
                notified_emails,
                fail_silently=False,
            )
        except:
            from django.core.mail import send_mail

            send_mail(
                subject_rendered,
                content_rendered,
                email_from,
                notified_emails,
                fail_silently=False,
                html_message=html_content_rendered,
            )


# EmailTemplateModel = deferred.MaterializedModel(AbstractEmailTemplate)


@python_2_unicode_compatible
class AbstractFormVerificationCode(models.Model):
    form = models.ForeignKey("form_builder.Form", on_delete=models.CASCADE)
    verification_code = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        editable=True,
        verbose_name=_("Verification Code"),
    )
    file_number = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        editable=True,
        verbose_name=_("File Number"),
    )
    email = models.EmailField(_("Email"), null=True, blank=True)
    company_name = models.CharField(
        _("Company Name"), max_length=100, null=True, blank=True
    )
    max_usage = models.IntegerField(_("Max Usage"), default=1)
    actual_usage = models.IntegerField(_("Actual Usage"), default=0, editable=False)
    email_template = models.ManyToManyField(
        "form_builder.EmailTemplate", blank=True, verbose_name=_("Email Template")
    )
    email_sent = models.BooleanField(_("Email is sent"), default=False, editable=False)
    is_active = models.BooleanField(_("Is Active"), default=True)

    def __str__(self):
        output = self.verification_code if self.verification_code else self.form.name
        return output

    def is_used(self):
        from form_builder.models import FieldEntry

        return FieldEntry.objects.filter(
            value=self.verification_code, field_name="verified_code"
        ).exists()

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        range_start = 10 ** (4 - 1)
        range_end = (10 ** 4) - 1

        if not self.verification_code:
            self.verification_code = str(randint(range_start, range_end))

        super(AbstractFormVerificationCode, self).save(
            force_insert=False, force_update=False, using=None, update_fields=None
        )

        domain = Site.objects.get_current().domain
        if hasattr(settings, "LANGUAGE_CODE"):
            submit_link = "".join(
                [
                    "http://",
                    domain,
                    "/{}".format(settings.LANGUAGE_CODE),
                    self.form.get_absolute_url_no_i18n(),
                ]
            )
        else:
            submit_link = "".join(
                ["http://", domain, self.form.get_absolute_url_no_i18n()]
            )

        context = {
            "form": self.form,
            "verification_code": self.verification_code,
            "company_name": self.company_name,
            "submit_link": submit_link,
        }

        if False and self.form.verification_code_email_template and not self.email_sent:
            email_template = self.form.verification_code_email_template
            django_engine = engines["django"]
            body_html_template = django_engine.from_string(email_template.html_content)
            body_html_rendered = body_html_template.render(context)
            body_plain_template = django_engine.from_string(email_template.content)
            body_plain_rendered = body_plain_template.render(context)
            subject_template = django_engine.from_string(email_template.subject)
            subject_rendered = subject_template.render(context)
            if (
                hasattr(settings, "REST_FORM_EMAIL_FROM")
                and settings.REST_FORM_EMAIL_FROM
            ):

                try:
                    send_html_mail(
                        subject_rendered,
                        body_plain_rendered,
                        body_html_rendered,
                        settings.REST_FORM_EMAIL_FROM,
                        [self.email],
                        fail_silently=False,
                    )
                except:
                    send_mail(
                        subject_rendered,
                        body_plain_rendered,
                        settings.REST_FORM_EMAIL_FROM,
                        [self.email],
                        fail_silently=False,
                        html_message=body_html_rendered,
                    )
                self.email_sent = True

    class Meta:
        abstract = True
        unique_together = (("form", "file_number"),)


# FormVerificationCodeModel = deferred.MaterializedModel(AbstractFormVerificationCode)
