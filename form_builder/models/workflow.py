# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import ast

from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings

try:
    from django.utils.encoding import python_2_unicode_compatible
except ImportError:
    from django.utils.six import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from six import with_metaclass
from form_builder import deferred
from autoslug import AutoSlugField
from treebeard.mp_tree import MP_Node
from django_extensions.db.models import TimeStampedModel
from django.utils.html import format_html

from form_builder.utils import get_upload_path, get_class
from form_builder import settings as form_settings


@python_2_unicode_compatible
class AbstractWorkflow(MP_Node):
    name = models.CharField(_("Name"), max_length=50)
    state = models.CharField(_("Status Name"), max_length=50, null=True, blank=True)
    slug = AutoSlugField(
        _("slug"),
        max_length=50,
        unique=True,
        populate_from="name",
        editable=form_settings.EDITABLE_SLUG,
    )
    # entrance_email_template = models.ManyToManyField(
    #     'form_builder.EmailTemplate',
    #     verbose_name=_("Entrance Email Template"),
    #     blank=True,
    #     related_name="workflow_entrance",
    #     help_text=_("Email template used to notify user on Entrance to this workflow step.")
    # )
    exit_email_template = models.ManyToManyField(
        "form_builder.EmailTemplate",
        verbose_name=_("Exit Email Template by admin"),
        blank=True,
        related_name="workflow_exit",
        help_text=_(
            "Email template used to notify user on exit to this workflow step."
        ),
    )
    email_template_changed_by_user = models.ManyToManyField(
        "form_builder.EmailTemplate",
        verbose_name=_("Email Template if changed by submitter"),
        blank=True,
        related_name="workflow_user_exit",
        help_text=_(
            "Email template used to notify user on enter to this workflow step if the action is done by the "
            "submitter himself."
        ),
    )
    next_step = models.ForeignKey(
        "form_builder.Workflow", null=True, blank=True, on_delete=models.SET_NULL
    )
    is_active = models.BooleanField(_("Is active?"), default=True)
    is_sent_resubmit = models.BooleanField(
        _("Send Re-Submit"),
        default=False,
        help_text=_(
            "If you want to send the resubmit to user, use this variable for resubmit url {{ resubmit_url }}"
        ),
    )
    is_state_after_resubmit = models.BooleanField(
        _("Is the state after resubmit"),
        default=False,
        help_text=_("After user do resubmit, the workflow will stand on this state"),
    )
    action_form = models.ForeignKey(
        "form_builder.Form",
        verbose_name=_("Attached Form"),
        null=True,
        blank=True,
        related_name="action_form",
        on_delete=models.SET_NULL,
    )
    apply_permission = models.BooleanField(
        verbose_name=_("Apply permission"),
        default=False,
        help_text=_("Check this if you want to apply the permissions defined here"),
    )
    permitted_users = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        verbose_name=_("Permitted User"),
        blank=True,
        help_text=_("User who is permitted to this workflow step"),
    )
    permitted_groups = models.ManyToManyField(
        "auth.Group",
        verbose_name=_("Permitted Groups"),
        blank=True,
        help_text=_("Group who is permitted to this workflow step"),
    )
    custom_script = models.CharField(
        max_length=190,
        choices=form_settings.WORKFLOW_CUSTOM_SCRIPT,
        null=True,
        blank=True,
        verbose_name=_("Custom Script"),
    )

    class Meta:
        verbose_name = _("Workflow Step")
        verbose_name_plural = _("Workflow Steps")
        ordering = [
            "path",
        ]
        abstract = True

    # def enter_action(self, ctx=None, send_to=None):
    #     """run this method when workflow reach to this step"""
    #     ctx = {}
    #     for theTemplate in self.entrance_email_template.all():
    #         theTemplate.send_mail(ctx=ctx, send_to=send_to or [])

    def exit_action(self, ctx=None, send_to=None, submission=None, request=None):
        """run this method when workflow exits from this step"""
        if request:
            ctx["current_user"] = request.user
        if submission:
            ctx["submitter"] = submission.submitter_user
        if (
            not (submission and request.user)
            or submission.submitter_user == request.user
        ):
            for theTemplate in self.exit_email_template.all():
                theTemplate.send_mail(ctx=ctx, send_to=send_to or [])

        if (
            submission and request.user and submission.submitter_user == request.user
        ):  # added to send email when the user do resubmission
            for theTemplate in self.email_template_changed_by_user.all():
                theTemplate.send_mail(ctx=ctx, send_to=send_to or [])

        if self.custom_script:
            get_class(self.custom_script)(submission, request)

    def __str__(self):
        return self.name

    def get_next_step(self):
        return self.next_step


# WorkflowModel = deferred.MaterializedModel(AbstractWorkflow)


@python_2_unicode_compatible
class AbstractWorkflowProgress(TimeStampedModel):
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL
    )
    created_by_username = models.CharField(max_length=150, null=True, blank=True)
    workflow = models.ForeignKey(
        "form_builder.Workflow", null=True, blank=True, on_delete=models.SET_NULL
    )
    workflow_name = models.CharField(max_length=150, null=True, blank=True, default="")
    submission = models.ForeignKey(
        "form_builder.FormSubmit", null=True, blank=True, on_delete=models.SET_NULL
    )

    def save(self, **kwargs):
        if self.workflow:
            self.workflow_name = self.workflow.name

        super(AbstractWorkflowProgress, self).save(**kwargs)

    class Meta:
        verbose_name = _("Workflow Progress")
        verbose_name_plural = _("Workflow Progress")
        abstract = True

    def __str__(self):
        return "{} - {}".format(
            self.workflow.name or self.workflow_name, self.submission.id or ""
        )


# AbstractWorkflowProgressModel = deferred.MaterializedModel(AbstractWorkflowProgress)


@python_2_unicode_compatible
class AbstractReSubmitFields(MP_Node):
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL
    )
    created_by_username = models.CharField(max_length=150, null=True, blank=True)
    submission = models.ForeignKey(
        "form_builder.FormSubmit", null=True, blank=True, on_delete=models.SET_NULL
    )
    form = models.ForeignKey(
        "form_builder.Form", null=True, blank=True, on_delete=models.SET_NULL
    )
    form_field = models.ForeignKey(
        "form_builder.FormField", null=True, blank=True, on_delete=models.SET_NULL
    )
    field_entry = models.ForeignKey(
        "form_builder.FieldEntry", null=True, blank=True, on_delete=models.SET_NULL
    )
    value = models.CharField(
        max_length=form_settings.FIELD_MAX_LENGTH, blank=True, default=""
    )
    file = models.FileField(_("File"), upload_to=get_upload_path, null=True, blank=True)
    submitter = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("Submitter"),
        null=True,
        blank=True,
        help_text=_("User who submitted the form"),
        related_name="resubmit_submitter",
        on_delete=models.SET_NULL,
    )

    class Meta:
        verbose_name = _("Workflow Progress")
        verbose_name_plural = _("Workflow Progress")
        abstract = True

    def __str__(self):
        return "{} - {}".format(self.submission.id or "", self.field)

    def get_formset_index(self):
        n = 0
        for entry in self.field_entry.entry.form_submit.entries.filter(form=self.form):
            if entry.field_entries.filter(id=self.field_entry.id):
                return n
            else:
                n += 1

    def get_value(self):
        if self.field and self.field.has_choices():
            if ";" in self.value:
                choice_id, text = self.value.split("; ")
                try:
                    return b"%s <br> %s" % (
                        self.field.choices.get(pk=choice_id).value,
                        text,
                    )
                except ObjectDoesNotExist:
                    return self.value
            if self.value and self.value != "NULL":
                try:
                    choices = ast.literal_eval(self.value)
                    choices = (
                        [choice for choice in choices if str(choice) != ""]
                        if type(choices) is list
                        else choices
                    )
                except SyntaxError:
                    return self.value
                except ValueError:
                    return "-"
                try:
                    return_value = []
                    for a_value in self.field.choices.filter(
                        pk__in=[int(choice) for choice in choices if choice != ""]
                    ):
                        single_value = []
                        single_value.append(b"%s" % a_value.value)
                        extras = self.resubmission_field_entry_extra.filter(
                            choice=a_value
                        )
                        if extras.exists():
                            extra = extras.last()
                            single_value.append(b"%s" % extra.value)
                        return_value.append(" -- ".join(single_value))
                    return " <br> ".join(return_value)
                except TypeError:
                    try:
                        return self.field.choices.get(pk=choices).value
                    except:
                        return self.value
        if hasattr(self.file, "url"):
            return "<a target='_blank' href='{0}'>{1}</a>".format(
                self.file.url, self.file.name.split("/")[-1]
            )
        return self.value

    def get_html_value(self):
        if self.field and self.field.has_choices():
            if ";" in self.value:
                choice_id, text = self.value.split("; ")
                try:
                    return format_html(
                        "%s <br> %s"
                        % (self.field.choices.get(pk=choice_id).value, text)
                    )
                except ObjectDoesNotExist:
                    return self.value
            if self.value and self.value != "NULL":
                try:
                    choices = ast.literal_eval(self.value)
                except SyntaxError:
                    return self.value
                except ValueError:
                    return "-"
                try:
                    return_value = []
                    for a_value in self.field.choices.filter(
                        pk__in=[int(choice) for choice in choices if choice != ""]
                    ):
                        single_value = []
                        single_value.append(b"%s" % a_value.value)
                        extras = self.resubmission_field_entry_extra.filter(
                            choice=a_value
                        )
                        if extras.exists():
                            extra = extras.last()
                            single_value.append(b"%s" % extra.value)
                        return_value.append(" -- ".join(single_value))
                    return " <br> ".join(return_value)
                except TypeError:
                    return self.field.choices.get(pk=choices).value

        if hasattr(self.file, "url"):
            return format_html(
                "<a target='_blank' href='{0}'>{1}</a>".format(
                    self.file.url, self.file.name.split("/")[-1]
                )
            )
        return self.value


# AbstractReSubmitFieldsModel = deferred.MaterializedModel(AbstractReSubmitFields)
