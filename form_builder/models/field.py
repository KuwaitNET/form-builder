# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from decimal import Decimal
import datetime

from django import forms
from django.conf import settings
from django.core.validators import (
    MinValueValidator,
    MaxValueValidator,
    MaxLengthValidator,
    MinLengthValidator,
)
from django.db import models

try:
    from django.utils.encoding import python_2_unicode_compatible
except ImportError:
    from django.utils.six import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import timedelta, now
from django.core.exceptions import ValidationError

try:
    from django.utils.encoding import force_text
except ImportError:
    from django.utils.text import force_text

from adminsortable.models import Sortable
from ckeditor.fields import RichTextField

from form_builder.utils import get_class
from form_builder.validators import validate_regex_expr
from form_builder import settings as form_settings


from ..utils import get_submitter
from .widget import CustomRadioWidget, CustomCheckboxSelectMultiple, InlineRadioWidget


class FieldQuerySet(models.QuerySet):
    def required(self):
        return self.filter(required=True)

    def files(self):
        return self.filter(field_class__in=form_settings.FILE_CLASSES)

    def non_files(self):
        return self.exclude(field_class__in=form_settings.FILE_CLASSES)


class FieldManager(models.Manager):
    def get_queryset(self):
        return FieldQuerySet(self.model, using=self._db)

    def required(self):
        return self.required()

    def files(self):
        return self.files()

    def non_files(self):
        return self.non_files()


class AbstractFieldFormMixin(object):
    """
    This are all methods and attrs for FieldModel class which will provide
    all functionality to construct a django form instance.
    """

    def get_init_form_args(self, token, request, update, is_formset, form_model):
        from form_builder.models import FieldEntry

        args = {
            "required": False
            if self.field_class in form_settings.FILE_CLASSES
            else self.required,
            "label": self.label,
            "help_text": self.field.help_text if self.field else None,
            "initial": self.field.initial if self.field else None,
        }

        if self.hidden_by_default and request.method == "POST":
            args["required"] = False
        submitter = get_submitter(request)
        # Get values for auto fill (initial)
        if self.field and self.field_class == form_settings.DateField:
            format_str = "%Y-%m-%d"  # The format
            if self.field.initial == "now":
                datetime_obj = datetime.datetime.now()
                args.update(initial=datetime_obj)
            elif self.field.initial:
                datetime_obj = datetime.datetime.strptime(
                    self.field.initial, format_str
                )
                args.update(initial=datetime_obj)

        # Get value for auto fill, if the same field were filled before
        if not form_model.is_formset:
            if self.field_class not in form_settings.FILE_CLASSES:
                field_entry = FieldEntry.objects.filter(
                    entry__token=token,
                    entry__submitter=submitter,
                    form_field=self,
                    entry__form=form_model,
                ).last()

                if field_entry:
                    if self.field_class == form_settings.DateField:
                        # format_str = '%Y-%m-%d'  # The format
                        # datetime_obj = datetime.datetime.strptime(field_entry.value, format_str)
                        args.update(initial=field_entry.value)
                    else:
                        args.update(initial=field_entry.value)
            if update and self.field_class not in form_settings.FILE_CLASSES:
                try:
                    field_entry = FieldEntry.objects.filter(
                        entry__token=token,
                        entry__submitter=submitter,
                        form_field=self,
                        form=form_model,
                    ).first()
                except FieldEntry.DoesNotExist:
                    pass
                else:
                    if self.field_class in form_settings.ChoiceWithOtherField:
                        value = field_entry.value.split("; ")
                    elif field_entry:
                        value = field_entry.value

                    if self.field_class == form_settings.DateField:
                        format_str = "%Y-%m-%d"  # The format
                        datetime_obj = datetime.datetime.strptime(
                            field_entry.value, format_str
                        )
                        args.update(initial=field_entry.value)
                    elif field_entry:
                        args.update(initial=field_entry.value)

        if self.field_class == form_settings.FileField:
            args.update(
                widget=forms.ClearableFileInput(
                    attrs={"initial_required": self.required}
                )
            )
            try:
                field_entry = FieldEntry.objects.filter(
                    entry__token=token,
                    entry__submitter=submitter,
                    form_field=self,
                    form=form_model,
                ).first()
            except FieldEntry.DoesNotExist:
                field_entry = None

            if field_entry:
                args.update(initial=field_entry.file)

        field_using = {"id": self.id}

        # CharField, EmailField, RegexField
        if self.field_class in (
            form_settings.CharField,
            form_settings.EmailField,
            form_settings.RegexField,
            form_settings.TextAreaField,
        ):
            args.update(
                max_length=self.field.max_length if self.field else None,
                min_length=self.field.min_length if self.field else None,
            )

            if self.field_class == form_settings.RegexField:
                args.update(regex=self.field.regex or "")

            if self.field and self.field.widget:
                field_using.update({"placeholder": self.field.placeholder_text})
                if self.field.widget == self.TEXTAREA:
                    args.update(widget=forms.Textarea(attrs=field_using))
                elif self.field.widget == self.PASSWORD:
                    args.update(widget=forms.PasswordInput(attrs=field_using))
                elif self.field.widget == self.HIDDEN:
                    args.update(widget=forms.HiddenInput(attrs=field_using))
                else:
                    args.update(widget=forms.TextInput(attrs=field_using))

        # IntegerField
        if self.field and self.field_class == form_settings.IntegerField:
            args.update(
                max_value=self.field.max_value if self.field else None,
                min_value=self.field.min_value if self.field else None,
            )

            if self.field.placeholder_text:
                field_using.update({"placeholder": self.field.placeholder_text})
                args.update(widget=forms.TextInput(attrs=field_using))

        if self.field_class == form_settings.PhoneNumberField:
            from phonenumber_field.validators import validate_international_phonenumber

            args.update(validators=[validate_international_phonenumber])

        # DecimalField
        if self.field_class == form_settings.DecimalField:
            args.update(
                max_value=self.field.max_value if self.field else None,
                min_value=self.field.min_value if self.field else None,
                max_digits=self.field.max_digits
                if self.field and self.field.max_digits
                else 10,
                decimal_places=self.field.decimal_places
                if self.field and self.field.decimal_places
                else 2,
            )
            if self.field and self.field.placeholder_text:
                args.update(
                    widget=forms.TextInput(
                        placeholder=self.field.placeholder_text, attrs=field_using
                    )
                )

        # DateField
        if self.field_class == form_settings.DateField:
            args.update(input_formats=settings.DATE_INPUT_FORMATS)
            validators = []

            if self.field and self.field.min_date:
                validators.append(
                    MaxValueValidator(
                        now().date() - timedelta(days=self.field.min_date),
                        message=self.field.min_date_message,
                    )
                )
            if self.field and self.field.max_date:
                validators.append(
                    MinValueValidator(
                        now().date() + timedelta(days=self.field.max_date),
                        message=self.field.max_date_message,
                    )
                )

            if validators:
                args.update(validators=validators)

            if self.field and self.field.placeholder_text:
                args.update(
                    widget=forms.DateInput(
                        placeholder=self.field.placeholder_text, attrs=field_using
                    )
                )

        # ChoiceField

        if self.field and self.field_class == form_settings.RadioChoiceField:
            args.update(choices=tuple(self.field.choices.values_list("id", "value")))
            args.update(
                widget=forms.RadioSelect(attrs=field_using)
                # widget=CustomRadioWidget(attrs=field_using)
            )

        if self.field and self.field_class == form_settings.ChoiceField:
            args.update(choices=tuple(self.field.choices.values_list("id", "value")))
        elif self.field_class == form_settings.ChoiceField:
            args.update(choices=[])

        # MultipleChoiceField
        if self.field and self.field_class == form_settings.MultipleChoiceField:
            args.update(choices=tuple(self.field.choices.values_list("id", "value")))
            if False and self.field.widget == self.CHECKBOX_MULTIPLE:
                args.update(
                    # widget=forms.CheckboxSelectMultiple()
                    widget=CustomCheckboxSelectMultiple()
                )
        elif self.field_class == form_settings.MultipleChoiceField:
            args.update(choices=[])

        if self.field_class in [
            form_settings.DynamicChoice,
        ]:
            args.update(choices=get_class(self.field.get_dynamic_choice)(request, self))

        if self.field and self.field_class in [
            form_settings.ChoiceWithOtherField,
            form_settings.MultipleChoiceWithInputField,
        ]:
            args.update(
                choices=tuple(self.field.choices.values_list("id", "value", "trigger"))
            )
        elif self.field_class in [
            form_settings.ChoiceWithOtherField,
            form_settings.MultipleChoiceWithInputField,
        ]:
            args.update(choices=[])

        if self.field and self.field.extra_validator:
            extra_validators = args["validators"] if "validators" in args else []
            extra_validators.append(get_class(self.field.extra_validator))
            args.update(validators=extra_validators)
        return args

    def get_field_base(self):
        for base, fields in form_settings.FIELD_BASES.items():
            if self.field_class in fields:
                return base
        return form_settings.form_base

    def instantiate_field(self, token, request, update, is_formset, form_model):
        field_class = self.get_field_base() + self.field_class
        field = get_class(field_class)(
            **self.get_init_form_args(token, request, update, is_formset, form_model)
        )
        field.widget.above_field_paragraph = (
            self.field.above_field_paragraph if self.field else None
        )
        field.widget.below_field_paragraph = (
            self.field.below_field_paragraph if self.field else None
        )
        if self.field_class == form_settings.PhoneNumberField:
            field.default_error_messages["invalid"] = _(
                "The phone number entered is not valid. Please enter country code (+965 xxxxxxxxxx)"
            )

        return field


class AbstractFieldSerializerMixin(object):
    """
    This are all methods and attrs for FieldModel class which will provide
    all functionality to construct a DRF serializer. It is built to use
    reder_form templatetag.
    """

    def get_init_serializer_args(self):
        # set file fields required to False to allow async upload
        args = {
            "required": False
            if self.field_class in (form_settings.FileField, form_settings.ImageField)
            else self.required
        }

        if self.help_text:
            args.update(help_text=self.help_text)

        if self.label:
            args.update(label=self.label)

        if self.field_class in (
            form_settings.CharField,
            form_settings.EmailField,
            form_settings.RegexField,
        ):
            if self.max_length:
                args.update(max_length=self.max_length)
            if self.min_length:
                args.update(min_length=self.min_length)
        if self.field_class in (form_settings.IntegerField, form_settings.DecimalField):
            if self.max_value:
                args.update(max_value=Decimal(str(self.max_value)))
            if self.min_value:
                args.update(min_value=Decimal(str(self.min_value)))
        if self.field_class == form_settings.DecimalField:
            args.update(
                max_digits=self.max_digits or 10,
                decimal_places=self.decimal_places or 2,
            )
        if self.field_class == form_settings.DateField:
            args.update(input_formats=settings.DATE_INPUT_FORMATS)
            validators = []

            if self.min_date:
                validators.append(
                    MinValueValidator(
                        now().date() - timedelta(days=self.min_date),
                        message=self.min_date_message,
                    )
                )
            if self.max_date:
                validators.append(
                    MaxValueValidator(
                        now().date() + timedelta(days=self.max_date),
                        message=self.max_date_message,
                    )
                )

            if validators:
                args.update(validators=validators)

        if self.field_class == form_settings.RegexField:
            args.update(regex=self.regex or "")
        if self.field_class in (
            form_settings.ChoiceField,
            form_settings.MultipleChoiceField,
        ):
            args.update(choices=tuple(self.choices.values_list("id", "value")))

        if self.widget or self.placeholder_text:
            style = {"placeholder": self.placeholder_text, "input_type": self.widget}
            if self.widget == self.TEXTAREA:
                style.update(base_template="textarea.html")
                if self.rows is not None:
                    style.update(rows=self.rows)
            if self.widget == self.RADIO_BUTTON:
                style.update(base_template="radio.html")

            if self.widget == self.RADIO_BUTTON_INLINE:
                style.update(base_template="radio_inline.html")

            if self.widget == self.CHECKBOX_MULTIPLE:
                style.update(base_template="checkbox_multiple.html")

            if self.widget == self.CHECKBOX:
                style.update(base_template="checkbox.html")
            args.update(style=style)

        return args

    def instantiate_serializer(self):
        field_class = form_settings.serializer_base + self.field_class
        return get_class(field_class)(**self.get_init_serializer_args())


@python_2_unicode_compatible
class AbstractField(AbstractFieldSerializerMixin, models.Model):
    """
    Field for built-in form
    """

    (
        TEXT,
        TEXTAREA,
        RADIO_BUTTON,
        RADIO_BUTTON_INLINE,
        PASSWORD,
        HIDDEN,
        CHECKBOX,
        CHECKBOX_MULTIPLE,
        OPTIONAL_CHOICE,
    ) = (
        "text",
        "textarea",
        "radio",
        "radio_inline",
        "password",
        "hidden",
        "checkbox",
        "checkbox_multiple",
        "optional_choice",
    )

    WIDGET_CHOICES = (
        (TEXT, _("Text")),
        (TEXTAREA, _("Textarea")),
        (RADIO_BUTTON, _("Radio button")),
        (RADIO_BUTTON_INLINE, _("Radio button inline")),
        (PASSWORD, _("Password")),
        (CHECKBOX, _("Checkbox")),
        (CHECKBOX_MULTIPLE, _("Checkbox Multiple")),
        (OPTIONAL_CHOICE, _("Optional Choice")),
        (HIDDEN, _("Hidden")),
    )

    display_name = models.CharField(
        _("name"), max_length=64, default=""
    )  # just for display
    widget = models.CharField(
        _("Widget"), choices=WIDGET_CHOICES, max_length=34, default="", blank=True
    )

    help_text = models.CharField(
        _("Help text"), blank=True, max_length=form_settings.HELP_TEXT_MAX_LENGTH
    )
    rows = models.PositiveSmallIntegerField(
        _("Rows"), null=True, blank=True, help_text=_("Number of rows for textarea")
    )
    regex = models.CharField(
        _("Regular expression"),
        max_length=255,
        blank=True,
        null=True,
        validators=[validate_regex_expr],
    )
    placeholder_text = models.CharField(
        _("Placeholder Text"), default="", blank=True, max_length=100
    )
    initial = models.TextField(_("Initial value"), blank=True, null=True)
    max_length = models.IntegerField(_("Max. length"), blank=True, null=True)
    min_length = models.IntegerField(_("Min. length"), blank=True, null=True)
    max_value = models.FloatField(_("Max. value"), blank=True, null=True)
    min_value = models.FloatField(_("Min. value"), blank=True, null=True)
    max_date = models.PositiveSmallIntegerField(
        _("Max. date"),
        blank=True,
        null=True,
        help_text=_(
            "This will define the max date limit counted in days. "
            "This will be calculated from current date."
        ),
    )
    max_date_message = models.CharField(
        _("Max date error message"),
        max_length=200,
        blank=True,
        null=True,
        help_text=_("This is the error message shown for max date validator."),
    )
    min_date = models.PositiveSmallIntegerField(
        _("Min. date"),
        blank=True,
        null=True,
        help_text=_(
            "This will define the min date limit counted in days. "
            "This will be calculated from current date."
        ),
    )
    min_date_message = models.CharField(
        _("Min date error message"),
        max_length=200,
        blank=True,
        null=True,
        help_text=_("This is the error message shown for min date validator."),
    )
    max_digits = models.IntegerField(_("Max. digits"), blank=True, null=True)
    decimal_places = models.IntegerField(_("Decimal places"), blank=True, null=True)
    min_size = models.FloatField(
        _("Min. file size"),
        blank=True,
        null=True,
        help_text=_("Minimum size of the uploaded file in MB."),
    )
    max_size = models.FloatField(
        _("Max. file size"),
        blank=True,
        null=True,
        help_text=_("Maximum size of the uploaded file in MB."),
        default=2.50,
    )
    allowed_file_types = models.TextField(
        _("Allowed file types"),
        default="",
        blank=True,
        help_text=_(
            "Provide a separated comma list of file extensions. IE: "
            "doc, docx, xls, xlsx, zip, pdf, odt, png, gif."
        ),
    )
    send_success_email = models.BooleanField(
        default=False,
        help_text=_(
            "If checked, an email will be sent "
            "to an email address from value of this field."
        ),
    )
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    above_field_paragraph = RichTextField(
        blank=True, null=True, verbose_name=_("Paragraph above field")
    )
    below_field_paragraph = RichTextField(
        blank=True, null=True, verbose_name=_("Paragraph below field")
    )

    extra_validator = models.CharField(
        max_length=190,
        choices=form_settings.FIELD_EXTRA_VALIDATOR,
        null=True,
        blank=True,
        verbose_name=_("Extra Validators"),
    )
    get_dynamic_choice = models.CharField(
        max_length=190,
        choices=form_settings.GET_FIELD_CHOICE_DYNAMIC,
        null=True,
        blank=True,
        verbose_name=_("Get Field Choice Dynamic"),
    )

    objects = FieldManager()

    def __str__(self):
        return self.display_name

    def has_choices(self):
        return self.choices.all().exists()

    class Meta:
        verbose_name = _("Field")
        verbose_name_plural = _("Fields")
        abstract = True

    def save(self, *args, **kwargs):
        # self.name = snake_case(self.name)
        super(AbstractField, self).save(*args, **kwargs)


# FieldModel = deferred.MaterializedModel(AbstractField)


@python_2_unicode_compatible
class AbstractChoice(Sortable):
    field = models.ForeignKey(
        "form_builder.Field",
        verbose_name=_("field"),
        related_name="choices",
        on_delete=models.CASCADE,
    )
    value = models.CharField(_("Value"), max_length=form_settings.LABEL_MAX_LENGTH)
    trigger = models.BooleanField(_("Trigger"), default=False)
    trigger_field = models.ManyToManyField(
        "form_builder.FormField",
        verbose_name=_("Trigger field"),
        related_name="trigger_field",
        blank=True,
    )
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        verbose_name = _("Choice")
        verbose_name_plural = _("Choices")
        ordering = ("order",)
        abstract = True

    def __str__(self):
        return "{} - {}".format(self.id, self.value)


# ChoiceModel = deferred.MaterializedModel(AbstractChoice)
