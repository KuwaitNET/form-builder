# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.db import models

from form_builder.settings import FORK

FORK = False

if not FORK:
    from .config import AbstractFormVerificationCode, AbstractEmailTemplate

    from .field import AbstractField, AbstractChoice
    from .entry import (
        AbstractFieldEntry,
        AbstractFormEntry,
        AbstractFormSubmit,
        AbstractEmailLog,
        AbstractFieldEntryExtras,
    )
    from .redirect import AbstractCondition, AbstractConditionalRedirect
    from .form import (
        AbstractForm,
        AbstractFormField,
        AbstractFormType,
        PDFForFormAbstract,
    )
    from .workflow import (
        AbstractWorkflow,
        AbstractReSubmitFields,
        AbstractWorkflowProgress,
    )

    class EmailTemplate(AbstractEmailTemplate):
        class Meta:
            verbose_name = _("Email Template")
            verbose_name_plural = _("Email Templates")

    class FormEntry(AbstractFormEntry):
        class Meta:
            verbose_name = _("Form entry")
            verbose_name_plural = _("Form entries")
            ordering = ("-created", "modified")

    class FormSubmit(AbstractFormSubmit):
        class Meta:
            verbose_name = _("Form submit")
            verbose_name_plural = _("Form submits")
            ordering = ("-created", "modified")

    class Form(AbstractForm):
        class Meta:
            verbose_name = _("Form")
            verbose_name_plural = _("Forms")
            ordering = ["path", "-modified"]

    class PDFForForm(PDFForFormAbstract):
        pass

    class FormType(AbstractFormType):
        class Meta:
            verbose_name = _("Form Type")
            verbose_name_plural = _("Form Types")

    class FormField(AbstractFormField):
        class Meta:
            verbose_name = _("Form Field")
            verbose_name_plural = _("Form Fields")
            ordering = ["order"]

    class Field(AbstractField):
        class Meta:
            verbose_name = _("Field Setting")
            verbose_name_plural = _("Field Settings")

    class Choice(AbstractChoice):
        class Meta:
            verbose_name = _("Choice")
            verbose_name_plural = _("Choices")
            ordering = ("order",)

    class FieldEntry(AbstractFieldEntry):
        class Meta:
            verbose_name = _("Field entry")
            verbose_name_plural = _("Field entries")
            ordering = ("-created", "modified")

    class Condition(AbstractCondition):
        class Meta:
            verbose_name = _("condition")
            verbose_name_plural = _("conditions")

    class ConditionalRedirect(AbstractConditionalRedirect):
        class Meta:
            verbose_name = _("conditional redirect")
            verbose_name_plural = _("conditional redirect")

    class EmailLog(AbstractEmailLog):
        class Meta:
            verbose_name = _("Email Log")
            verbose_name_plural = _("Email Logs")

    class FormVerificationCode(AbstractFormVerificationCode):
        class Meta:
            verbose_name = _("Participant Information")
            verbose_name_plural = _("Participant Information")

    class Workflow(AbstractWorkflow):
        class Meta:
            verbose_name = _("Workflow")
            verbose_name_plural = _("Workflows")
            ordering = ("path",)

    class ReSubmitFields(AbstractReSubmitFields):
        pass

    class WorkflowProgress(AbstractWorkflowProgress):
        pass

    class FieldEntryExtras(AbstractFieldEntryExtras):
        pass
