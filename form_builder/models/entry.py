from __future__ import unicode_literals

import ast

from django.conf import settings
from django.apps import apps
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from django.utils.module_loading import import_string

try:
    from django.shortcuts import reverse
except ImportError:
    from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Q

try:
    from django.utils.encoding import python_2_unicode_compatible
except ImportError:
    from django.utils.six import python_2_unicode_compatible
from django.utils.html import format_html
from django.utils.functional import cached_property
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _, get_language_from_request

try:
    from ipware.ip import get_ip
except ImportError:
    from ipware.ip import get_client_ip as get_ip
import jsonfield

try:
    from django.utils.encoding import force_text
except ImportError:
    from django.utils.text import force_text

from form_builder import settings as form_settings
from form_builder.signals import after_submit_signal
from form_builder.utils import get_upload_path, reverse_no_i18n


class FieldEntryQuerySet(models.QuerySet):
    def fields(self):
        return self.exclude(form_field__field_class__in=form_settings.FILE_CLASSES)

    def files(self):
        return self.filter(form_field__field_class__in=form_settings.FILE_CLASSES)

    def required(self):
        return self.filter(form_field__required=True)


class FormEntryManager(models.Manager):
    def create_for_form(self, form, token, submitter, request):
        return self.create(
            form=form,
            submitter=submitter,
            token=token,
            stored_request=self.stored_request(request),
        )

    def stored_request(self, request):
        """
        Extract useful information about the request to be used for emulating
        a Django request during offline rendering.
        """
        return {
            "language": get_language_from_request(request),
            "absolute_base_uri": request.build_absolute_uri("/"),
            "remote_ip": get_ip(request),
            "user_agent": request.META.get("HTTP_USER_AGENT"),
        }


class Status(object):
    PENDING = "PENDING"
    COMPLETE = "COMPLETE"
    INCOMPLETE = "INCOMPLETE"
    APPROVED = "APPROVED"
    REJECTED = "REJECTED"

    CHOICES = (
        (PENDING, PENDING),
        (COMPLETE, COMPLETE),
        (INCOMPLETE, INCOMPLETE),
        (APPROVED, APPROVED),
        (REJECTED, REJECTED),
    )


@python_2_unicode_compatible
class AbstractFormSubmit(models.Model):
    approved = models.NullBooleanField(
        _("Approved?"), help_text=_("Required if the parent form requires approval.")
    )
    under_moderation = models.BooleanField(
        _("Under moderation"),
        default=False,
        help_text=_("Checked if the form entry is under moderation."),
    )
    entry_time = models.DateTimeField(_("Date/time"), null=True, blank=True)
    submitted = models.BooleanField(_("Submitted"), default=False)
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)
    submitter_user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        verbose_name=_("Submitter"),
        null=True,
        blank=True,
        help_text=_("User who submitted the form"),
    )
    workflow_state = models.ForeignKey(
        "form_builder.Workflow", null=True, blank=True, on_delete=models.SET_NULL
    )
    form = models.ForeignKey(
        "form_builder.Form", null=True, blank=True, on_delete=models.SET_NULL,
    )
    CANCEL_STATUS = (
        ("cancel_request", _("Cancel Request")),
        ("approve_cancel", _("Cancel is Approved")),
        ("reject_cancel", _("Cancel is Rejected")),
    )
    cancel_status = models.CharField(
        _("Cancel Status"), max_length=50, choices=CANCEL_STATUS, null=True, blank=True
    )

    def __str__(self):
        return "{}".format(self.form,)

    def get_workflow_state(self):
        return self.get_workflow().state if self.get_workflow() else None

    def get_workflow(self):
        if self.form and (self.form.workflow or self.form.get_root().workflow):
            # if self.workflow_state and self.workflow_state.next_step:
            if self.workflow_state:
                return self.workflow_state
            # elif self.workflow_state:
            #     return self.workflow_state
            else:
                workflow = self.form.workflow or self.form.get_root().workflow
                return (
                    workflow.get_children()[0]
                    if workflow and len(workflow.get_children()) > 0
                    else None
                )

    def is_sent_resubmit(self):
        theReturn = False
        workflow = self.get_workflow()
        if workflow and workflow.is_sent_resubmit:
            theReturn = True

        if workflow and not theReturn:
            for a_workflow in workflow.get_children():
                if a_workflow.is_sent_resubmit:
                    theReturn = True
        return theReturn

    @cached_property
    def get_token(self):
        return self.entries.first().token if self.entries.exists() else None

    @cached_property
    def submitter(self):
        form_entry = self.entries.first()
        return form_entry.get_submitter() if form_entry else None

    def get_export_to_pdf_url(self):
        return reverse("form_builder:submit_to_pdf", args=(self.pk,))

    def get_export_to_excel_url(self):
        return reverse(
            "form_builder:export_all", args=(self.form.get_root().slug,)
        ) + "?ids={}".format(self.id)

    def get_download_attachments_url(self):
        return reverse("form_builder:download_attachments", args=(self.pk,))

    class Meta:
        abstract = True

    def reject(self):
        self.submitted = False
        self.save()

    def after_submit(self, form, request):
        after_submit_signal.send(
            sender=self, **{"request": request, "form": form, "submit": self}
        )

    def submit(self, request, additional_ctx=None):
        if self.submitted:
            return
        email_template = "form_submission_admin"
        entry = self.entries.latest("created")
        form = entry.form
        ctx = {"form": form, "entry": self}
        if additional_ctx is not None:
            ctx.update(additional_ctx)
        emails = list()

        # # send mails

        # update object
        self.entry_time = now()
        self.submitted = True
        self.save()

    def get_submit_data(self, request=None):
        from form_builder.views.utils import get_submit_data

        (
            all_forms_name,
            all_entry_forms,
            initial_values,
            initial_values_dispaly,
            initial_values_dispaly_by_slug,
            initial_values_dispaly_by_slug_entry_id,
            all_entry_forms_entry_obj,
        ) = get_submit_data(self, request)
        return (
            all_forms_name,
            all_entry_forms,
            initial_values,
            initial_values_dispaly,
            initial_values_dispaly_by_slug,
            initial_values_dispaly_by_slug_entry_id,
            all_entry_forms_entry_obj,
        )


# FormSubmitModel = deferred.MaterializedModel(AbstractFormSubmit)


@python_2_unicode_compatible
class AbstractFormEntry(models.Model):
    """
    An entry submitted via a user-built form.

    * Submit method will be called when all forms are completed.
    """

    form = models.ForeignKey(
        "form_builder.Form",
        related_name="entries",
        on_delete=models.SET_NULL,
        null=True,
    )
    parent = models.ForeignKey(
        "self",
        on_delete=models.SET_NULL,
        verbose_name=_("Parent form"),
        blank=True,
        null=True,
    )
    form_submit = models.ForeignKey(
        "form_builder.FormSubmit",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="entries",
        help_text=_(
            "Form submit object is being created when all entries "
            "needed for the form to be completed are created."
        ),
    )
    submitter = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        verbose_name=_("Submitter"),
        null=True,
        blank=True,
        help_text=_("User who submitted the form"),
    )
    token = models.UUIDField(
        _("Token"),
        null=True,
        blank=True,
        editable=False,
        help_text=_(
            "Token unique for a set of form entries or for a single"
            "instance of a form. Required for identity purpose."
        ),
    )
    stored_request = jsonfield.JSONField(
        default={},
        help_text=_("Parts of the Request objects on the moment of " "submission."),
    )
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)
    # is_canceled = models.BooleanField(default=False)

    objects = FormEntryManager()

    def __str__(self):
        if self.form_submit:
            return "Entry for [{}] - [{}]".format(self.form, self.form_submit.pk)
        return "Entry for [{}]".format(self.form)

    def get_submitter(self):
        # return self.submitter or self.token
        return self.submitter or _("Non Registered User")

    class Meta:
        verbose_name = _("Form entry")
        verbose_name_plural = _("Form entries")
        abstract = True
        ordering = ["form__path"]

    @property
    def has_submission(self):
        return True if self.form_submit else False

    def as_serializer_instance_base(self, skip_file_initial=False):
        # TODO: change to ORM only
        default_media_storage = import_string(settings.DEFAULT_FILE_STORAGE)
        instances = self.field_entries.values_list("form_field__slug", "value", "file")

        if skip_file_initial:
            the_return = {
                field_name: value if not the_file else None
                for field_name, value, the_file in instances
            }
        else:
            the_return = {
                field_name: value
                if not the_file
                else default_media_storage().url(name=the_file)
                for field_name, value, the_file in instances
            }

        FieldEntryExtras = apps.get_model(
            app_label="form_builder", model_name="FieldEntryExtras"
        )
        for extra in FieldEntryExtras.objects.filter(field_entry__entry=self):
            if extra.choice:
                the_return[
                    "{}_extraEntry_{}".format(
                        extra.field_entry.form_field.slug, extra.choice.id
                    )
                ] = extra.value
        return the_return

    @cached_property
    def as_serializer_instance():
        return self.as_serializer_instance_base()

    @cached_property
    def all_entries(self):
        """
        :return: Queryset with all entries related to the form/wizard
        """
        FormEntry = apps.get_model(app_label="form_builder", model_name="FormEntry")
        qs = FormEntry.objects.filter(form_submit=self.form_submit)
        return qs.filter(Q(submitter=self.submitter) & Q(token=self.token))

    def can_be_submitted(self):
        return self.all_entries.count() == self.form.count_required_steps() + 1

    def get_fields_to_dict(self):
        """
        Returns all fields(name/value) pairs into a dictionary.
        Does not return file values
        :return: dict
        """
        fields = self.field_entries.filter(file="")
        return dict(fields.values_list("form_field__slug", "value"))

    def get_absolute_submission_url_no_i18n(self):
        return reverse_no_i18n(
            "form_builder:dashboard:submission_form_detail",
            args=(self.form.slug, self.form_submit.id),
        )


# FormEntryModel = deferred.MaterializedModel(AbstractFormEntry)


@python_2_unicode_compatible
class AbstractFieldEntry(models.Model):
    """
    A single field value for a form entry submitted via a user-built form.
    """

    form = models.ForeignKey(
        "form_builder.Form",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="field_entries",
    )
    entry = models.ForeignKey(
        "form_builder.FormEntry",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="field_entries",
    )
    formset_order = models.PositiveSmallIntegerField(
        _("Formset order number"),
        help_text=_("used internally, don't touch"),
        blank=True,
        null=True,
    )
    formset_id = models.CharField(
        _("Formset ID"),
        help_text=_("used internally, don't touch"),
        null=True,
        blank=True,
        max_length=120,
    )
    # TODO: store the field name for history if field gets deleted
    field_name = models.CharField(
        _("Field name"), max_length=200, blank=True, default=""
    )
    # field = models.ForeignKey(  # TODO should be removed
    #     'form_builder.Field',
    #     null=True,
    #     on_delete=models.SET_NULL,
    #     related_name='entries'
    # )
    form_field = models.ForeignKey(
        "form_builder.FormField",
        null=True,
        on_delete=models.SET_NULL,
        related_name="entries",
    )
    value = models.CharField(
        max_length=form_settings.FIELD_MAX_LENGTH, blank=True, default=""
    )
    file = models.FileField(_("File"), upload_to=get_upload_path, null=True, blank=True)
    submitter = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        verbose_name=_("Submitter"),
        null=True,
        blank=True,
        help_text=_("User who submitted the form"),
    )
    token = models.UUIDField(
        _("Token"),
        null=True,
        blank=True,
        editable=False,
        help_text=_(
            "Token unique for a set of form entries or for a single"
            "instance of a form. Required for identity purpose."
        ),
    )
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)
    objects = FieldEntryQuerySet.as_manager()

    def __str__(self):
        return "{} - {}".format(self.field_name, self.value)

    class Meta:
        abstract = True
        verbose_name = _("Field entry")
        verbose_name_plural = _("Field entries")

    def get_value(self):
        if self.form_field.field and self.form_field.field.has_choices():

            if ";" in self.value:
                choice_id, text = self.value.split("; ")
                try:
                    return "%s <br> %s" % (
                        self.form_field.field.choices.get(pk=choice_id).value,
                        text,
                    )
                except ObjectDoesNotExist:
                    return self.value
            if self.value and self.value != "NULL":
                try:
                    choices = ast.literal_eval(self.value)
                    choices = (
                        [choice for choice in choices if str(choice) != ""]
                        if type(choices) is list
                        else choices
                    )
                except SyntaxError:
                    return self.value
                except ValueError:
                    return "-"
                try:
                    # if self.form_field.field_class == "MultipleChoiceWithInputField":
                    #     theReturn = []
                    #     for choice in choices:
                    #         value = self.form_field.field.choices.filter(pk=choice).values_list('id', flat=True)[0]
                    #         theText = self.field_entry_extra.filter(choice__value=value).last().value if self.field_entry_extra.filter(choice__value=value).exists() else ""
                    #         theReturn.append([str(value), theText])
                    #     print("BBBBBBBBBBBBBBBBBBBBBBBBBBBBB, ", theReturn)
                    #     return theReturn

                    values = self.form_field.field.choices.filter(
                        pk__in=choices
                    ).values_list("value", flat=True)
                    return ", ".join(["%s" % value for value in values])
                except TypeError:
                    try:
                        return self.field.choices.get(pk=choices).value
                    except:
                        return self.value
        if self.file and hasattr(self.file, "url"):
            return "<a target='_blank' href='{0}'>{1}</a>".format(
                self.file.url, self.file.name.split("/")[-1]
            )
        return self.value

    def get_excel_value(self):
        if (
            self.form_field
            and self.form_field.field
            and self.form_field.field.has_choices()
        ):
            if ";" in self.value:
                choice_id, text = self.value.split("; ")
                try:
                    return "%s : %s" % (
                        self.form_field.field.choices.get(pk=choice_id).value,
                        text,
                    )
                except ObjectDoesNotExist:
                    return self.value
            if self.value and self.value != "NULL":
                try:
                    choices = [ast.literal_eval(self.value)]
                except SyntaxError:
                    return self.value
                except ValueError:
                    return "-"
                try:
                    return_value = []
                    for a_value in self.form_field.field.choices.filter(
                        pk__in=[int(choice) for choice in choices if choice != ""]
                    ):

                        single_value = []
                        single_value.append("%s" % a_value.value)
                        extras = self.field_entry_extra.filter(choice=a_value)
                        if extras.exists():
                            extra = extras.first()
                            single_value.append("%s" % extra.value)
                        return_value.append(" -- ".join(single_value))
                    return " , ".join(return_value)
                except TypeError:
                    return self.form_field.field.choices.get(pk=choices).value
        if self.file and hasattr(self.file, "url"):
            return "{}".format(self.file.url,)
        return self.value

    def get_html_value(self):
        if (
            self.form_field
            and self.form_field.field
            and self.form_field.field.has_choices()
        ):

            if ";" in self.value:
                choice_id, text = self.value.split("; ")
                try:
                    return format_html(
                        "%s <br> %s"
                        % (self.form_field.field.choices.get(pk=choice_id).value, text)
                    )
                except ObjectDoesNotExist:
                    return format_html(self.value)
            if self.value and self.value != "NULL":
                try:
                    choices = ast.literal_eval(self.value)
                except SyntaxError:
                    return format_html(self.value)
                except ValueError:
                    return format_html("-")
                try:
                    choices = choices if isinstance(choices, list) else [choices]
                    return_value = []
                    for a_value in self.form_field.field.choices.filter(
                        pk__in=[int(choice) for choice in choices if choice != ""]
                    ):

                        single_value = []
                        single_value.append("%s" % a_value.value)
                        extras = self.field_entry_extra.filter(choice=a_value)
                        if extras.exists():
                            extra = extras.first()
                            single_value.append("%s" % extra.value)
                        return_value.append(" -- ".join(single_value))
                    return format_html(" <br> ".join(return_value))
                except TypeError:
                    return format_html(
                        str(self.value)
                        + " - "
                        + force_text(_("Value is not in the choices!!!!"))
                    )

        if self.file and hasattr(self.file, "url"):
            return format_html(
                "<a target='_blank' href='{0}'>{1}</a>".format(
                    self.file.url, self.file.name.split("/")[-1]
                )
            )
        return self.value


@python_2_unicode_compatible
class AbstractEmailLog(models.Model):
    """
    This is a record of all emails sent to a user.
    Normally, we only record order-related emails.
    """

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="form_builder_emails",
        verbose_name=_("User"),
    )
    subject = models.TextField(_("Subject"), max_length=255)
    body_html = models.TextField(_("Body HTML"), blank=True)
    date_sent = models.DateTimeField(_("Date Sent"), auto_now_add=True)

    class Meta:
        abstract = True
        verbose_name = _("Email Log")
        verbose_name_plural = _("Email Logs")

    def __str__(self):
        return _("Email to %(user)s with subject '%(subject)s'") % {
            "user": self.user.get_username(),
            "subject": self.subject,
        }


@python_2_unicode_compatible
class AbstractFieldEntryExtras(models.Model):
    """
    A single field value for a form entry submitted via a user-built form.
    """

    field_entry = models.ForeignKey(
        "form_builder.FieldEntry",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="field_entry_extra",
    )
    resubmission_field_entry = models.ForeignKey(
        "form_builder.ReSubmitFields",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="resubmission_field_entry_extra",
    )
    choice = models.ForeignKey(
        "form_builder.Choice",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="field_choice_entry_extra",
    )
    value = models.CharField(
        max_length=form_settings.FIELD_MAX_LENGTH, blank=True, default=""
    )
    file = models.FileField(_("File"), upload_to=get_upload_path, null=True, blank=True)
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    def __str__(self):
        return "{} - {}".format(self.field_entry.field_name, self.value)

    class Meta:
        abstract = True
        verbose_name = _("Field entry Extra")
        verbose_name_plural = _("Field Extras")
