from __future__ import unicode_literals

from django.conf.urls import url, include

try:
    from django.views.i18n import JavaScriptCatalog
except:
    from django.views.i18n import javascript_catalog

from . import views
from .constants import FORM_PATH_REGEX


urlpatterns = [
    url(r"^api/", include(("form_builder.api.urls", "form_builder"), namespace="api")),
    url(
        r"^success/{}/(?P<pk>\d+)/$".format(FORM_PATH_REGEX),
        views.SuccessURL.as_view(),
        name="success",
    ),
    url(
        r"^not_available/(?P<pk>\d+)/$",
        views.NotAvailable.as_view(),
        name="not_available",
    ),
    # TODO: Update this view for non-auth users. Currently there is no
    # middleware to solve this auth user.
    url(
        r"^create/{}/$".format(FORM_PATH_REGEX),
        views.FormModelView.as_view(),
        name="create",
    ),
    url(
        r"^resubmit/(?P<pk>\d+)/$",
        views.ResubmitFormModelView.as_view(),
        name="resubmit_view",
    ),
    url(r"^jsi18n/$", JavaScriptCatalog.as_view(), name="javascript-catalog"),
    url(
        r"^submit/(?P<pk>\d+)/export/pdf/$",
        views.SubmitToPDFView.as_view(),
        name="submit_to_pdf",
    ),
    url(
        r"^export/(?P<pk>\d+)/download-attachments/zip/$",
        views.DownloadSubmitAttachmentsView.as_view(),
        name="download_attachments",
    ),
    url(
        r"^cancel_submission_status/$",
        views.cancel_submission_status,
        name="cancel_submission_status",
    ),
    url(r"^print_submit/(?P<pk>\d+)/$", views.print_submission, name="print_submit"),
    url(
        r"^submission_admin/{}/form/(?P<pk>\d+)/detail/$".format(FORM_PATH_REGEX),
        views.SubmissionFormDetailAdmin.as_view(),
        name="submission_form_detail_admin",
    ),
    url(
        r"^export/{}/form/$".format(FORM_PATH_REGEX),
        views.ExportAllView.as_view(),
        name="export_all",
    ),
    url(
        r"^charts/{}/form/$".format(FORM_PATH_REGEX),
        views.ChartsFormView.as_view(),
        name="charts_form",
    ),
    url(
        r"^charts2/{}/form/$".format(FORM_PATH_REGEX),
        views.Charts2FormView.as_view(),
        name="charts2_form",
    ),
]
