# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _

from openpyxl import Workbook
from openpyxl.compat import range
from openpyxl.cell import get_column_letter
from openpyxl.writer.excel import save_virtual_workbook

# https://bitbucket.org/openpyxl/openpyxl/issues/657/save_virtual_workbook-generates-junk-data
# http://stackoverflow.com/questions/16016039/django-openpyxl-saving-workbook-as-attachment

from form_builder import settings as form_settings

from form_builder.models import FormSubmit, Field
from form_builder.utils import snake_case


class Export(object):
    format = "xlsx"

    def __init__(self, form_submit, format=None):
        self.form_submit = form_submit
        self.format = format or self.format

    def get_filename(self):
        return "{form}.{format}".format(
            form=snake_case(self.form_submit.form.name), format=self.format
        )

    def export(self):
        wb = Workbook()
        dest_filename = self.get_filename()
        ws = wb.active
        ws.title = _("Entries")
        field_ids = FormSubmit.entries.exclude(
            entries__form_field__field_class__in=form_settings.FILE_CLASSES
        ).values_list("entries__field__id", flat=True)
        ws.append(list(Field.objects.filter(id__in=list(field_ids))))
        data = FormSubmit.entries.all().values_list("field")
