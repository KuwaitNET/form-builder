# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import datetime
from collections import OrderedDict
import json

from django.conf import settings
from django.http import JsonResponse

try:
    from django.shortcuts import reverse
except ImportError:
    from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import Http404
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404
from django.views import generic
from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import SafeString

import plotly
import plotly.graph_objs as go

from form_builder.models import (
    FormSubmit,
    ReSubmitFields,
    FormField,
    Choice,
    FormEntry,
    FieldEntryExtras,
    Form,
    FieldEntry,
)
from form_builder.forms import form_factory
from form_builder import settings as form_settings

# from form_builder.utils import get_submit_data
from .utils import get_submit_data
from .mixins import FormURLMixin, BaseFormMixin
from ..utils import get_submitter, user_is_authenticated, get_class


class FormModelView(FormURLMixin, BaseFormMixin, generic.edit.ProcessFormView):
    template_name_field = "page_template"
    context_object_name = "form"
    login_url = "/admin/login"
    custom_next_url = None

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=self.get_queryset())
        if self.object.is_formset:
            return HttpResponseRedirect(self.object.get_parent().get_absolute_url())

        if self.object.get_root().is_available_script and not get_class(
            self.object.get_root().is_available_script
        )(self.object, request):
            return HttpResponseRedirect(
                reverse("form_builder:not_available", args=[self.object.id])
            )

        if (
            self.object.get_root().entry_condition == "sequential"
            and user_is_authenticated(request.user)
        ):
            form_entry = FormEntry.objects.filter(
                submitter=get_submitter(request),
                form=self.object,
                form_submit__isnull=False,
            )
            if (
                form_entry.exists()
                and (
                    form_entry.first().form_submit.created
                    + datetime.timedelta(days=form_entry.first().form.days_next_submit)
                ).date()
                > datetime.date.today()
            ):
                return HttpResponseRedirect(
                    reverse(
                        "form_builder:success",
                        args=[
                            self.object.get_root().url_path,
                            form_entry.first().form_submit.pk,
                        ],
                    )
                )

        if self.object.get_root().entry_condition == "single" and user_is_authenticated(
            request.user
        ):
            form_entry = FormEntry.objects.filter(
                submitter=get_submitter(request),
                form=self.object,
                form_submit__isnull=False,
            )
            if form_entry.exists() and user_is_authenticated(request.user):
                return HttpResponseRedirect(
                    reverse(
                        "form_builder:success",
                        args=[
                            self.object.get_root().url_path,
                            form_entry.first().form_submit.pk,
                        ],
                    )
                )

        return super(FormModelView, self).get(request, *args, **kwargs)

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=self.get_queryset())
        if self.object.get_root().login_required and not user_is_authenticated(
            request.user
        ):
            return HttpResponseRedirect(self.get_login_url())
        response = super(FormModelView, self).dispatch(request, *args, **kwargs)
        return response

    def get_login_url(self):
        return getattr(settings, "LOGIN_URL", self.login_url)

    def get_queryset(self):
        return Form.objects.get_for_request(self.request)

    def get_context_data(self, **kwargs):
        context = super(FormModelView, self).get_context_data(**kwargs)
        if self.object.custom_action_at_creation:
            get_class(self.object.custom_action_at_creation)(self.object, self.request)
        exclude_form_data = self.object.exclude_form_list(
            get_submitter(self.request), self.token
        )
        total_forms = Form.get_tree(self.object.get_root()).active()
        q = Q()
        for exclude_form in exclude_form_data:
            q |= Q(id=exclude_form.id)
        total_forms = total_forms.exclude(q | Q(is_formset=True))
        if self.object:
            form_template = getattr(self.object, "form_template", None)

        exclude_form_list = (
            self.object.exclude_form_list(submitter=self.request.user, token=self.token)
            if self.request.user.is_authenticated
            else None
        )
        context.update(
            {
                "form_tree": total_forms,
                "exclude_form_list": exclude_form_list,
                "progress": self.object.calculate_progress(
                    self.request, total_forms, self.token
                ),
                "user_entries": self.object.get_forms_entires(self.request, self.token),
                "form_template": form_template,
            }
        )

        initial = {}

        if self.object.entry_condition in [
            "multiple",
            "sequential",
        ] and user_is_authenticated(self.request.user):
            get_entries = FormEntry.objects.filter(
                form=self.object, submitter=get_submitter(self.request)
            )
            if get_entries:
                initial.update(
                    get_entries.first().as_serializer_instance_base(
                        skip_file_initial=True
                    )
                )

        # for hidden fields, we get the initial value from all token entries (form entries),  # noqa
        # we need this to use field option trigger
        for form_field in self.object.fields.filter(
            hidden_by_default=True, unique_form_tree=True
        ):
            initial[form_field.slug] = (
                FieldEntry.objects.filter(
                    token=self.token, form_field__field=form_field.field
                )
                .first()
                .value
                if FieldEntry.objects.filter(
                    token=self.token, form_field__field=form_field.field
                ).exists()
                else None
            )

        context["form"].initial = initial
        return context


class ResubmitFormModelView(FormModelView):
    template_name_field = "resubmit_page_template"
    context_object_name = "form"
    login_url = "/admin/login"
    custom_next_url = None

    def get_context_data(self, **kwargs):
        ctx = super(ResubmitFormModelView, self).get_context_data(**kwargs)
        submission_id = self.kwargs.get("pk", None)
        ctx["submission_id"] = submission_id
        resubmission_form_tree = {}
        ctx["enable_resubmit"] = False
        if submission_id:
            submission = get_object_or_404(FormSubmit, id=int(submission_id))
            if submission.get_workflow() and submission.get_workflow().is_sent_resubmit:
                ctx["enable_resubmit"] = True

            for resubmission in ReSubmitFields.objects.filter(
                submission__id=submission_id
            ):
                if resubmission.form not in resubmission_form_tree:
                    resubmission_form_tree[resubmission.form] = []
                for resubmission_field in ReSubmitFields.objects.filter(
                    submission__id=submission_id, form=resubmission.form
                ):
                    if (
                        resubmission_field
                        not in resubmission_form_tree[resubmission.form]
                    ):
                        resubmission_form_tree[resubmission.form].append(
                            resubmission_field
                        )

        ctx["resubmission_form_tree"] = resubmission_form_tree
        exclude_form_data = self.object.exclude_form_list(
            get_submitter(self.request), self.token
        )
        total_forms = Form.get_tree(self.object.get_root()).active()
        q = Q()
        for exclude_form in exclude_form_data:
            q |= Q(id=exclude_form.id)
        total_forms = total_forms.exclude(q)
        total_form_tree = {}
        for form_object in total_forms:
            total_form_tree[form_object.id] = form_factory(
                form_model=form_object, token=self.token, request=self.request
            )

        ctx["total_form_tree"] = total_form_tree
        return ctx

    def new_get_object(self, queryset=None):
        return self.object

    def dispatch(self, request, *args, **kwargs):
        submission_id = kwargs.get("pk", None)
        if submission_id:
            submission = get_object_or_404(FormSubmit, id=submission_id)
            object = submission.form
            self.object = object.get_root() if object.get_root() else object

        setattr(self, "get_object", self.new_get_object)
        return super(ResubmitFormModelView, self).dispatch(request, *args, **kwargs)

    def process_file(self, request, submission):
        submitter = get_submitter(request)
        field_name = request.FILES.keys()[0].split("_resubmitprefix_")[0]
        field_name = request.FILES.keys()[0]
        field_entry_id = (
            request.FILES.keys()[0].split("_resubmitprefix_")[1]
            if len(request.FILES.keys()[0].split("_resubmitprefix_")) > 1
            else None
        )
        if field_entry_id:
            ReSubmitFields.objects.filter(
                submission=submission, field_entry__id=field_entry_id
            ).update(
                file=request.FILES.get(field_name),
                submitter=submitter,
            )
        return field_entry_id

    def post(self, request, *args, **kwargs):
        submission_id = kwargs.get("pk", None)
        if submission_id:
            submission = get_object_or_404(FormSubmit, id=submission_id)
            object = submission.form
            self.object = object.get_root() if object.get_root() else object
            setattr(self, "get_object", self.new_get_object)

            if request.FILES:
                pk = self.process_file(request, submission)
                return JsonResponse({"pk": pk})

            # The following loop to handle the fields themselves without having "_extraEntry_" in field name  # noqa
            for resubmission_field in submission.resubmitfields_set.all():
                if (
                    resubmission_field.field.field_class
                    in form_settings.FIELD_CLASS_WITH_SINGLE_CHOICE
                    + form_settings.FIELD_CLASS_WITH_MULTI_CHOICE
                ):
                    new_value = [
                        value
                        for key, value in request.POST.items()
                        if resubmission_field.field.name
                        == key.split("_resubmitprefix_")[0]
                        and "_extraEntry_" not in key
                    ]

                else:
                    new_value = request.POST.get(
                        resubmission_field.field.name
                        + "_resubmitprefix_"
                        + str(resubmission_field.field_entry.id),
                        None,
                    )

                resubmission_field.value = new_value
                resubmission_field.submitter = get_submitter(request)
                resubmission_field.save()

            # The following loop to handle the extra field name that have _extraEntry_
            for key, value in request.POST.items():
                # field_name = key.split('_resubmitprefix_')[0]
                field_entry_id = (
                    key.split("_resubmitprefix_")[1]
                    if len(key.split("_resubmitprefix_")) > 1
                    else None
                )
                if field_entry_id:
                    if "_extraEntry_" in field_entry_id:
                        real_field_entry_id = field_entry_id.split("_extraEntry_")[0]
                        choice_id = field_entry_id.split("_extraEntry_")[1]
                        choice_qst = Choice.objects.filter(id=choice_id)
                        choice = choice_qst.first() if choice_qst.exists() else None

                        resubmission_qst = ReSubmitFields.objects.filter(
                            submission__id=submission_id,
                            field_entry__id=int(real_field_entry_id),
                        )
                        resubmission = (
                            resubmission_qst.first()
                            if resubmission_qst.exists()
                            else None
                        )
                        if resubmission:
                            FieldEntryExtras(
                                resubmission_field_entry=resubmission,
                                choice=choice,
                                value=value,
                            ).save()

            for a_workflow in submission.get_workflow().get_tree(
                parent=submission.get_workflow().get_root()
            ):
                if a_workflow.is_state_after_resubmit:

                    submission.workflow_state = a_workflow
                    submission.save()
                    (
                        all_forms_name,
                        all_entry_forms,
                        initial_values,
                        initial_values_dispaly,
                        initial_values_dispaly_by_slug,
                        initial_values_dispaly_by_slug_entry_id,
                        all_entry_forms_entry_obj,
                    ) = get_submit_data(submission, self.request)

                    submission.workflow_state.exit_action(
                        ctx=initial_values_dispaly_by_slug_entry_id,
                        send_to=[submission.submitter_user.email],
                        submission=submission,
                        current_user=request.user,
                    )

                    break

            # response = super(AjaxResponseMixin, self).form_valid(form)
            if self.request.is_ajax():
                return JsonResponse(self.get_json_payload(redirect=True))
            # else:
            #     return response
            return super(ResubmitFormModelView, self).post(request, *args, **kwargs)

        return super(ResubmitFormModelView, self).post(request, *args, **kwargs)


class SuccessURL(FormURLMixin, generic.DetailView):
    template_name_field = "success_page_template"
    model = FormSubmit

    def get_object(self, queryset=None):
        try:
            form_submit = self.model.objects.get(
                pk=self.kwargs.get("pk", None),
            )
            if user_is_authenticated(
                self.request.user
            ) and form_submit.submitter == get_submitter(self.request):
                return form_submit
            entries = form_submit.entries.all().filter(
                pk__in=self.request.session.get("entry_pks", [])
            )
            if (
                entries.exists()
                and entries.first().get_submitter() == form_submit.submitter
            ):
                return form_submit
        except self.model.DoesNotExist:
            pass
        raise Http404()

    def get_context_data(self, **kwargs):
        ctx = super(SuccessURL, self).get_context_data(**kwargs)
        for entry in self.object.entries.all().filter(
            pk__in=self.request.session.get("entry_pks", [])
        ):
            if not entry.form.slug.replace("-", "_") in ctx:
                ctx[entry.form.slug.replace("-", "_")] = []

            this_entry = {"form_name": entry.form.name, "fields": {}}

            for field_entry in entry.field_entries.all():
                this_entry["fields"][field_entry.form_field.slug] = {
                    "label": field_entry.form_field.label,
                    "value": field_entry.get_value(),
                }

            ctx[entry.form.slug.replace("-", "_")].append(this_entry)

        return ctx

    def get_template_names(self):
        form = Form.objects.get(url_path=self.kwargs.get("path", None))
        return [form.success_page_template]


class NotAvailable(generic.DetailView):
    context_object_name = "form"
    model = Form

    def get_template_names(self):
        if self.object.get_root().not_available_template:
            return self.object.get_root().not_available_template


def cancel_submission_status(request):
    submit_id = request.GET.get("submit_id", None)
    action = request.GET.get("action", None)
    if submit_id and action and action in ["approve_cancel", "reject_cancel"]:
        submission = get_object_or_404(FormSubmit, id=int(submit_id))
        submission.cancel_status = action
        submission.save()
        if submission.form:
            send_to = [request.user.email] if request.user.email else []
            ctx = {
                "user": request.user,
                "submission": submission,
            }
            for theTemplate in submission.form.submit_cancel_email_template.all():
                theTemplate.send_mail(ctx=ctx, send_to=send_to or [])
        return HttpResponse("Done")
    if submit_id and action and action in ["cancel_request"]:
        submission = get_object_or_404(FormEntry, id=int(submit_id))
        if submission.submitter == request.user:
            if submission.form_submit:
                submission.form_submit.cancel_status = action
                submission.form_submit.save()
        if submission.form:
            send_to = [request.user.email] if request.user.email else []
            ctx = {
                "user": request.user,
                "submission": submission,
            }
            for theTemplate in submission.form.submit_cancel_email_template.all():
                theTemplate.send_mail(ctx=ctx, send_to=send_to or [])
        return HttpResponse("Done")
    return HttpResponse("Error", status=200)


def print_submission(request, pk):
    # obj = FormSubmit.objects.get(id=pk)
    obj = get_object_or_404(FormSubmit, id=pk)
    ctx = {}

    if obj.form.print_template:
        initial_values_dispaly = {}

        for a_from in FormEntry.objects.filter(token=obj.get_token):
            form_key = a_from.form.slug.replace("-", "_")
            if a_from.form.is_formset:
                formset_initial_values_dispaly = {}
                if form_key not in initial_values_dispaly:
                    initial_values_dispaly[form_key] = []
                for field_entry in a_from.field_entries.all():
                    if field_entry.field:
                        if field_entry.field.field_class == "ChoiceWithOtherField":
                            formset_initial_values_dispaly[
                                field_entry.field.name
                            ] = field_entry.get_value().split("<br>")
                        else:
                            formset_initial_values_dispaly[
                                field_entry.field.name
                            ] = field_entry.get_value()
                initial_values_dispaly[form_key].append(formset_initial_values_dispaly)
            else:
                initial_values_dispaly[form_key] = {}
                for field_entry in a_from.field_entries.all():
                    if field_entry.field:
                        if field_entry.field.field_class == "ChoiceWithOtherField":
                            initial_values_dispaly[form_key][
                                field_entry.field.name
                            ] = field_entry.get_value().split("<br>")
                        else:
                            initial_values_dispaly[form_key][
                                field_entry.field.name
                            ] = field_entry.get_value()

        ctx.update(initial_values_dispaly)
        ctx.update({"obj": obj})

        return render(request, obj.form.print_template, context=ctx)
    else:
        return HttpResponse(_("No template for this Form"))


class ChartsFormView(generic.TemplateView):
    template_name = "dashboard/charts.html"

    def get_context_data(self, **kwargs):
        # filter = FormSubmitFilter(self.request.GET,
        #                        queryset=FormSubmit.objects.filter(entries__form__slug=self.kwargs.get('path')))
        context = super(ChartsFormView, self).get_context_data(**kwargs)
        form_slug = self.kwargs.get("path", None)
        context["title"] = _("Submission of {}".format(form_slug))
        # context['filter'] = filter
        queryset = FormSubmit.objects.filter(entries__form__slug=form_slug)
        data = {}

        for index, obj in enumerate(queryset):
            for entry in obj.entries.all():
                for field_entry in entry.field_entries.all():
                    if (
                        entry.form.get_root().slug == form_slug
                        and field_entry.form_field
                        and field_entry.form_field.show_chart
                        and field_entry.form_field.field_class
                        in [
                            "ChoiceField",
                            "ChoiceWithOtherField",
                            "MultipleChoiceField",
                        ]
                        and field_entry.form_field.label not in data
                    ):
                        data.update({field_entry.form_field.label: None})
                        choices = {}
                        for choice in field_entry.form_field.field.choices.all():
                            choices.update({choice.value: 0})
                        data[field_entry.form_field.label] = choices
        for index, obj in enumerate(queryset):
            for entry in obj.entries.all():
                for field_entry in entry.field_entries.all():
                    try:
                        value = field_entry.get_value()
                        if (
                            entry.form.get_root().slug == form_slug
                            and field_entry.form_field
                            and field_entry.form_field.show_chart
                            and field_entry.form_field.field_class
                            in ["ChoiceField", "MultipleChoiceField"]
                            and value in data[field_entry.form_field.label]
                        ):
                            data[field_entry.form_field.label][
                                field_entry.get_value()
                            ] += 1
                        elif (
                            entry.form.get_root().slug == form_slug
                            and field_entry.form_field.field_class
                            == "ChoiceWithOtherField"
                        ):

                            for one in [data[field_entry.form_field.label].keys()][0]:
                                if one in value:
                                    data[field_entry.form_field.label][one] += 1
                    except Exception:
                        pass

        context["form_slug"] = form_slug
        ordered_data_list = []
        root_form = (
            Form.objects.filter(slug=form_slug).first()
            if Form.objects.filter(slug=form_slug).exists()
            else None
        )
        if root_form:
            for index1, a_form in enumerate(Form.get_tree(parent=root_form)):
                for index2, form_field in enumerate(
                    FormField.objects.filter(form=a_form, show_chart=True).order_by(
                        "order"
                    )
                ):
                    if form_field.label in data:
                        try:
                            label = (
                                str(int(len(ordered_data_list) + 1))
                                + ". "
                                + form_field.label
                            )
                            ordered_data_list.append((label, data[form_field.label]))
                        except Exception:
                            pass
        ordered_data = OrderedDict(ordered_data_list)
        context["data"] = SafeString(json.dumps(ordered_data))
        chart_type = "pie"
        if (
            self.request.GET.get("chart_type", None)
            and self.request.GET.get("chart_type", None) == "bar"
        ):
            chart_type = "bar"
        context["chart_type"] = chart_type
        return context


class Charts2FormView(generic.TemplateView):
    template_name = "dashboard/charts2.html"

    def get_context_data(self, **kwargs):
        # filter = FormSubmitFilter(self.request.GET,
        #                        queryset=FormSubmit.objects.filter(entries__form__slug=self.kwargs.get('path')))
        context = super(Charts2FormView, self).get_context_data(**kwargs)
        form_slug = self.kwargs.get("path", None)
        context["title"] = _("Submission of {}".format(form_slug))
        # context['filter'] = filter
        queryset = FormSubmit.objects.filter(entries__form__slug=form_slug)
        data = {}

        for index, obj in enumerate(queryset):
            for entry in obj.entries.all():
                for field_entry in entry.field_entries.all():
                    if (
                        entry.form.get_root().slug == form_slug
                        and field_entry.form_field
                        and field_entry.form_field.show_chart
                        and field_entry.form_field.field_class
                        in [
                            "ChoiceField",
                            "ChoiceWithOtherField",
                            "MultipleChoiceField",
                        ]
                        and field_entry.form_field.label not in data
                    ):
                        data.update({field_entry.form_field.label: None})
                        choices = {}
                        for choice in field_entry.form_field.field.choices.all():
                            choices.update({choice.value: 0})
                        data[field_entry.form_field.label] = choices
        for index, obj in enumerate(queryset):
            for entry in obj.entries.all():
                for field_entry in entry.field_entries.all():
                    try:
                        value = field_entry.get_value()
                        if (
                            entry.form.get_root().slug == form_slug
                            and field_entry.form_field
                            and field_entry.form_field.show_chart
                            and field_entry.form_field.field_class
                            in ["ChoiceField", "MultipleChoiceField"]
                            and value in data[field_entry.form_field.label]
                        ):
                            data[field_entry.form_field.label][
                                field_entry.get_value()
                            ] += 1
                        elif (
                            entry.form.get_root().slug == form_slug
                            and field_entry.form_field.field_class
                            == "ChoiceWithOtherField"
                        ):

                            for one in [data[field_entry.form_field.label].keys()][0]:
                                if one in value:
                                    data[field_entry.form_field.label][one] += 1
                    except Exception:
                        pass

        context["form_slug"] = form_slug
        ordered_data_list = []
        root_form = (
            Form.objects.filter(slug=form_slug).first()
            if Form.objects.filter(slug=form_slug).exists()
            else None
        )
        if root_form:
            for index1, a_form in enumerate(Form.get_tree(parent=root_form)):
                for index2, form_field in enumerate(
                    FormField.objects.filter(form=a_form, show_chart=True).order_by(
                        "order"
                    )
                ):
                    if form_field.label in data:
                        try:
                            label = (
                                str(int(len(ordered_data_list) + 1))
                                + ". "
                                + form_field.label
                            )
                            ordered_data_list.append((label, data[form_field.label]))
                        except Exception:
                            pass
        ordered_data = OrderedDict(ordered_data_list)
        # context['data'] = SafeString(json.dumps(ordered_data))
        chart_type = "pie"
        if (
            self.request.GET.get("chart_type", None)
            and self.request.GET.get("chart_type", None) == "bar"
        ):
            chart_type = "bar"
        context["chart_type"] = chart_type

        charts = []
        if chart_type == "pie":
            for a_chart_data in ordered_data.items():
                labels = []
                values = []
                for a_lable, a_value in a_chart_data[1].items():
                    labels.append(a_lable)
                    values.append(a_value)
                pie = plotly.offline.plot(
                    {
                        "data": [
                            go.Pie(
                                labels=labels,
                                values=values,
                                pull=0.1,
                                hole=0.1,
                                textposition="outside",
                                textinfo="value+percent",
                            )
                        ],
                        "layout": go.Layout(title=a_chart_data[0]),
                    },
                    auto_open=False,
                    output_type="div",
                    config={
                        "displaylogo": False,
                        "showLink": False,
                        "modeBarButtonsToRemove": [
                            "sendDataToCloud",
                            "hoverCompareCartesian",
                        ],
                    },
                )
                charts.append(pie)
        if chart_type == "bar":
            for a_chart_data in ordered_data.items():
                labels = []
                values = []
                for a_lable, a_value in a_chart_data[1].items():
                    labels.append(a_lable)
                    values.append(a_value)
                pie = plotly.offline.plot(
                    {
                        "data": [
                            go.Bar(
                                x=labels,
                                y=values,
                                marker_color=plotly.colors.qualitative.Plotly,
                            )
                        ],
                        "layout": go.Layout(title=a_chart_data[0]),
                    },
                    auto_open=False,
                    output_type="div",
                    config={
                        "displaylogo": False,
                        "showLink": False,
                        "modeBarButtonsToRemove": [
                            "sendDataToCloud",
                            "hoverCompareCartesian",
                        ],
                    },
                )
                charts.append(pie)
        context["charts"] = charts
        return context
