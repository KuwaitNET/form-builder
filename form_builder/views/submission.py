# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import re
from collections import OrderedDict
from django.contrib import messages

try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse

from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext as _
from django.views import generic
from django.utils.safestring import SafeString

import xlwt

from form_builder import settings as form_settings
from form_builder.models import (
    FormSubmit,
    FormEntry,
    FieldEntry,
    FormField,
    Workflow,
    ReSubmitFields,
    Field,
    Form,
)
from form_builder.utils import user_is_authenticated, get_submitter
from form_builder.forms.utils import form_factory
from .utils import get_submit_data

from django.contrib.sites.models import Site
from django.conf import settings
from django.template import engines
from .utils import get_entry_data
from django.contrib.auth import get_user_model
from ..utils import send_mail

User = get_user_model()


class SidebarMixin(generic.View):
    def get_context_data(self, **kwargs):
        context = super(SidebarMixin, self).get_context_data(**kwargs)
        user = self.request.user
        if user_is_authenticated(user) and self.request.user.is_superuser:
            context["sidebar_forms"] = Form.objects.filter(depth=1)
        elif user_is_authenticated(user):
            context["sidebar_forms"] = Form.objects.filter(depth=1, permission=user)
        return context

    def dispatch(self, request, *args, **kwargs):
        context = super(SidebarMixin, self).dispatch(request, *args, **kwargs)
        if (
            not user_is_authenticated(self.request.user)
            or not self.request.user.is_staff
        ):
            return HttpResponseRedirect(reverse("admin:login"))
        return context


class SubmissionFormDetailAdmin(SidebarMixin, generic.DetailView):
    model = FormSubmit
    template_name = "admin/form_builder/detail_admin.html"

    def token(self):
        return self.object.entries.all()[0].field_entries.all()[0].token

    def get_context_data(self, **kwargs):
        ctx = super(SubmissionFormDetailAdmin, self).get_context_data(**kwargs)
        ctx["title"] = _("Form details of {}").format(self.object.form)

        (
            all_forms_name,
            all_entry_forms,
            initial_values,
            initial_values_dispaly,
            initial_values_dispaly_by_slug,
            initial_values_dispaly_by_slug_entry_id,
            all_entry_forms_entry_obj,
        ) = get_submit_data(self.object, self.request)

        ctx["initial_values"] = initial_values
        ctx["initial_values_dispaly"] = initial_values_dispaly
        ctx[
            "initial_values_dispaly_by_slug_entry_id"
        ] = initial_values_dispaly_by_slug_entry_id
        ctx["all_entry_forms"] = all_entry_forms
        ctx["all_entry_forms_entry_obj"] = all_entry_forms_entry_obj
        ctx.update(initial_values_dispaly_by_slug)

        ctx["all_forms_name"] = all_forms_name

        if self.object.form.use_verification:
            ctx["verified_company"] = (
                self.object.entries.all()[0]
                .field_entries.filter(field_name="verified_company")
                .first()
                .value
                if self.object.entries.all()[0]
                .field_entries.filter(field_name="verified_company")
                .exists()
                else ""
            )
            ctx["verified_email"] = (
                self.object.entries.all()[0]
                .field_entries.filter(field_name="verified_email")
                .first()
                .value
                if self.object.entries.all()[0]
                .field_entries.filter(field_name="verified_email")
                .exists()
                else ""
            )

        ctx["app_number"] = (
            FieldEntry.objects.filter(
                token=self.object.get_token,
                field_name__in=["temporary_ref", "permanent_ref"],
            )
            .first()
            .get_value()
            if FieldEntry.objects.filter(
                token=self.object.get_token,
                field_name__in=["temporary_ref", "permanent_ref"],
            ).exists()
            else None
        )
        ctx["token"] = self.object.get_token
        workflow = self.object.form.workflow or self.object.form.get_root().workflow
        if workflow:
            if self.object.workflow_state:
                ctx["workflow"] = self.object.workflow_state
            else:
                ctx["workflow"] = (
                    workflow.get_children()[0]
                    if len(workflow.get_children()) > 0
                    else None
                )

        initial_values = {}
        initial_values_dispaly = (
            {}
        )  # TODO should be depreciated for initial_values_dispaly_by_slug
        initial_values_dispaly_by_slug = {}
        initial_values_dispaly_by_slug_entry_id = {}

        all_workflow_forms_name = []
        all_forms = {}
        for a_from in FormEntry.objects.filter(
            token=self.object.get_token, form_submit=None
        ):
            all_workflow_forms_name.append(a_from.form.name)
            initial_values[a_from.form.name] = {}
            initial_values_dispaly[a_from.form.name] = {}
            form_key = a_from.form.slug.replace("-", "_")
            initial_values_dispaly_by_slug[form_key] = {}
            initial_values_dispaly_by_slug_entry_id[form_key] = {}
            for field_entry in a_from.field_entries.all():
                if field_entry.form_field:
                    initial_values_dispaly[a_from.form.name][
                        field_entry.form_field.slug
                    ] = field_entry.get_html_value()
                    initial_values_dispaly_by_slug[form_key][
                        field_entry.form_field.slug
                    ] = field_entry.get_html_value()
                    initial_values_dispaly_by_slug_entry_id[form_key][
                        field_entry.form_field.slug
                    ] = field_entry.id
                if field_entry.form_field:
                    if (
                        field_entry.form_field.field_class
                        in form_settings.ChoiceWithOtherField
                    ):
                        initial_values[a_from.form.name][
                            field_entry.form_field.slug
                        ] = field_entry.value.split(";")

                    elif (
                        field_entry.form_field.field_class
                        in form_settings.MultipleChoiceField
                    ):
                        pattern = re.compile(r"\d+")
                        initial_values[a_from.form.name][
                            field_entry.form_field.slug
                        ] = pattern.findall(field_entry.value)
                    else:
                        initial_values[a_from.form.name][
                            field_entry.form_field.slug
                        ] = field_entry.get_html_value()
            all_forms[a_from.form.name] = []
            all_forms[a_from.form.name].append(
                form_factory(
                    form_model=a_from.form,
                    token=self.object.get_token,
                    request=self.request,
                )(initial=initial_values)
            )

        pk = self.kwargs.get(self.pk_url_kwarg)
        resubmit_link = ""
        if pk:
            resubmit_link = reverse("form_builder:resubmit_view", args=(pk,))

        ctx["initial_values"].update(initial_values)
        ctx["initial_values_dispaly"].update(initial_values_dispaly)
        ctx["initial_values_dispaly_by_slug_entry_id"].update(
            initial_values_dispaly_by_slug_entry_id
        )
        ctx.update(initial_values_dispaly_by_slug)
        ctx.update(initial_values_dispaly)
        ctx["all_workflow_forms_name"] = all_workflow_forms_name
        ctx["all_workflow_forms"] = all_forms
        ctx["resubmit_link"] = self.request.build_absolute_uri(resubmit_link)
        return ctx

    def process_file(self, request, form_model, entry=None):
        formset_id = None
        formset_order = None
        submitter = get_submitter(request)
        field_entries = []
        for i in range(len(request.FILES)):
            if "-" in request.FILES.keys()[i]:
                # formset field posted, find the field on that form
                # formset-[form_id]-[order]-[field_name]
                field_opts = request.FILES.keys()[i].split("-")
                original_file_name = request.FILES.keys()[i]
                formset_order = field_opts[2]
                formset_id = field_opts[1]
                field_name = field_opts[3]
                form_model = Form.objects.get(pk=int(formset_id))
                fields = form_model.get_fields().files().filter(slug=field_name)
                if fields.count() != 1:
                    return -1
                form_field = fields[0]

                extenssion = request.FILES.get(form_field.slug).name.split(".")[-1]
                if (
                    form_field.field
                    and form_field.field.allowed_file_types
                    and extenssion not in form_field.field.allowed_file_types.split(",")
                ):
                    return -1

                FieldEntry.objects.filter(
                    entry=entry,
                    form=self.object,
                    field_name=form_field.slug,
                    formset_order=formset_order,
                    formset_id=formset_id,
                    form_field=form_field,
                    submitter=submitter,
                    token=self.token(),
                    file__isnull=False,
                ).delete()

                field_entry = FieldEntry.objects.create(
                    entry=entry,
                    form=self.object,
                    field_name=form_field.slug,
                    formset_order=formset_order,
                    formset_id=formset_id,
                    form_field=form_field,
                    file=request.FILES.get(original_file_name),
                    submitter=submitter,
                    token=self.token(),
                )

            else:
                # field posted, find in current form's fields
                field_name = request.FILES.keys()[i]
                fields = form_model.get_fields().files().filter(slug=field_name)
                if fields.count() != 1:
                    return -1

                form_field = fields[0]

                extenssion = request.FILES.get(form_field.slug).name.split(".")[-1]
                if (
                    form_field.field
                    and form_field.field.allowed_file_types
                    and extenssion not in form_field.field.allowed_file_types.split(",")
                ):
                    return -1

                # remove the old file if we upload another one over the same formset_id
                FieldEntry.objects.filter(
                    entry=entry,
                    form=self.object.form,
                    field_name=form_field.slug,
                    formset_order=formset_order,
                    formset_id=formset_id,
                    form_field=form_field,
                    submitter=submitter,
                    token=self.token(),
                    file__isnull=False,
                ).delete()

                field_entry = FieldEntry.objects.create(
                    entry=entry,
                    form=self.object.form,
                    field_name=form_field.slug,
                    formset_order=formset_order,
                    formset_id=formset_id,
                    form_field=form_field,
                    file=request.FILES.get(form_field.slug),
                    submitter=submitter,
                    token=self.token(),
                )
            field_entries.append(field_entry.pk)

        return field_entries

    def process_file_workflow(self, request, form_model, entry=None):
        formset_id = None
        formset_order = None
        submitter = get_submitter(request)
        field_entries = []
        for i in range(len(request.FILES)):
            if "-" in request.FILES.keys()[i]:
                # formset field posted, find the field on that form
                # formset-[form_id]-[order]-[field_name]
                field_opts = request.FILES.keys()[i].split("-")
                original_file_name = request.FILES.keys()[i]
                formset_order = field_opts[2]
                formset_id = field_opts[1]
                field_name = field_opts[3]
                fields = (
                    form_model.get_fields().files().filter(form_field__slug=field_name)
                )
                if fields.count() != 1:
                    return -1
                form_field = fields[0].form_field

                FieldEntry.objects.filter(
                    entry=entry,
                    form=form_model,
                    field_name=form_field.slug,
                    formset_order=formset_order,
                    formset_id=formset_id,
                    form_field=form_field,
                    submitter=submitter,
                    token=self.token(),
                    file__isnull=False,
                ).delete()

                field_entry = FieldEntry.objects.create(
                    entry=entry,
                    form=form_model,
                    field_name=form_field.slug,
                    formset_order=formset_order,
                    formset_id=formset_id,
                    form_field=form_field,
                    file=request.FILES.get(original_file_name),
                    submitter=submitter,
                    token=self.token(),
                )

            else:
                # field posted, find in current form's fields
                field_name = request.FILES.keys()[i]
                fields = form_model.get_fields().files().filter(slug=field_name)
                if fields.count() != 1:
                    return -1

                form_field = fields[0]

                # remove the old file if we upload another one over the same formset_id
                FieldEntry.objects.filter(
                    entry=entry,
                    form=form_model,
                    field_name=form_field.slug,
                    formset_order=formset_order,
                    formset_id=formset_id,
                    form_field=form_field,
                    submitter=submitter,
                    token=self.token(),
                    file__isnull=False,
                ).delete()

                field_entry = FieldEntry.objects.create(
                    entry=entry,
                    form=form_model,
                    field_name=form_field.slug,
                    formset_order=formset_order,
                    formset_id=formset_id,
                    form_field=form_field,
                    file=request.FILES.get(field_name),
                    submitter=submitter,
                    token=self.token(),
                )
            field_entries.append(field_entry.pk)

    def process_file_for_edit(self, request):
        field_entries = []
        for i in range(len(request.FILES)):
            file_name_in_request = request.FILES.keys()[i]
            entry_id_in_request = (
                request.FILES.keys()[i].split("_resubmitentryprefix_")[1]
                if len(request.FILES.keys()[i].split("_resubmitentryprefix_")) > 1
                else None
            )

            if entry_id_in_request:
                formset_id = None
                formset_order = None
                form_model = get_object_or_404(FormEntry, id=entry_id_in_request).form
                entry = get_object_or_404(FormEntry, id=entry_id_in_request)
                get_submitter(request)
                if "-" in file_name_in_request:
                    # formset field posted, find the field on that form
                    # formset-[form_id]-[order]-[field_name]
                    original_file_name = file_name_in_request.split(
                        "_resubmitentryprefix_"
                    )[0]
                    field_opts = original_file_name.split("-")
                    formset_order = field_opts[2]
                    formset_id = field_opts[1]
                    field_name = field_opts[3] + "_resubmitentryprefix_" + str(entry.id)
                    fields = form_model.get_fields().files().filter(slug=field_name)
                    if fields.count() != 1:
                        return -1
                    form_field = fields[0]

                    FieldEntry.objects.filter(
                        entry=entry,
                        form=form_model,
                        field_name=form_field.slug,
                        formset_order=formset_order,
                        formset_id=formset_id,
                        form_field=form_field,
                        submitter=get_submitter(request),
                        token=self.token(),
                        file__isnull=False,
                    ).delete()

                    field_entry = FieldEntry.objects.create(
                        entry=entry,
                        form=form_model,
                        field_name=form_field.slug,
                        formset_order=formset_order,
                        formset_id=formset_id,
                        form_field=form_field,
                        file=request.FILES.get(original_file_name),
                        submitter=get_submitter(request),
                        token=self.token(),
                    )

                else:
                    # field posted, find in current form's fields
                    field_name = file_name_in_request
                    original_file_name = file_name_in_request.split(
                        "_resubmitentryprefix_"
                    )[0]
                    fields = (
                        form_model.get_fields().files().filter(slug=original_file_name)
                    )
                    if fields.count() != 1:
                        return -1

                    form_field = fields[0]

                    # remove the old file if we upload another one over the same formset_id  # noqa
                    FieldEntry.objects.filter(
                        entry=entry,
                        form=form_model,
                        field_name=form_field.slug,
                        formset_order=formset_order,
                        formset_id=formset_id,
                        form_field=form_field,
                        submitter=get_submitter(request),
                        token=self.token(),
                        file__isnull=False,
                    ).delete()
                    field_entry = FieldEntry.objects.create(
                        entry=entry,
                        form=form_model,
                        field_name=form_field.slug,
                        formset_order=formset_order,
                        formset_id=formset_id,
                        form_field=form_field,
                        file=request.FILES.get(field_name),
                        submitter=get_submitter(request),
                        token=self.token(),
                    )
                field_entries.append(field_entry.pk)

    def post(self, request, *args, **kwargs):
        action_id = request.POST.get("action_id")
        workflow_form_id = request.POST.get("workflow_form_id")
        re_submit_fields = request.POST.get("re_submit_fields", None)
        accept_resubmission = request.POST.get("accept_resubmission", None)

        self.object = self.get_object()
        if accept_resubmission:
            for resubmission_field in ReSubmitFields.objects.filter(
                submission=self.object
            ):
                resubmission_field.field_entry.value = resubmission_field.value
                resubmission_field.field_entry.save()

        if re_submit_fields and re_submit_fields == "True":
            ReSubmitFields.objects.filter(submission=self.object).delete()

            for key, value in request.POST.items():
                form_id = (
                    key.split("_resubmitprefix_")[0]
                    if "_resubmitprefix_" in key
                    else None
                )
                field_name = (
                    key.split("_resubmitprefix_")[1]
                    if "_resubmitprefix_" in key
                    else None
                )
                field_entry_id = (
                    key.split("_resubmitprefix_")[2]
                    if "_resubmitprefix_" in key
                    and key.split("_resubmitprefix_")[2] != "None"
                    else None
                )
                resubmit_form = (
                    get_object_or_404(Form, id=int(form_id)) if form_id else None
                )
                resubmit_field = (
                    resubmit_form.fields.filter(form_field__slug=field_name)
                    .first()
                    .field
                    if resubmit_form
                    and resubmit_form.fields.filter(
                        form_field__slug=field_name
                    ).exists()
                    else None
                )
                field_entry = (
                    get_object_or_404(FieldEntry, id=field_entry_id)
                    if field_entry_id
                    else None
                )
                if resubmit_form and resubmit_field and self.object and field_entry:
                    ReSubmitFields(
                        form=resubmit_form,
                        field=resubmit_field,
                        submission=self.object,
                        field_entry=field_entry,
                    ).save()

        if action_id and not workflow_form_id:
            try:
                self.object = self.get_object()
                self.object.workflow_state = Workflow.objects.get(id=int(action_id))
                self.object.save()
                self.object.workflow_state.exit_action(
                    ctx=self.get_context_data(),
                    send_to=[self.object.submitter_user.email],
                    submission=self.object,
                    request=request,
                )
                return HttpResponse("Done")
            except Exception:
                return HttpResponse("Error")
        if workflow_form_id:
            send_email = True
            # try:
            self.object = self.get_object()
            form_model = Form.objects.get(id=int(workflow_form_id))
            form = form_factory(
                form_model=form_model,
                token=self.object.get_token,
                request=request,
                is_formset=False,
            )
            form = form(request.POST)
            if form.is_valid():
                entry = form.save()
                if request.FILES:
                    self.process_file_workflow(
                        request, form_model=form_model, entry=entry
                    )
                    form.save_file_entries(
                        entry, files=self.request.POST.get("_files", None)
                    )

                if (
                    entry.form
                    and entry.form.send_email
                    and entry.form.form_builder_email_template
                ):
                    domain = Site.objects.get_current().domain
                    if hasattr(settings, "LANGUAGE_CODE"):
                        submit_link = "".join(
                            [
                                "http://",
                                domain,
                                "/{}".format(settings.LANGUAGE_CODE),
                                entry.form.get_absolute_submission_url_no_i18n(),
                            ]
                        )
                    else:
                        submit_link = "".join(
                            [
                                "http://",
                                domain,
                                entry.form.get_absolute_submission_url_no_i18n(),
                            ]
                        )
                    context = {
                        "form": entry.form,
                        "create_date": entry.created,
                        "file_number": entry.field_entries.filter(
                            field_name="file_number"
                        )
                        .first()
                        .value
                        if entry.field_entries.filter(field_name="file_number").exists()
                        else None,
                        "verified_company": entry.field_entries.filter(
                            field_name="verified_company"
                        )
                        .first()
                        .value
                        if entry.field_entries.filter(
                            field_name="verified_company"
                        ).exists()
                        else None,
                        "verified_email": entry.field_entries.filter(
                            field_name="verified_email"
                        )
                        .first()
                        .value
                        if entry.field_entries.filter(
                            field_name="verified_email"
                        ).exists()
                        else None,
                        "verified_code": entry.field_entries.filter(
                            field_name="verified_code"
                        )
                        .first()
                        .value
                        if entry.field_entries.filter(
                            field_name="verified_code"
                        ).exists()
                        else None,
                        "submit_link": submit_link,
                        "submitter": self.request.user,
                    }
                    (
                        all_forms_name,
                        all_entry_forms,
                        initial_values,
                        initial_values_dispaly,
                        initial_values_dispaly_by_slug,
                        initial_values_dispaly_by_slug_entry_id,
                        all_entry_forms_entry_obj,
                    ) = get_entry_data(entry)
                    email_template = entry.form.form_builder_email_template
                    django_engine = engines["django"]
                    body_html_template = django_engine.from_string(
                        email_template.html_content
                    )
                    body_html_rendered = body_html_template.render(
                        {**context, **initial_values_dispaly_by_slug}
                    )
                    body_plain_template = django_engine.from_string(
                        email_template.content
                    )
                    body_plain_rendered = body_plain_template.render(
                        {**context, **initial_values_dispaly_by_slug}
                    )
                    subject_template = django_engine.from_string(email_template.subject)
                    subject_rendered = subject_template.render(
                        {**context, **initial_values_dispaly_by_slug}
                    )
                    if (
                        hasattr(settings, "FORM_BUILDER_EMAIL_FROM")
                        and settings.FORM_BUILDER_EMAIL_FROM
                    ):
                        email_from = (
                            email_template.email_from
                            if email_template.email_from
                            else settings.FORM_BUILDER_EMAIL_FROM
                        )
                        if entry.form.email_from_is_email_field:
                            email_field_qst = entry.field_entries.filter(
                                form_field=entry.form.email_field
                            )
                            if email_field_qst.exists():
                                email_from = email_field_qst.first().value

                        attachments = None

                        if entry.form.send_files_email:
                            attachments = (
                                []
                            )  # should be a list of a tuple (<file_name>, <content>)
                            for a_field in entry.field_entries.exclude(
                                file=None
                            ).exclude(file=""):
                                attachment = a_field.file.file.read()
                                # attachment = requests.get(a_field.file.url).content
                                attachment_name = a_field.form_field.label
                                file_extension = a_field.file.url.split(".")[-1]
                                attachments.append(
                                    (
                                        "{}.{}".format(attachment_name, file_extension),
                                        attachment,
                                    )
                                )

                        if entry.form.send_email_to_submitter:
                            form_submit = FormEntry.objects.filter(
                                token=self.object.get_token
                            ).exclude(form_submit=None)
                            form_submit = (
                                form_submit.first() if form_submit.exists() else None
                            )

                            submitter_email = (
                                form_submit.form_submit.submitter_user.email
                                if form_submit
                                and form_submit.form_submit.submitter_user
                                and form_submit.form_submit.submitter_user.email
                                else None
                            )
                            submitter_email = (
                                entry.submitter.email
                                if not submitter_email
                                and entry.submitter
                                and entry.submitter.email
                                else submitter_email
                            )

                            if submitter_email:
                                send_mail(
                                    str(subject_rendered),
                                    str(body_plain_rendered),
                                    str(body_html_rendered),
                                    email_from,
                                    [submitter_email],
                                    fail_silently=False,
                                    attachments=attachments,
                                )

                        if entry.form.notify_emails:
                            for send_to in entry.form.notify_emails.split(";"):
                                send_mail(
                                    str(subject_rendered),
                                    str(body_plain_rendered),
                                    str(body_html_rendered),
                                    email_from,
                                    [send_to.strip()],
                                    fail_silently=False,
                                    attachments=attachments,
                                )
                        for user in entry.form.notify_users.all():
                            send_mail(
                                subject_rendered,
                                body_plain_rendered,
                                body_html_rendered,
                                email_from,
                                [user.email],
                                fail_silently=False,
                                attachments=attachments,
                            )

                        for group in entry.form.notify_groups.all():
                            for user in User.objects.filter(groups__id=group.id):
                                send_mail(
                                    subject_rendered,
                                    body_plain_rendered,
                                    body_html_rendered,
                                    email_from,
                                    [user.email],
                                    fail_silently=False,
                                    attachments=attachments,
                                )

                if action_id:
                    try:
                        self.object = self.get_object()
                        self.object.workflow_state = Workflow.objects.get(
                            id=int(action_id)
                        )
                        self.object.save()
                        if send_email:
                            self.object.workflow_state.exit_action(
                                ctx=self.get_context_data(),
                                send_to=[self.object.submitter_user.email],
                                submission=self.object,
                                request=request,
                            )
                        return HttpResponse("Done")
                    except Exception:
                        return HttpResponse("Error")
            else:
                for key, value in form.errors.iteritems():
                    messages.add_message(
                        request,
                        messages.ERROR,
                        "{} : {}".format(
                            Field.objects.get(name=key).label,
                            _("This field is required or entered wrongly"),
                        ),
                    )

            self.object.workflow_state = Workflow.objects.get(
                id=int(request.POST.get("action_id_inform"))
            )
            self.object.save()
            if send_email:
                self.object.workflow_state.exit_action(
                    ctx=self.get_context_data(),
                    send_to=[self.object.submitter_user.email],
                    submission=self.object,
                    request=request,
                )
            return HttpResponseRedirect(
                reverse(
                    "form_builder:submission_form_detail_admin",
                    args=(form_model.slug, self.object.id),
                )
            )
        for key, value in request.POST.items():
            field_name = (
                key.split("_resubmitentryprefix_")[0]
                if "_resubmitentryprefix_" in key
                else None
            )
            form_entry = (
                key.split("_resubmitentryprefix_")[1]
                if "_resubmitentryprefix_" in key
                else None
            )
            if form_entry:
                form_entry = form_entry.split("_")[0]

            if field_name and form_entry:
                FieldEntry.objects.filter(
                    entry__id=form_entry, form_field__slug=field_name
                ).update(value=value)

        if request.FILES:
            self.process_file_for_edit(request)

        return HttpResponseRedirect(request.path_info)


class ChartsFormView(generic.TemplateView):
    template_name = "dashboard/charts.html"

    def get_context_data(self, **kwargs):
        # filter = FormSubmitFilter(self.request.GET,
        #                        queryset=FormSubmit.objects.filter(entries__form__slug=self.kwargs.get('path')))
        context = super(ChartsFormView, self).get_context_data(**kwargs)
        form_slug = self.kwargs.get("path", None)
        context["title"] = _("Submission of {}".format(form_slug))
        # context['filter'] = filter
        queryset = FormSubmit.objects.filter(entries__form__slug=form_slug)
        data = {}
        for index, obj in enumerate(queryset):
            for entry in obj.entries.all():
                for field_entry in entry.field_entries.all():
                    if (
                        entry.form.slug == form_slug
                        and field_entry.field
                        and field_entry.field.field_class
                        in [
                            "ChoiceField",
                            "ChoiceWithOtherField",
                            "MultipleChoiceField",
                        ]
                        and field_entry.field.label not in data
                    ):
                        data.update({field_entry.field.label: None})
                        choices = {}
                        for choice in field_entry.field.choices.all():
                            choices.update({choice.value: 0})
                        data[field_entry.field.label] = choices

        for index, obj in enumerate(queryset):
            for entry in obj.entries.all():
                for field_entry in entry.field_entries.all():
                    try:
                        value = field_entry.get_value()
                        if (
                            entry.form.slug == form_slug
                            and field_entry.field
                            and field_entry.field.field_class
                            in ["ChoiceField", "MultipleChoiceField"]
                            and value in data[field_entry.field.label]
                        ):
                            data[field_entry.field.label][field_entry.get_value()] += 1
                        elif (
                            entry.form.slug == form_slug
                            and field_entry.field.field_class == "ChoiceWithOtherField"
                        ):

                            for one in [data[field_entry.field.label].keys()][0]:
                                if one in value:
                                    data[field_entry.field.label][one] += 1
                    except Exception:
                        pass

        context["form_slug"] = form_slug
        ordered_data_list = []
        root_form = (
            Form.objects.filter(slug=form_slug).first()
            if Form.objects.filter(slug=form_slug).exists()
            else None
        )
        if root_form:
            for index1, a_form in enumerate(Form.get_tree(parent=root_form)):
                for index2, form_field in enumerate(
                    FormField.objects.filter(form=a_form, show_chart=True).order_by(
                        "order"
                    )
                ):
                    if form_field.field.label in data:
                        try:
                            label = (
                                str(int(len(ordered_data_list) + 1))
                                + ". "
                                + form_field.field.label
                            )
                            ordered_data_list.append(
                                (label, data[form_field.field.label])
                            )
                        except Exception:
                            pass
        ordered_data = OrderedDict(ordered_data_list)
        context["data"] = SafeString(json.dumps(ordered_data))
        chart_type = "pie"
        if (
            self.request.GET.get("chart_type", None)
            and self.request.GET.get("chart_type", None) == "bar"
        ):
            chart_type = "bar"
        context["chart_type"] = chart_type
        return context


class ExportAllView(generic.View):
    def get(self, request, *args, **kwargs):
        ids = request.GET.get("ids")
        modified__year = request.GET.get("modified__year")
        if ids:
            try:
                id = [int(x) for x in ids.split(",")]
                queryset = FormSubmit.objects.filter(
                    entries__form__slug=kwargs.get("path"), id__in=id
                )
            except ValueError:
                queryset = FormSubmit.objects.filter(
                    entries__form__slug=kwargs.get("path")
                )
        else:
            queryset = FormSubmit.objects.filter(entries__form__slug=kwargs.get("path"))

        if modified__year:
            queryset = queryset.filter(modified__year=int(modified__year))

        # field = FormField.objects.filter(form__slug=kwargs.get('path'))

        response = HttpResponse(content_type="application/ms-excel")
        response["Content-Disposition"] = 'attachment; filename="{0}.xls"'.format(
            kwargs.get("path")
        )

        wb = xlwt.Workbook(encoding="utf-8")
        # ws = wb.add_sheet('Parent Form')

        # row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        # columns = []
        #########################################################
        sheet_per_form = False
        # wb = xlwt.Workbook(encoding='utf-8')
        all_forms_slug = []
        global start_col_index
        start_col_index = 0

        def update_form(form, ws, sheet_per_form, start_col_index):
            all_forms_slug.append(form.slug)
            row_num = 0
            global columns
            columns = []
            font_style = xlwt.XFStyle()
            font_style.font.bold = True
            if sheet_per_form:
                ws = wb.add_sheet(form.name[:29] if len(form.name) > 29 else form.name)
                start_col_index = 0

            form_field = FormField.objects.filter(form__slug=form.slug, is_active=True)
            if form.use_verification:
                columns.append(_("File Number"))
                columns.append(_("Company Name"))
                columns.append(_("Email"))

            for obj in form_field:
                try:
                    columns.append(obj.label_ar)
                except Exception:
                    columns.append(obj.name)
            for col_num in range(len(columns)):
                ws.write(
                    row_num, col_num + start_col_index, columns[col_num], font_style
                )
            for row in queryset:
                row_num += 1
                for entry in row.entries.filter(form=form):
                    for col_num, field_entry in enumerate(entry.field_entries.all()):
                        index = -1
                        value = field_entry.get_excel_value()
                        if "<br>" in value:
                            value_list = value.split("<br>")
                            value = "Choice: {}, Other: {}".format(
                                value_list[0], value_list[1]
                            )
                        # try:
                        if field_entry.form_field:
                            try:
                                index_field = columns.index(
                                    field_entry.form_field.label_ar
                                )
                            except Exception:
                                try:
                                    index_field = columns.index(
                                        field_entry.form_field.name
                                    )
                                except Exception:
                                    continue
                            try:
                                ws.write(
                                    row_num,
                                    index_field + start_col_index,
                                    value,
                                    font_style,
                                )
                            except Exception:
                                continue
                            # We comments these to prevent old submissions #
                            # else:
                            #     index = columns.index(field_entry.field.label_ar)
                            #     if index>=0:
                            #         ws.write(row_num, index, value, font_style)
                        else:
                            if field_entry.field_name == "file_number":
                                index = columns.index(_("File Number"))
                            if field_entry.field_name == "verified_code":
                                # index = columns.index(_('Verification Code'))
                                pass
                            if field_entry.field_name == "verified_company":
                                index = columns.index(_("Company Name"))
                            if field_entry.field_name == "verified_email":
                                index = columns.index(_("Email"))
                            if index >= 0:
                                ws.write(
                                    row_num, index + start_col_index, value, font_style
                                )

                # for col_num, field_entry in enumerate(columns):

            path = form.path
            depth = form.depth
            # global start_col_index
            start_col_index += len(columns)
            for a_from in Form.objects.filter(path__startswith=path, depth=depth + 1):
                try:
                    update_form(
                        a_from,
                        ws,
                        sheet_per_form=sheet_per_form,
                        start_col_index=start_col_index,
                    )
                except Exception:
                    break
                has_more = a_from.numchild > 0
                if has_more:
                    try:
                        update_form(
                            a_from,
                            ws,
                            sheet_per_form=sheet_per_form,
                            start_col_index=start_col_index,
                        )
                    except Exception:
                        break
                else:
                    all_forms_slug.append(a_from.slug)

        form = get_object_or_404(Form, slug=kwargs.get("path"))
        if not sheet_per_form:
            ws = wb.add_sheet(form.name[:29] if len(form.name) > 29 else form.name)
        update_form(
            form, ws, sheet_per_form=sheet_per_form, start_col_index=start_col_index
        )
        ################################################################
        # for obj in field:
        #     columns.append(obj.field.label)
        #
        # for col_num in range(len(columns)):
        #     ws.write(row_num, col_num, columns[col_num], font_style)
        #
        # font_style = xlwt.XFStyle()
        #
        # for row in queryset:
        #     row_num += 1
        #     for entry in row.entries.all():
        #         for col_num, field_entry in enumerate(entry.field_entries.all()):
        #             value = field_entry.get_value()
        #             if columns[col_num] == field_entry.field.label:
        #                 ws.write(row_num, col_num, value, font_style)
        #             else:
        #                 index = columns.index(field_entry.field.label)
        #                 if index>=0:
        #                     ws.write(row_num, index, value, font_style)

        wb.save(response)
        return response
