# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid
import requests
import datetime

from django.contrib.auth import get_user_model
from django.forms.formsets import formset_factory, BaseFormSet
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.utils.functional import cached_property
from django.utils.translation import ugettext as _
from django.views import generic
from django.db.models import F
from django.contrib.sites.models import Site
from django.conf import settings
from django.template import engines

from ..utils import get_submitter, get_class
from .utils import get_entry_data

# StreamingHttpResponse has been added in 1.5, and gets used for verification
# only.
User = get_user_model()

try:
    from django.http import StreamingHttpResponse
except ImportError:

    class StreamingHttpResponse(object):
        pass


from form_builder.models import (
    FormEntry,
    FieldEntry,
    FormSubmit,
    FieldEntryExtras,
    Form,
    FormVerificationCode,
    Choice,
)
from form_builder.forms import form_factory
from form_builder.models import FormVerificationCode, Choice

from ..utils import send_mail

# if "mailer" in settings.INSTALLED_APPS:
#     from mailer import send_mail, send_html_mail
# else:
#     from django.core.mail import send_mail


class FormURLMixin(object):
    slug_field = "url_path"
    slug_url_kwarg = "path"


class AjaxResponseMixin(object):
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """

    NOTIFY_INFO, NOTIFY_ERROR, NOTIFY_WARNING, NOTIFY_SUCCESS = (
        "info",
        "error",
        "warn",
        "success",
    )

    def get_json_payload(self, redirect=False, notify=None, errors=None, data=None):
        redirect_url = ""
        if redirect:
            redirect_url = self.success_url

        return {
            "notify": notify,
            "action": {"redirect": redirect, "redirect_url": redirect_url},
            "errors": errors,
            "data": data,
        }

    def form_invalid(self, form):
        response = super(AjaxResponseMixin, self).form_invalid(form)
        notify = []
        payload = {"errors": []}
        if any(form.errors):
            payload.update(errors=form.errors)
            notify.append(
                {
                    "message": _("Please correct form errors marked in red"),
                    "type": self.NOTIFY_ERROR,
                }
            )

        for error in form.non_field_errors():
            notify.append({"message": error, "type": self.NOTIFY_ERROR})
        payload.update(notify=notify)

        if self.request.is_ajax():
            return JsonResponse(self.get_json_payload(**payload), status=400)
        else:
            return response

    def form_extra_invalid(self, form, message, payload=None):
        """Handle the return in case the extra validation failed, it return the payload comes from the validator
        function"""
        response = super(AjaxResponseMixin, self).form_invalid(form)
        if not payload:
            notify = []
            payload = {"errors": [], "notify": notify}
            notify.append({"message": str(message), "type": self.NOTIFY_ERROR})

        if self.request.is_ajax():
            return JsonResponse(self.get_json_payload(**payload), status=400)
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        response = super(AjaxResponseMixin, self).form_valid(form)
        if self.request.is_ajax():
            return JsonResponse(self.get_json_payload(redirect=True))
        else:
            return response


class FormModelLoginRequiredMixin(object):
    """
    This view must check if Form object is required or not.
    """

    pass


class BaseFormMixin(
    AjaxResponseMixin,
    generic.edit.FormMixin,
    generic.detail.SingleObjectMixin,
    generic.detail.SingleObjectTemplateResponseMixin,
):
    """
    Mixin to render Form in template and validate.
    """

    def dispatch(self, request, *args, **kwargs):

        self.object = self.get_object()
        self.form_entry = FormEntry.objects.filter(
            submitter=get_submitter(request),
            token=self.token,
            form=self.object,
            form_submit__isnull=True,
        )
        self.update = self.form_entry.exists()
        self.custom_next_url = self.request.GET.get("next-url", None)
        return super(BaseFormMixin, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        # if self.update:
        # self.form_entry.first().field_entries.files().delete()
        return super(BaseFormMixin, self).get(request, *args, **kwargs)

    def process_file(self, request, form_model):
        formset_id = None
        formset_order = None
        submitter = get_submitter(request)
        if "-" in list(request.FILES.keys())[0]:
            # formset field posted, find the field on that form
            # formset-[form_id]-[order]-[field_name]
            field_opts = list(request.FILES.keys())[0].split("-")
            original_file_name = list(request.FILES.keys())[0]
            formset_order = field_opts[2]
            formset_id = field_opts[1]
            field_name = field_opts[3]
            form_model = Form.objects.get(pk=int(formset_id))
            fields = form_model.get_fields().files().filter(slug=field_name)
            if fields.count() != 1:
                return -1
            form_field = fields[0]
            file_list = filter(
                lambda name: form_field.slug in name, request.FILES.keys()
            )
            file_slug = list(file_list)[0] if file_list else None
            if file_slug:
                extenssion = request.FILES.get(file_slug).name.split(".")[-1]
                if (
                    form_field.field
                    and form_field.field.allowed_file_types
                    and extenssion not in form_field.field.allowed_file_types.split(",")
                ):
                    return -1
            FieldEntry.objects.filter(
                form=self.object,
                field_name=form_field.slug,
                formset_order=formset_order,
                formset_id=formset_id,
                form_field=form_field,
                submitter=submitter,
                token=self.token,
                file__isnull=False,
            ).delete()

            field_entry = FieldEntry.objects.create(
                form=self.object,
                field_name=form_field.slug,
                formset_order=formset_order,
                formset_id=formset_id,
                form_field=form_field,
                file=request.FILES.get(original_file_name),
                submitter=submitter,
                token=self.token,
            )

        else:
            # field posted, find in current form's fields
            field_name = list(request.FILES.keys())[0]
            fields = form_model.get_fields().files().filter(slug=field_name)
            if fields.count() != 1:
                return -1

            form_field = fields[0]

            extenssion = request.FILES.get(form_field.slug).name.split(".")[-1]
            if (
                form_field.field
                and form_field.field.allowed_file_types
                and extenssion not in form_field.field.allowed_file_types.split(",")
            ):
                return -1

            # remove the old file if we upload another one over the same formset_id
            FieldEntry.objects.filter(
                form=self.object,
                field_name=form_field.slug,
                formset_order=formset_order,
                formset_id=formset_id,
                form_field=form_field,
                submitter=submitter,
                token=self.token,
                file__isnull=False,
            ).delete()

            field_entry = FieldEntry.objects.create(
                form=self.object,
                field_name=form_field.slug,
                formset_order=formset_order,
                formset_id=formset_id,
                form_field=form_field,
                file=request.FILES.get(form_field.slug),
                submitter=submitter,
                token=self.token,
            )

        return field_entry.pk

    def post(self, request, *args, **kwargs):
        """
        We need the object attribute here for form_factory to create the form.
        """
        verification_code = request.POST.get("verification_code", None)
        if self.object.use_verification and not verification_code:
            response = dict()
            response["notify"] = [
                {"message": _("Invalid verification code"), "type": "error"}
            ]
            response["action"] = {"redirect": False, "redirect_url": ""}
            return JsonResponse(response, status=400)
        if verification_code:
            codes = FormVerificationCode.objects.filter(
                form=self.object, is_active=True, actual_usage__lt=F("max_usage")
            )
            if verification_code not in codes.values_list(
                "verification_code", flat=True
            ):
                response = dict()
                response["notify"] = [
                    {"message": _("Invalid verification code"), "type": "error"}
                ]
                response["action"] = {"redirect": False, "redirect_url": ""}
                return JsonResponse(response, status=400)
        submitter = get_submitter(request)
        if request.FILES:
            pk = self.process_file(request, self.object)
            return JsonResponse({"pk": pk})
        elif request.POST.get("file_delete"):
            response = dict()
            pk = request.POST.get("file_delete")
            try:
                FieldEntry.objects.get(pk=pk).delete()
            except FieldEntry.DoesNotExist:
                # Field doesn't exist in database while file uploading
                # so pk assign as -1
                # while deleting it post pk  wtih -1 & no record found for -1 pk
                response["errors"] = "File not exist"
            except:
                response["errors"] = "Unknown error"
            return JsonResponse(response)
        else:
            if self.object.is_formset:
                formset = self.get_formset_class(
                    form=self.get_form_class(),
                    form_model=self.object,
                    token=self.token,
                    submitter=submitter,
                )(request.POST, request.FILES, prefix=self.object.formset_prefix)
                if formset.is_valid():
                    for index, form in enumerate(formset):
                        if form.is_valid():
                            files = request.POST.get(
                                "formset-%s-%s-_files" % (self.object.pk, index)
                            )
                            if self.object.extra_validator:
                                is_extra_valid, extr_message, payload = get_class(
                                    self.object.extra_validator
                                )(request, form=self.object, token=self.token)
                                if not is_extra_valid:
                                    return self.form_extra_invalid(
                                        form, extr_message, payload
                                    )
                            entry = form.save(files=files)
                            # Run Form Extra Action
                            if entry.form.custom_action:
                                get_class(self.object.custom_action)(entry)
                            if self.request.session.get("form_pks"):
                                request.session["form_pks"].append(entry.id)
                            else:
                                request.session["form_pks"] = [entry.id]
                        else:
                            return self.form_invalid(form)
                    return self.formset_valid(formset)
                else:
                    return self.formset_invalid(formset)
            elif self.object.has_formset_childrens():
                form = self.get_form()
                if form.is_valid():
                    valid, response = self.validate_files(form)
                    if not valid:
                        return response
                    if self.object.extra_validator:
                        is_extra_valid, extr_message, payload = get_class(
                            self.object.extra_validator
                        )(request, form=self.object, token=self.token)
                        if not is_extra_valid:
                            return self.form_extra_invalid(form, extr_message, payload)
                    entry = form.save(
                        files=self.request.POST.get("_files", None), update=self.update
                    )
                    # Run Form Extra Action
                    if entry.form.custom_action:
                        get_class(self.object.custom_action)(entry)
                    if self.request.session.get("form_pks"):
                        self.request.session["form_pks"].append(entry.id)
                    else:
                        self.request.session["form_pks"] = [entry.id]

                    # Delete all previous formset since we are going to add them again
                    for i, form_model in enumerate(self.object.get_children()):
                        if form_model.is_formset:
                            FormEntry.objects.filter(
                                form=form_model, submitter=submitter, token=self.token,
                            ).delete()

                    for i, form_model in enumerate(self.object.get_children()):
                        if form_model.is_formset:
                            form = form_factory(
                                form_model,
                                token=self.token,
                                request=self.request,
                                is_formset=True,
                            )
                            formset = self.get_formset_class(
                                form=form,
                                form_model=form_model,
                                token=self.token,
                                submitter=submitter,
                            )
                            formset = formset(
                                request.POST, prefix="formset-%s" % form_model.pk
                            )
                            if formset.is_valid():
                                for index, form in enumerate(formset):
                                    if form.is_valid():
                                        if (
                                            "DELETE" in form.cleaned_data
                                            and form.cleaned_data["DELETE"]
                                        ):
                                            continue
                                        else:
                                            valid, response = self.validate_files(
                                                form, formset
                                            )
                                            if not valid:
                                                return response
                                            files = request.POST.get(
                                                "formset-%s-%s-_files"
                                                % (form_model.pk, index)
                                            )
                                            entry = form.save(
                                                parent=entry,
                                                files=files,
                                                update=self.update,
                                            )
                                            # Run Form Extra Action
                                            if entry.form.custom_action:
                                                get_class(self.object.custom_action)(
                                                    entry
                                                )
                                            if self.request.session.get("form_pks"):
                                                request.session["form_pks"].append(
                                                    entry.id
                                                )
                                            else:
                                                request.session["form_pks"] = [entry.id]
                                    else:
                                        return self.form_invalid(form)
                            else:
                                return self.formset_invalid(formset)
                    self.run_custom_action_send_email(entry)
                    return super(BaseFormMixin, self).form_valid(entry)
                else:
                    return self.form_invalid(form)
                # return super(BaseFormMixin, self).post(request, *args, **kwargs)
            else:
                # form = self.get_form()
                # if form.is_valid():
                #     if self.object.extra_validator:
                #         is_extra_valid, extr_message, payload = get_class(self.object.extra_validator)(request,
                #                                                                                        form=self.object,
                #                                                                                        token=self.token)
                #         if not is_extra_valid:
                #             return self.form_extra_invalid(form, extr_message, payload)
                return super(BaseFormMixin, self).post(request, *args, **kwargs)

    @staticmethod
    def get_formset_class(
        form, form_model, token, submitter, formset_class=BaseFormSet, extra=1
    ):
        """
        Create a formset from current form, using attributes from Form
        obj.
        """
        if form_model.formset_min_num > 0:
            extra = 0
        return formset_factory(
            form,
            formset_class,
            extra=extra,
            can_order=form_model.formset_can_order,
            can_delete=form_model.formset_can_delete,
            max_num=form_model.formset_max_num,
            min_num=form_model.formset_min_num,
            validate_max=True,
            validate_min=True,
        )

    def get_form_class(self):
        """
        Factory the form on the fly from the current object. If form is defined
        as formset return None.
        """
        return form_factory(
            self.object,
            token=self.token,
            request=self.request,
            update=self.update,
            is_formset=self.object.is_formset,
        )

    def get_form(self, form_class=None):
        if self.object.is_formset:
            form = self.get_formset_class(
                form=self.get_form_class(),
                form_model=self.object,
                token=self.token,
                submitter=get_submitter(self.request),
            )
            kwargs = self.get_form_kwargs()
            kwargs.update(prefix="formset-%s" % (self.object.pk))
            return form(**kwargs)
        else:
            form = self.get_form_class()
            return form(**self.get_form_kwargs())

    def get_success_url(self):
        submitter = get_submitter(self.request)
        # TODO: Make this a customizable success_url on Form.
        if self.request.session.get("form_pks"):
            self.request.session["entry_pks"] = self.request.session.get("form_pks")
        entry_pks = self.request.session.get("entry_pks")
        next_form = self.object.get_next(submitter=submitter, token=self.token)
        if not bool(next_form):
            try:
                self.request.session.pop("form_pks")
            except Exception as e:
                pass
        self.success_url = self.object.get_redirect_url(
            self.request,
            submitter,
            self.token,
            next_form,
            entry_pks,
            self.custom_next_url,
        )
        return self.success_url

    def form_valid(self, form):
        valid, response = self.validate_files(form)
        if not valid:
            return response

        if self.object.extra_validator:
            is_extra_valid, extr_message, payload = get_class(
                self.object.extra_validator
            )(self.request, form=self.object, token=self.token)
            if not is_extra_valid:
                return self.form_extra_invalid(form, extr_message, payload)
        entry = form.save(
            files=self.request.POST.get("_files", None), update=self.update
        )
        verification_code = self.request.POST.get("verification_code", None)
        if verification_code:
            verification = FormVerificationCode.objects.filter(
                form=self.object, is_active=True, verification_code=verification_code
            ).first()
            submitter = get_submitter(self.request)
            next_form = self.object.get_next(submitter=submitter, token=self.token)
            if not next_form:
                verification.actual_usage = 1 + verification.actual_usage
                verification.save()
            verified_company = verification.company_name
            verified_email = verification.email
            entry.field_entries.create(
                field_name="verified_company",
                value=verified_company,
                form=entry.form,
                submitter=entry.submitter,
            )
            entry.field_entries.create(
                field_name="verified_email",
                value=verified_email,
                form=entry.form,
                submitter=entry.submitter,
            )
            entry.field_entries.create(
                field_name="verified_code",
                value=verification.verification_code or "",
                form=entry.form,
                submitter=entry.submitter,
            )
            entry.field_entries.create(
                field_name="file_number",
                value=verification.file_number or "",
                form=entry.form,
                submitter=entry.submitter,
            )
        if self.request.session.get("form_pks"):
            self.request.session["form_pks"].append(entry.id)
        else:
            self.request.session["form_pks"] = [entry.id]

        # Save Extra input of choice fields
        for key, value in self.request.POST.items():
            key_list = key.split("_extraEntry_")
            if len(key_list) > 1:
                field_name = key_list[0]
                choice_id = key_list[1]

                choice_qst = Choice.objects.filter(id=choice_id)
                choice = choice_qst.first() if choice_qst.exists() else None
                field_entries = entry.field_entries.filter(form_field__slug=field_name)
                field_entry = field_entries.first() if field_entries.exists() else None
                if field_entry:
                    FieldEntryExtras(
                        field_entry=field_entry, choice=choice, value=value
                    ).save()

        # Submitter is saved in form.get_redirect_url()
        # form_submit = FormSubmit.objects.create(submitted=True)
        # entry.form_submit = form_submit
        # entry.save()
        # form_submit.after_submit(self, self.request)

        self.run_custom_action_send_email(entry)

        return super(BaseFormMixin, self).form_valid(entry)

    def run_custom_action_send_email(self, entry):
        # Run Form Extra Action
        if entry.form.custom_action:
            get_class(self.object.custom_action)(entry)

        if (
            entry.form
            and entry.form.send_email
            and entry.form.form_builder_email_template
        ):
            domain = Site.objects.get_current().domain
            if hasattr(settings, "LANGUAGE_CODE"):
                submit_link = "".join(
                    [
                        "http://",
                        domain,
                        "/{}".format(settings.LANGUAGE_CODE),
                        entry.form.get_absolute_submission_url_no_i18n(),
                    ]
                )
                # this_submit_link = ''.join(
                #     ['http://', domain, '/{}'.format(settings.LANGUAGE_CODE), entry.get_absolute_submission_url_no_i18n()])
            else:
                submit_link = "".join(
                    [
                        "http://",
                        domain,
                        entry.form.get_absolute_submission_url_no_i18n(),
                    ]
                )
                # this_submit_link = ''.join(
                #     ['http://', domain, entry.form.get_absolute_submission_url_no_i18n()])

            context = {
                "form": entry.form,
                "create_date": entry.created,
                "file_number": entry.field_entries.filter(field_name="file_number")
                .first()
                .value
                if entry.field_entries.filter(field_name="file_number").exists()
                else None,
                "verified_company": entry.field_entries.filter(
                    field_name="verified_company"
                )
                .first()
                .value
                if entry.field_entries.filter(field_name="verified_company").exists()
                else None,
                "verified_email": entry.field_entries.filter(
                    field_name="verified_email"
                )
                .first()
                .value
                if entry.field_entries.filter(field_name="verified_email").exists()
                else None,
                "verified_code": entry.field_entries.filter(field_name="verified_code")
                .first()
                .value
                if entry.field_entries.filter(field_name="verified_code").exists()
                else None,
                "submit_link": submit_link,
                "submitter": self.request.user,
            }

            (
                all_forms_name,
                all_entry_forms,
                initial_values,
                initial_values_dispaly,
                initial_values_dispaly_by_slug,
                initial_values_dispaly_by_slug_entry_id,
                all_entry_forms_entry_obj,
            ) = get_entry_data(entry)
            email_template = entry.form.form_builder_email_template
            django_engine = engines["django"]
            body_html_template = django_engine.from_string(email_template.html_content)
            body_html_rendered = body_html_template.render(
                {
                    **context,
                    **initial_values_dispaly_by_slug,
                    **{"all_entries": all_entry_forms_entry_obj},
                }
            )
            body_plain_template = django_engine.from_string(email_template.content)
            body_plain_rendered = body_plain_template.render(
                {
                    **context,
                    **initial_values_dispaly_by_slug,
                    **{"all_entries": all_entry_forms_entry_obj},
                }
            )
            subject_template = django_engine.from_string(email_template.subject)
            subject_rendered = subject_template.render(
                {
                    **context,
                    **initial_values_dispaly_by_slug,
                    **{"all_entries": all_entry_forms_entry_obj},
                }
            )

            email_from = (
                email_template.email_from if email_template.email_from else None
            )
            if (
                not email_from
                and hasattr(settings, "FORM_BUILDER_EMAIL_FROM")
                and settings.FORM_BUILDER_EMAIL_FROM
            ):
                email_from = settings.FORM_BUILDER_EMAIL_FROM

            if (
                not email_from
                and hasattr(settings, "DEFAULT_FROM_EMAIL")
                and settings.DEFAULT_FROM_EMAIL
            ):
                email_from = settings.DEFAULT_FROM_EMAIL

            if email_from:
                # if hasattr(settings, 'FORM_BUILDER_EMAIL_FROM') and settings.FORM_BUILDER_EMAIL_FROM:
                #     email_from = email_template.email_from if email_template.email_from else settings.FORM_BUILDER_EMAIL_FROM

                if entry.form.email_from_is_email_field:
                    email_field_qst = entry.field_entries.filter(
                        form_field=entry.form.email_field
                    )
                    if email_field_qst.exists():
                        email_from = email_field_qst.first().value

                attachments = None

                if entry.form.send_files_email:
                    attachments = (
                        []
                    )  # should be a list of a tuple (<file_name>, <content>)
                    for a_field in entry.field_entries.exclude(file=None).exclude(
                        file=""
                    ):
                        attachment = a_field.file.file.read()
                        # attachment = requests.get(a_field.file.url).content
                        attachment_name = a_field.form_field.label
                        file_extension = a_field.file.url.split(".")[-1]
                        attachments.append(
                            (
                                "{}.{}".format(attachment_name, file_extension),
                                attachment,
                            )
                        )

                if entry.form.send_email_to_submitter:
                    form_submit = FormEntry.objects.filter(token=self.token).exclude(
                        form_submit=None
                    )
                    form_submit = form_submit.first() if form_submit.exists() else None

                    submitter_email = (
                        form_submit.form_submit.submitter_user.email
                        if form_submit
                        and form_submit.form_submit.submitter_user
                        and form_submit.form_submit.submitter_user.email
                        else None
                    )
                    submitter_email = (
                        entry.submitter.email
                        if not submitter_email
                        and entry.submitter
                        and entry.submitter.email
                        else submitter_email
                    )
                    if submitter_email:
                        send_mail(
                            str(subject_rendered),
                            str(body_plain_rendered),
                            str(body_html_rendered),
                            email_from,
                            [submitter_email],
                            fail_silently=False,
                            attachments=attachments,
                        )

                if entry.form.notify_emails:
                    for send_to in entry.form.notify_emails.split(";"):
                        send_mail(
                            str(subject_rendered),
                            str(body_plain_rendered),
                            str(body_html_rendered),
                            email_from,
                            [send_to.strip()],
                            fail_silently=False,
                            attachments=attachments,
                        )
                for user in entry.form.notify_users.all():
                    send_mail(
                        subject_rendered,
                        body_plain_rendered,
                        body_html_rendered,
                        email_from,
                        [user.email],
                        fail_silently=False,
                        attachments=attachments,
                    )

                for group in entry.form.notify_groups.all():
                    for user in User.objects.filter(groups__id=group.id):
                        send_mail(
                            subject_rendered,
                            body_plain_rendered,
                            body_html_rendered,
                            email_from,
                            [user.email],
                            fail_silently=False,
                            attachments=attachments,
                        )

    def validate_files(self, form, formset=None):
        formset_id = None
        formset_order = None
        if formset:
            fs, formset_id, formset_order = form.prefix.split("-")
        errors = {}

        all_values = []
        all_submitted_field = []
        for key, value in self.request.POST.items():
            all_submitted_field.append(key)
            try:
                all_values.append(int(value))
            except:
                pass

        for file_field in self.object.get_fields().files():
            required = False
            # for a_choice in Choice.objects.filter(field__formfield__id__in=self.object.fields.all().values_list('id', flat=True)):
            for a_choice in Choice.objects.filter(id__in=all_values):
                if file_field.required and file_field in a_choice.trigger_field.all():
                    required = True

            if required:
                if not FieldEntry.objects.filter(
                    form=self.object,
                    form_field=file_field,
                    field_name=file_field.slug,
                    submitter=get_submitter(self.request),
                    token=self.token,
                    formset_id=formset_id,
                    formset_order=formset_order,
                ).exists():
                    prefix = form.prefix + "-" if form.prefix else ""
                    field_name = "{}{}".format(prefix, file_field.slug)
                    errors.update({field_name: [_("This field is required"),]})
        if errors:
            notify = [
                {
                    "message": _("Please correct form errors marked in red"),
                    "type": self.NOTIFY_ERROR,
                }
            ]
            return (
                False,
                JsonResponse(
                    self.get_json_payload(errors=errors, notify=notify), status=400
                ),
            )
        return True, None

    def formset_invalid(self, formset):
        notify = []
        # payload = {'errors': []}
        payload = {
            "errors": {}
        }  # Errors should be a dictionary since the javascript doesn't handle lists
        for index, form in enumerate(formset):
            for error in form.errors:

                error_data = {
                    "%s-%s-%s" % (formset.prefix, index, error): form.errors[error]
                }
                # payload['errors'].append(error_data)
                payload["errors"].update(error_data)  # Update since it is a dictionary
        notify.append(
            {
                "message": _("Please correct form errors marked in red"),
                "type": self.NOTIFY_ERROR,
            }
        )
        for error in formset.non_form_errors():
            notify.append({"message": error, "type": self.NOTIFY_ERROR})
        payload.update(notify=notify)

        # if self.request.is_ajax():
        return JsonResponse(self.get_json_payload(**payload), status=400)
        # else:
        #     return self.render_to_response(self.get_context_data(form=form))

    def formset_valid(self, formset):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        if self.request.is_ajax():
            return JsonResponse(self.get_json_payload(redirect=True))
        else:
            return HttpResponseRedirect(self.get_success_url())

    def get_forms_tree(self):
        return self.object.get_tree()

    def get_context_data(self, **kwargs):
        kwargs = super(BaseFormMixin, self).get_context_data(**kwargs)
        submitter = get_submitter(self.request)
        formsets = []
        if self.object.has_formset_childrens():
            for i, form_model in enumerate(self.object.get_children()):
                if form_model.is_formset:
                    form = form_factory(
                        form_model,
                        token=self.token,
                        request=self.request,
                        update=self.update,
                        is_formset=True,
                    )
                    initial = self.get_initial_for_formset(form_model)
                    extra = 0 if initial else 1

                    formset = self.get_formset_class(
                        form=form,
                        form_model=form_model,
                        extra=extra,
                        token=self.token,
                        submitter=submitter,
                    )
                    formset = formset(
                        prefix="formset-%s" % (form_model.pk,), initial=initial
                    )
                    setattr(formset, "form_model_name", form_model.name)
                    formsets.append(formset)

            kwargs["children_formsets"] = formsets
        kwargs["is_final"] = not bool(kwargs.get("object", None))
        return kwargs

    def get_initial_for_formset(self, form_model):
        initial = []
        form_entries = FormEntry.objects.filter(
            # parent__form=form_model,
            token=self.token,
            submitter=get_submitter(self.request),
            form__is_formset=True,
        )
        for form_entry in form_entries:
            form_entry_dict = {}
            for field_entry in form_entry.field_entries.all():
                form_entry_dict.update({field_entry.form_field.slug: field_entry.value})
            if form_entry_dict:
                initial.append(form_entry_dict)
        return initial

    @cached_property
    def token(self):
        form_model = self.get_object()
        session_key = "{}-token".format(form_model.name)
        is_root = form_model.is_root()
        submitter = get_submitter(self.request)
        form_entry = FormEntry.objects.filter(
            submitter=submitter,
            form=form_model.get_root(),
            form_submit__isnull=True,
        )
        if form_entry.count() == 0 or not submitter:
            # first entry
            # token = self.request.session.get(session_key, None)
            # if not token:
            token = uuid.uuid4()
        else:
            token = form_entry.first().token
            # self_form_entry = FormEntry.objects.filter(
            #     submitter=get_submitter(self.request),
            #     form=form_model,
            #     form_submit__isnull=True,
            # )
            # self_form_submited = FormEntry.objects.filter(
            #     submitter=get_submitter(self.request),
            #     form=form_model,
            #     form_submit__isnull=False,
            # )
            # if form_entry.first().form.multiple and self_form_entry.exists() and not form_entry.first().form.sequential:
            #     # form can have multiple entries, return new token uuid
            #     token = uuid.uuid4()
            # # elif form_entry.first().form.sequential and self_form_submited.exists() and \
            # #         (self_form_submited.first().form_submit.created + datetime.timedelta(
            # #     days=form_entry.first().form.days_next_submit)).date() > datetime.date.today():
            # #     token = uuid.uuid4()
            # else: #, if we have entry for the form then return that token
            #     token = form_entry.first().token
        if is_root:
            self.request.session[session_key] = str(token)
        return token
