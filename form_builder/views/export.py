# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from io import BytesIO
import requests
import zipfile

from django.http import HttpResponse
from django.views.generic import View
from django.views.generic.detail import SingleObjectMixin

from wkhtmltopdf.views import PDFTemplateView

from form_builder.models import FormSubmit, FieldEntry
from ..utils import get_submitter


class SubmitExportMixin(SingleObjectMixin):
    model = FormSubmit
    context_object_name = "submit"

    def get_queryset(self):
        user = get_submitter(self.request)
        if user.is_superuser or user.has_perm("form_builder.can_export_formsubmit"):
            return FormSubmit.objects.all()
        else:
            return FormSubmit.objects.filter(entries__submitter=user)


class SubmitToPDFView(SubmitExportMixin, PDFTemplateView):
    """
    Render all the entries of a submit to PDF
    """

    template_name = "form_builder/pdf/content.html"
    # header_template = 'form_builder/pdf/header.html'
    # footer_template = 'form_builder/pdf/footer.html'
    show_content_in_browser = True

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(SubmitToPDFView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super(SubmitToPDFView, self).get_context_data(**kwargs)
        ctx["master_form"] = self.object.form
        ctx["entries"] = self.object.entries.all().order_by("form__path")
        return ctx

    def get_filename(self):
        return "%s-%s.pdf" % (self.object.form.slug, self.object.submitter)


class DownloadSubmitAttachmentsView(SubmitExportMixin, View):
    """
    Export all uploaded files into a zip file.
    """

    filename = "attachments.zip"

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return self.render_to_zip()

    def get_filename(self):
        return "%s-%s.zip" % (self.object.form.slug, self.object.submitter)

    def render_to_zip(self):
        submit = self.object
        field_entries = FieldEntry.objects.filter(entry__form_submit=submit.pk).files()
        temp_file = BytesIO()

        if field_entries.count() > 0:
            with zipfile.ZipFile(
                temp_file, mode="w", compression=zipfile.ZIP_DEFLATED
            ) as zip_file:
                for i, field_entry in enumerate(field_entries):
                    if (
                        hasattr(field_entry.file, "url")
                        and field_entry.file.url
                        and (
                            field_entry.file.url.startswith("http://")
                            or field_entry.file.url.startswith("https://")
                        )
                    ):
                        data = requests.get(field_entry.file.url)
                        extension = (
                            field_entry.file.url.split(".")[-1]
                            if len(data.url.split(".")) > 0
                            else ""
                        ).split("?")[0]
                        zip_info = zipfile.ZipInfo(
                            filename="{}.{}".format(field_entry.field_name, extension)
                        )
                        zip_file.writestr(zip_info, data=data.content)
                    else:
                        zip_file.write(
                            field_entry.file.path,
                            arcname="%s_%s" % (submit.form.slug, field_entry.file.name),
                        )

        file_size = temp_file.tell()
        temp_file.seek(0)
        response = HttpResponse(
            temp_file.getvalue(), content_type="application/force-download"
        )
        response["Content-Disposition"] = (
            'attachment; filename="%s"' % self.get_filename()
        )
        response["Content-Length"] = file_size
        return response
