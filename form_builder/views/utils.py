# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import re
from form_builder.forms.utils import form_factory
from form_builder import settings as form_settings
from form_builder.models import FormEntry


def get_submit_data(submit, request):
    initial_values = (
        {}
    )  # Used as initial values for forms that have active "enable edit"
    initial_values_dispaly = (
        {}
    )  # Used as initial values for forms that have inactive "enable edit", just display
    initial_values_dispaly_by_slug = {}
    initial_values_dispaly_by_slug_entry_id = {}
    all_forms_name = []
    all_entry_forms = {}  # carry forms based on the submission.entries (form entries)
    all_entry_forms_entry_obj = {}
    for entry in submit.entries.filter(form__is_formset=False).order_by(
        "id"
    ):  # Loop through single forms (not formset)
        field_entries = entry.field_entries.all()
        if field_entries.exists():  # Consider the form if only it has field entries
            all_forms_name.append(
                entry.form.name
            )  # when the form is not a formset, the dic keys are form.name
            initial_values[entry.form.name] = {}
            initial_values_dispaly[entry.form.name] = {}
            form_key = entry.form.slug.replace("-", "_")
            initial_values_dispaly_by_slug[form_key] = {}
            initial_values_dispaly_by_slug_entry_id[entry.form.name] = {}
            for field_entry in field_entries:
                # We consider the latest field_entry
                if (
                    field_entry.form_field
                    and field_entry.form_field.slug
                    not in initial_values_dispaly[entry.form.name]
                ):
                    initial_values_dispaly[entry.form.name][
                        field_entry.form_field.slug
                    ] = field_entry.get_html_value()
                    initial_values_dispaly_by_slug[form_key][
                        field_entry.form_field.slug
                    ] = field_entry.get_html_value()
                    initial_values_dispaly_by_slug_entry_id[entry.form.name][
                        field_entry.form_field.slug
                    ] = field_entry.id
                if (
                    field_entry.form_field
                    and field_entry.form_field.slug
                    not in initial_values[entry.form.name]
                ):
                    if (
                        field_entry.form_field.field_class
                        in form_settings.ChoiceWithOtherField
                    ):
                        initial_values[entry.form.name][
                            field_entry.form_field.slug
                        ] = field_entry.value.split(";")
                    elif (
                        field_entry.form_field.field_class
                        in form_settings.MultipleChoiceField
                    ):
                        pattern = re.compile(r"\d+")
                        initial_values[entry.form.name][
                            field_entry.form_field.slug
                        ] = pattern.findall(field_entry.value)
                    else:
                        initial_values[entry.form.name][
                            field_entry.form_field.slug
                        ] = field_entry.get_html_value()

            all_entry_forms_entry_obj[entry.form.name] = entry
            if request and request.method == "GET":  # create form with initials
                all_entry_forms[entry.form.name] = form_factory(
                    form_model=entry.form,
                    token=submit.entries.all()[0].token,
                    request=request,
                )
            elif request:  # create form without initial
                all_entry_forms[entry.form.name] = form_factory(
                    form_model=entry.form,
                    token=submit.entries.all()[0].token,
                    request=request,
                )

    for entry in submit.entries.filter(form__is_formset=True).order_by(
        "id"
    ):  # Loop through formset
        field_entries = entry.field_entries.all()
        n = 0
        n = len([x for x in all_forms_name if "{}_{}".format(entry.form.name, n) == x])
        if field_entries.exists():
            initial_values[
                "{}_{}".format(entry.form.name, n)
            ] = {}  # in formset, the dict keys are form.name + _n
            initial_values_dispaly["{}_{}".format(entry.form.name, n)] = {}
            form_key = entry.form.slug.replace("-", "_")
            initial_values_dispaly_by_slug["{}_{}".format(form_key, n)] = {}
            initial_values_dispaly_by_slug_entry_id[
                "{}_{}".format(entry.form.name, n)
            ] = {}
            for field_entry in field_entries:
                # We consider the latest field_entry
                if (
                    field_entry.form_field
                    and "{}".format(field_entry.form_field.slug)
                    not in initial_values_dispaly["{}_{}".format(entry.form.name, n)]
                ):
                    initial_values_dispaly["{}_{}".format(entry.form.name, n)][
                        "{}".format(field_entry.form_field.slug)
                    ] = field_entry.get_html_value()
                    initial_values_dispaly_by_slug["{}_{}".format(form_key, n)][
                        "{}".format(field_entry.form_field.slug)
                    ] = field_entry.get_html_value()
                    initial_values_dispaly_by_slug_entry_id[
                        "{}_{}".format(entry.form.name, n)
                    ]["{}".format(field_entry.form_field.slug)] = field_entry.id
                if (
                    field_entry.form_field
                    and field_entry.form_field.slug
                    not in initial_values["{}_{}".format(entry.form.name, n)]
                ):
                    if (
                        field_entry.form_field.field_class
                        in form_settings.ChoiceWithOtherField
                    ):
                        initial_values["{}_{}".format(entry.form.name, n)][
                            field_entry.form_field.slug
                        ] = field_entry.value.split(";")

                    elif (
                        field_entry.form_field.field_class
                        in form_settings.MultipleChoiceField
                    ):
                        pattern = re.compile(r"\d+")
                        initial_values["{}_{}".format(entry.form.name, n)][
                            field_entry.form_field.slug
                        ] = pattern.findall(field_entry.value)
                    else:
                        initial_values["{}_{}".format(entry.form.name, n)][
                            field_entry.form_field.slug
                        ] = field_entry.get_html_value()

            all_entry_forms_entry_obj["{}_{}".format(entry.form.name, n)] = entry
            all_forms_name.append("{}_{}".format(entry.form.name, n))
            if request and request.method == "GET":  # create form with initials
                all_entry_forms["{}_{}".format(entry.form.name, n)] = form_factory(
                    form_model=entry.form,
                    is_formset=True,
                    token=submit.entries.all()[0].field_entries.all()[0].token,
                    request=request,
                )
            elif request:  # create form without initials
                all_entry_forms["{}_{}".format(entry.form.name, n)] = form_factory(
                    form_model=entry.form,
                    is_formset=True,
                    token=submit.entries.all()[0].field_entries.all()[0].token,
                    request=request,
                )

    return (
        all_forms_name,
        all_entry_forms,
        initial_values,
        initial_values_dispaly,
        initial_values_dispaly_by_slug,
        initial_values_dispaly_by_slug_entry_id,
        all_entry_forms_entry_obj,
    )


def get_entry_data(entry, request=None):
    initial_values = (
        {}
    )  # Used as initial values for forms that have active "enable edit"
    initial_values_dispaly = (
        {}
    )  # Used as initial values for forms that have inactive "enable edit", just display
    initial_values_dispaly_by_slug = {}
    initial_values_dispaly_by_slug_entry_id = {}
    all_forms_name = []
    all_entry_forms = {}  # carry forms based on the submission.entries (form entries)
    all_entry_forms_entry_obj = {}
    token = entry.token

    for entry in FormEntry.objects.filter(form__is_formset=False, token=token).order_by(
        "id"
    ):  # Loop through single forms (not formset)
        field_entries = entry.field_entries.all()
        if field_entries.exists():  # Consider the form if only it has field entries
            all_forms_name.append(
                entry.form.name
            )  # when the form is not a formset, the dic keys are form.name
            initial_values[entry.form.name] = {}
            initial_values_dispaly[entry.form.name] = {}
            form_key = entry.form.slug.replace("-", "_")
            initial_values_dispaly_by_slug[form_key] = {}
            initial_values_dispaly_by_slug_entry_id[entry.form.name] = {}
            for field_entry in field_entries:
                # We consider the latest field_entry
                if (
                    field_entry.form_field
                    and field_entry.form_field.slug
                    not in initial_values_dispaly[entry.form.name]
                ):
                    initial_values_dispaly[entry.form.name][
                        field_entry.form_field.slug
                    ] = field_entry.get_html_value()
                    initial_values_dispaly_by_slug[form_key][
                        field_entry.form_field.slug
                    ] = field_entry.get_html_value()
                    initial_values_dispaly_by_slug_entry_id[entry.form.name][
                        field_entry.form_field.slug
                    ] = field_entry.id
                if (
                    field_entry.form_field
                    and field_entry.form_field.slug
                    not in initial_values[entry.form.name]
                ):
                    if (
                        field_entry.form_field.field_class
                        in form_settings.ChoiceWithOtherField
                    ):
                        initial_values[entry.form.name][
                            field_entry.form_field.slug
                        ] = field_entry.value.split(";")

                    elif (
                        field_entry.form_field.field_class
                        in form_settings.MultipleChoiceField
                    ):
                        pattern = re.compile(r"\d+")
                        initial_values[entry.form.name][
                            field_entry.form_field.slug
                        ] = pattern.findall(field_entry.value)
                    else:
                        initial_values[entry.form.name][
                            field_entry.form_field.slug
                        ] = field_entry.get_html_value()

            all_entry_forms_entry_obj[entry.form.name] = entry
            if request and request.method == "GET":  # create form with initials
                all_entry_forms[entry.form.name] = form_factory(
                    form_model=entry.form, token=token, request=request
                )
            elif request:  # create form without initial
                all_entry_forms[entry.form.name] = form_factory(
                    form_model=entry.form, token=token, request=request
                )

    for entry in FormEntry.objects.filter(form__is_formset=True, token=token).order_by(
        "id"
    ):  # Loop through formset
        field_entries = entry.field_entries.all()
        n = 0
        n = len([x for x in all_forms_name if "{}_{}".format(entry.form.name, n) == x])
        if field_entries.exists():
            initial_values[
                "{}_{}".format(entry.form.name, n)
            ] = {}  # in formset, the dict keys are form.name + _n
            initial_values_dispaly["{}_{}".format(entry.form.name, n)] = {}
            form_key = entry.form.slug.replace("-", "_")
            initial_values_dispaly_by_slug["{}_{}".format(form_key, n)] = {}
            initial_values_dispaly_by_slug_entry_id[
                "{}_{}".format(entry.form.name, n)
            ] = {}
            for field_entry in field_entries:
                # We consider the latest field_entry
                if (
                    field_entry.form_field
                    and "{}".format(field_entry.form_field.slug)
                    not in initial_values_dispaly["{}_{}".format(entry.form.name, n)]
                ):
                    initial_values_dispaly["{}_{}".format(entry.form.name, n)][
                        "{}".format(field_entry.form_field.slug)
                    ] = field_entry.get_html_value()
                    initial_values_dispaly_by_slug["{}_{}".format(form_key, n)][
                        "{}".format(field_entry.form_field.slug)
                    ] = field_entry.get_html_value()
                    initial_values_dispaly_by_slug_entry_id[
                        "{}_{}".format(entry.form.name, n)
                    ]["{}".format(field_entry.form_field.slug)] = field_entry.id
                if (
                    field_entry.form_field
                    and field_entry.form_field.slug
                    not in initial_values["{}_{}".format(entry.form.name, n)]
                ):
                    if (
                        field_entry.form_field.field_class
                        in form_settings.ChoiceWithOtherField
                    ):
                        initial_values["{}_{}".format(entry.form.name, n)][
                            field_entry.form_field.slug
                        ] = field_entry.value.split(";")

                    elif (
                        field_entry.form_field.field_class
                        in form_settings.MultipleChoiceField
                    ):
                        pattern = re.compile(r"\d+")
                        initial_values["{}_{}".format(entry.form.name, n)][
                            field_entry.form_field.slug
                        ] = pattern.findall(field_entry.value)
                    else:
                        initial_values["{}_{}".format(entry.form.name, n)][
                            field_entry.form_field.slug
                        ] = field_entry.get_html_value()

            all_entry_forms_entry_obj["{}_{}".format(entry.form.name, n)] = entry
            all_forms_name.append("{}_{}".format(entry.form.name, n))
            if request and request.method == "GET":  # create form with initials
                all_entry_forms["{}_{}".format(entry.form.name, n)] = form_factory(
                    form_model=entry.form, is_formset=True, token=token, request=request
                )
            elif request:  # create form without initials
                all_entry_forms["{}_{}".format(entry.form.name, n)] = form_factory(
                    form_model=entry.form, is_formset=True, token=token, request=request
                )
    return (
        all_forms_name,
        all_entry_forms,
        initial_values,
        initial_values_dispaly,
        initial_values_dispaly_by_slug,
        initial_values_dispaly_by_slug_entry_id,
        all_entry_forms_entry_obj,
    )
