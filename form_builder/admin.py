# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# from random import randint
# import sys
# import traceback
import copy
import random
import string

# from .translation import *
from adminsortable.admin import NonSortableParentAdmin, SortableTabularInline
import django
from django.contrib.admin.options import *
from django import forms
from django.conf import settings
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User

try:
    from django.shortcuts import reverse
except ImportError:
    from django.core.urlresolvers import reverse
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from django.utils.html import format_html
from django.http import HttpResponse
from django.template.response import TemplateResponse
from django.contrib.sites.models import Site
from django.template import engines
from django.shortcuts import HttpResponseRedirect

try:
    from django.urls import resolve
except ImportError:
    from django.core.urlresolvers import resolve

from treebeard.admin import TreeAdmin
from treebeard.forms import movenodeform_factory
from import_export import resources
from import_export.admin import ImportExportModelAdmin, ImportForm, ConfirmImportForm
from modeltranslation.admin import (
    TabbedDjangoJqueryTranslationAdmin,
    TranslationTabularInline,
)
from adminsortable.admin import (
    SortableTabularInline,
    SortableStackedInline,
    SortableAdmin,
)

from . import settings as form_settings
from .models import (
    Workflow,
    FormType,
    Field,
    Form,
    FormField,
    Workflow,
    FormSubmit,
    FormEntry,
    FieldEntry,
    Condition,
    ConditionalRedirect,
    Choice,
    FormVerificationCode,
    EmailTemplate,
)

from inline_actions.admin import InlineActionsMixin
from inline_actions.admin import InlineActionsModelAdminMixin
from .utils import (
    MyChangeList,
    smart_truncate,
)  # show filter even if it has only one choice

if "mailer" in settings.INSTALLED_APPS:
    from mailer import send_mail, send_html_mail
else:
    from django.core.mail import send_mail

try:
    from django.utils.encoding import force_text
except ImportError:
    from django.utils.encoding import force_unicode as force_text

User = get_user_model()

ENTRY_TIME_FIELDS = (_("Entry time"), {"fields": (("created", "modified"),),})


class ChoiceInlines(SortableStackedInline):
    model = Choice
    extra = 0
    # template = 'form_builder/custom_adminsortable/edit_inline/stacked.html'

    # class Media:
    #     js = (
    #         "/static/js/jquery-1.11.3.js",
    #         "/static/js/field.js"
    #     )

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == "trigger_field":
            resolved = resolve(request.path_info)
            if resolved.args:
                field = self.parent_model.objects.get(pk=resolved.args[0])
                field_ids = []
                for fieldform in field.formfield_set.all():
                    for form_fieldform in fieldform.form.fields.all():
                        field_ids.append(form_fieldform.field.id)
                kwargs["queryset"] = Field.objects.filter(id__in=field_ids)

        return super(ChoiceInlines, self).formfield_for_manytomany(
            db_field, request=None, **kwargs
        )


class FormTypeAdmin(TabbedDjangoJqueryTranslationAdmin):
    list_display = ["name", "permitted_users", "permitted_groups"]

    def permitted_users(self, obj):
        return " - ".join(obj.users.all().values_list("username", flat=True))

    def permitted_groups(self, obj):
        return " - ".join(obj.groups.all().values_list("name", flat=True))

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == "users":
            kwargs["queryset"] = User.objects.filter(is_staff=True)
        return super(FormTypeAdmin, self).formfield_for_manytomany(
            db_field, request, **kwargs
        )


class FieldAdmin(SortableAdmin, TabbedDjangoJqueryTranslationAdmin):
    list_display = ("__str__", "used_in_forms")
    search_fields = ("display_name",)
    readonly_fields = ("created", "modified")
    fieldsets = (
        (
            _("Settings"),
            {
                "fields": (
                    "display_name",
                    "get_dynamic_choice",
                    "help_text",
                    "placeholder_text",
                    "send_success_email",
                    "initial",
                    "above_field_paragraph",
                    "below_field_paragraph",
                ),
                "classes": ("collapse",),
            },
        ),
        (
            _("Validators"),
            {
                "fields": (
                    "extra_validator",
                    "regex",
                    "max_length",
                    "min_length",
                    "max_value",
                    "min_value",
                    "max_digits",
                    "max_date",
                    "max_date_message",
                    "min_date",
                    "min_date_message",
                    "decimal_places",
                    "min_size",
                    "max_size",
                    "allowed_file_types",
                ),
                "classes": ("collapse",),
            },
        ),
        ENTRY_TIME_FIELDS,
    )
    inlines = [ChoiceInlines]
    save_as = True

    def used_in_forms(self, obj):
        output = ""
        for form_field in FormField.objects.filter(field=obj):
            output += """<a href="{}">{}</a><br>""".format(
                reverse("admin:form_builder_form_change", args=(obj.id,)),
                form_field.form.name,
            )

        return format_html(output)

    # def validation_rule(self, obj):
    #     if obj.field_class in form_settings.FIELD_CLASSES_validation_help:
    #         return form_settings.FIELD_CLASSES_validation_help[obj.field_class]


class FormFieldInline(SortableTabularInline, TranslationTabularInline):
    model = FormField
    extra = 0
    fields = (
        "slug",
        "field_class",
        "label",
        "field",
        "required",
        "is_filter",
        "is_display",
        "show_chart",
        "hidden_by_default",
        "unique_form_tree",
        "is_active",
    )


class VerificationCodeInline(InlineActionsMixin, admin.TabularInline):
    model = FormVerificationCode
    extra = 0
    fields = ("file_number", "verification_code", "company_name", "email", "email_sent")
    readonly_fields = ("verification_code", "email_sent")
    inline_actions = ["send_verification"]

    def send_verification(self, request, obj, parent_obj=None):
        domain = Site.objects.get_current().domain
        if hasattr(settings, "LANGUAGE_CODE"):
            submit_link = "".join(
                [
                    "http://",
                    domain,
                    "/{}".format(settings.LANGUAGE_CODE),
                    obj.form.get_root().get_absolute_url_no_i18n(),
                ]
            )
        else:
            submit_link = "".join(
                ["http://", domain, obj.form.get_root().get_absolute_url_no_i18n()]
            )
        context = {
            "form": obj.form.get_root(),
            "verification_code": obj.verification_code,
            "company_name": obj.company_name,
            "submit_link": submit_link,
        }
        if obj.form.verification_code_email_template:
            email_template = obj.form.verification_code_email_template
            django_engine = engines["django"]
            body_html_template = django_engine.from_string(email_template.html_content)
            body_html_rendered = body_html_template.render(context)
            body_plain_template = django_engine.from_string(email_template.content)
            body_plain_rendered = body_plain_template.render(context)
            subject_template = django_engine.from_string(email_template.subject)
            subject_rendered = subject_template.render(context)

            if (
                hasattr(settings, "FORM_BUILDER_EMAIL_FROM")
                and settings.FORM_BUILDER_EMAIL_FROM
                and obj.is_active
            ):
                email_from = (
                    email_template.email_from
                    if email_template.email_from
                    else settings.FORM_BUILDER_EMAIL_FROM
                )
                try:
                    send_html_mail(
                        subject_rendered,
                        body_plain_rendered,
                        body_html_rendered,
                        email_from,
                        [obj.email],
                        fail_silently=False,
                    )
                except:
                    send_mail(
                        subject_rendered,
                        body_plain_rendered,
                        email_from,
                        [obj.email],
                        fail_silently=False,
                        html_message=body_html_rendered,
                    )
                obj.email_sent = True
                obj.save()
        url = reverse(
            "admin:{}_{}_change".format(obj._meta.app_label, obj._meta.model_name,),
            args=(obj.pk,),
        )
        url = (
            reverse("admin:form_builder_form_change", args=(obj.form.id,))
            + "#/tab/inline_0/"
        )

        return HttpResponseRedirect(url)

    send_verification.short_description = _("Send Verification")


class FormVerificationCodeResource(resources.ModelResource):
    class Meta:
        model = FormVerificationCode
        import_id_fields = ("file_number",)
        # fields = ('id', 'form', 'form__name', 'file_number', 'verification_code', 'company_name', 'email', )
        fields = (
            "file_number",
            "company_name",
            "email",
        )

    def __init__(self, **kwargs):
        self.request = kwargs.get("request", None)
        super(FormVerificationCodeResource, self).__init__()

    def before_import(self, dataset, dry_run, **kwargs):
        if self.request and self.request.GET.get("form_id", None):
            try:
                form_id = self.request.GET.get("form_id", None)
                form_id_list = [form_id for row_number in range(len(dataset.dict))]
                dataset.append_col(form_id_list, header="form_id")
            except:
                super(FormVerificationCodeResource, self).before_import(
                    dataset, dry_run, **kwargs
                )
        else:
            super(FormVerificationCodeResource, self).before_import(
                dataset, dry_run, **kwargs
            )

    def init_instance(self, row=None):
        form = None

        if (
            "form_id" in row
            and row["form_id"]
            and Form.objects.filter(id=row["form_id"]).exists()
        ):
            form = Form.objects.filter(id=row["form_id"]).first()

        if (
            "form" in row
            and row["form"]
            and Form.objects.filter(id=row["form"]).exists()
        ):
            form = Form.objects.filter(id=row["form"]).first()

        if (
            "form name" in row
            and row["form name"]
            and Form.objects.filter(name=row["form name"]).exists()
        ):
            form = Form.objects.filter(name=row["form name"]).first()

        if form:
            instance = FormVerificationCode(
                form=form,
                file_number=row["file_number"],
                company_name=row["company_name"],
                email=row["email"],
            )
            if "email_template" in row and row["email_template"]:
                email_template = EmailTemplate.objects.filter(
                    name=row["email_template"]
                )
                if email_template.exists():
                    instance.email_template.add(email_template.first())
            if instance:
                return instance
            else:
                raise NotImplementedError()
        else:
            raise NotImplementedError()


class FormAdmin(
    NonSortableParentAdmin, SortableAdmin, TreeAdmin, TabbedDjangoJqueryTranslationAdmin
):
    form = movenodeform_factory(Form)
    list_display = (
        "name",
        "site_view",
        "submission_view",
        "go_workflow",
        "choice_chart",
        "export_submission",
        "import_verification",
        "is_active",
    )
    list_editable = ("is_active",)
    prepopulated_fields = {"slug": ("name",)}
    readonly_fields = ("created", "modified")
    save_as = True
    # filter_horizontal = ('notify_users',)  # Doesn't work with Jet, since we use bootstrap select
    inlines = [VerificationCodeInline, FormFieldInline]
    change_form_template_extends = (
        "admin/form_builder/import_export/change_form_extend.html"
    )
    search_fields = ["name", "slug", "description"]
    list_filter = [
        "login_required",
        "use_verification",
        "enable_edit_submit",
        ("form_type", admin.RelatedOnlyFieldListFilter),
    ]

    actions = [
        "send_verification_code_email",
    ]

    # class Media:
    #     # js = (
    #     #     'modeltranslation/js/force_jquery.js',
    #     #     'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js',
    #     #     'modeltranslation/js/tabbed_translation_fields.js',
    #     # )
    #     css = {
    #         'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
    #     }

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == "notify_users":
            kwargs["queryset"] = User.objects.filter(is_staff=True)

        if db_field.name == "permission":
            kwargs["queryset"] = User.objects.filter(is_staff=True)
        return super(FormAdmin, self).formfield_for_manytomany(
            db_field, request, **kwargs
        )

    def copy_form_field(self, old_obj, new_obj):
        for formfield in old_obj.fields.all():
            new_formfield = copy.copy(formfield)
            # Comment the following lines to stop clone the Field settings
            new_field = copy.copy(formfield.field)
            new_field.pk = None
            new_field.display_name = new_field.display_name + "_{}".format(
                new_obj.slug.replace("-", "_")
            )
            new_field.save()

            for choice in formfield.field.choices.all():
                new_choice = copy.copy(choice)
                new_choice.pk = None
                new_choice.field = new_field
                new_choice.save()
            # \Comment the following lines to stop clone the Field settings
            new_formfield.pk = None
            new_formfield.form = new_obj
            new_formfield.field = new_field
            new_formfield.save()

    def copy_form_children(self, old_obj, new_obj, prefix):
        for child in Form.objects.get(id=old_obj.id).get_children():
            old_child = copy.copy(child)
            new_child = child
            new_child.pk = None
            new_child.slug = "{}_{}".format(new_obj.slug, new_child.slug)
            new_child.name = "{}_{}".format(new_child.name, prefix)
            if hasattr(new_child, "name_en"):
                new_child.name_en = new_child.name
            if hasattr(new_child, "name_ar"):
                new_child.name_ar = "{}_{}".format(new_child.name_ar, prefix)
            new_obj.add_child(instance=new_child)
            new_child.numchild = 0
            new_child.save()
            self.copy_form_field(old_obj=old_child, new_obj=new_child)

            if len(old_child.get_children()) > 0:
                self.copy_form_children(
                    old_obj=old_child, new_obj=new_child, prefix=prefix
                )

    @csrf_protect_m
    # @transaction.atomic # We need to commit this decorator to allow save the tree node before we add children
    def changeform_view(self, request, object_id=None, form_url="", extra_context=None):
        # override this to copy the formfield instead to refer to the same old records
        if request.method == "POST" and "_saveasnew" in request.POST:
            old_object = self.get_object(request=request, object_id=object_id)
            object_id = None
            add = object_id is None
            if not self.has_add_permission(request):
                raise PermissionDenied
            obj = None
            ModelForm = self.get_form(request, obj)
            form = ModelForm(request.POST, request.FILES, instance=obj)
            if form.is_valid():
                form_validated = True
                new_object = self.save_form(request, form, change=not add)
            else:
                form_validated = False
                new_object = form.instance
            formsets, inline_instances = self._create_formsets(
                request, new_object, change=not add
            )
            if all_valid(formsets) and form_validated:
                self.save_model(request, new_object, form, not add)
                self.save_related(request, form, formsets, not add)
                # letters = string.ascii_lowercase
                # prefix = ''.join(random.choice(letters) for i in range(3))
                seq_prefix = 1
                found_prefix = (
                    False
                    if Form.objects.filter(
                        name=new_object.name + "_{}".format(str(seq_prefix))
                    ).exists()
                    else True
                )
                while not found_prefix:
                    found_prefix = (
                        False
                        if Form.objects.filter(
                            name=new_object.name + "_{}".format(str(seq_prefix))
                        ).exists()
                        else True
                    )
                    seq_prefix = seq_prefix if found_prefix else seq_prefix + 1
                prefix = str(seq_prefix)
                new_object.name = "{}_{}".format(new_object.name, prefix)
                if hasattr(new_object, "name_en"):
                    new_object.name_en = new_object.name
                if hasattr(new_object, "name_ar"):
                    new_object.name_ar = "{}_{}".format(new_object.name_ar, prefix)
                new_object.save()
                self.copy_form_children(
                    old_obj=old_object, new_obj=new_object, prefix=prefix
                )

                new_object.fields.all().delete()
                self.copy_form_field(old_obj=old_object, new_obj=new_object)

                change_message = self.construct_change_message(
                    request, form, formsets, add
                )
                if add:
                    self.log_addition(request, new_object, change_message)
                    return self.response_add(request, new_object)
                else:
                    self.log_change(request, new_object, change_message)
                    return self.response_change(request, new_object)

        return super(FormAdmin, self).changeform_view(
            request, object_id=object_id, form_url=form_url, extra_context=extra_context
        )

    def send_verification_code_email(self, request, queryset):
        for obj in queryset:
            for verification in FormVerificationCode.objects.filter(form=obj):
                domain = Site.objects.get_current().domain
                if hasattr(settings, "LANGUAGE_CODE"):
                    submit_link = "".join(
                        [
                            "http://",
                            domain,
                            "/{}".format(settings.LANGUAGE_CODE),
                            obj.get_root().get_absolute_url_no_i18n(),
                        ]
                    )
                else:
                    submit_link = "".join(
                        ["http://", domain, obj.get_root().get_absolute_url_no_i18n()]
                    )
                context = {
                    "form": obj.get_root(),
                    "verification_code": verification.verification_code,
                    "company_name": verification.company_name,
                    "submit_link": submit_link,
                }
                if obj.verification_code_email_template:
                    email_template = obj.verification_code_email_template
                    django_engine = engines["django"]
                    body_html_template = django_engine.from_string(
                        email_template.html_content
                    )
                    body_html_rendered = body_html_template.render(context)
                    body_plain_template = django_engine.from_string(
                        email_template.content
                    )
                    body_plain_rendered = body_plain_template.render(context)
                    subject_template = django_engine.from_string(email_template.subject)
                    subject_rendered = subject_template.render(context)
                    if (
                        hasattr(settings, "FORM_BUILDER_EMAIL_FROM")
                        and settings.FORM_BUILDER_EMAIL_FROM
                        and verification.is_active
                    ):
                        email_from = (
                            email_template.email_from
                            if email_template.email_from
                            else settings.FORM_BUILDER_EMAIL_FROM
                        )
                        try:
                            send_html_mail(
                                subject_rendered,
                                body_plain_rendered,
                                body_html_rendered,
                                email_from,
                                [verification.email],
                                fail_silently=False,
                            )
                        except:
                            send_mail(
                                subject_rendered,
                                body_plain_rendered,
                                email_from,
                                [verification.email],
                                fail_silently=False,
                                html_message=body_html_rendered,
                            )
                        verification.email_sent = True
                        verification.save()

    send_verification_code_email.short_description = _(
        "Send Verification Codes by Email"
    )

    def get_queryset(self, request):
        queryset = super(FormAdmin, self).get_queryset(request)
        # queryset = queryset.filter(~Q(name="Resubmit Fields"))
        if request.user.is_superuser:
            return queryset
        else:
            form_type = list(
                request.user.formtype_set.all().values_list("id", flat=True)
            )
            for a_group in request.user.groups.all():
                form_type += list(
                    a_group.formtype_set.all().values_list("id", flat=True)
                )
            return queryset.filter(
                Q(form_type__id__in=form_type) | Q(permission=request.user)
            )

    def submission_view(self, obj):
        if obj.is_root():
            try:
                url = format_html(
                    """
                <a href="{}" target="_blank">{}</a>
                """.format(
                        reverse("admin:form_builder_formsubmit_changelist")
                        + "?form__id__exact="
                        + str(obj.id),
                        _("Submission Page"),
                    )
                )
                return url
            except:
                pass

    def go_workflow(self, obj):
        if obj.workflow:
            return format_html(
                """
                <a href="{}" target="_blank">{}</a>
                """.format(
                    reverse("admin:form_builder_workflow_changelist")
                    + "?form="
                    + str(obj.id),
                    _("Workflow"),
                )
            )

    def view_on_site(self, obj=None):
        if obj:
            try:
                return reverse("form_builder:create", kwargs={"path": obj.slug})
            except:
                pass

    def site_view(self, obj):
        if obj.is_root():
            try:
                url = format_html(
                    """
                <a href="{}" target="_blank">{}</a>
                """.format(
                        reverse("form_builder:create", kwargs={"path": obj.slug}),
                        _("View on site"),
                    )
                )
                return url
            except:
                pass

    site_view.short_description = _("View on site")

    def choice_chart(self, obj):
        if obj.is_root():
            # try:
            url = format_html(
                """
            <a href="{}" target="_blank">{}</a>
            """.format(
                    reverse("form_builder:charts2_form", kwargs={"path": obj.slug}),
                    _("View Charts"),
                )
            )
            return url
            # except:
            #     pass

    def export_submission(self, obj):
        if obj.is_root():
            try:
                url = format_html(
                    """
                <a href="{}" target="_blank" class="">{}</a>
                """.format(
                        reverse("form_builder:export_all", args=(obj.slug,)),
                        _("Export Submissions"),
                    )
                )
                return url
            except:
                pass

    def import_verification(self, obj):
        if obj.is_root():
            try:
                url = format_html(
                    """
                <a href="{}" target="_blank" class="">{}</a>
                """.format(
                        reverse("admin:form_builder_formverificationcode_changelist")
                        + "import/?form_id="
                        + str(obj.id),
                        _("Import Verification Codes"),
                    )
                )
                return url
            except:
                pass

    def get_prepopulated_fields(self, request, obj=None):
        if form_settings.EDITABLE_SLUG:
            return {"slug": ("name_en",)}

    def get_fieldsets(self, request, obj=None):
        fieldsets = (
            (
                None,
                {
                    "fields": (
                        "is_active",
                        "form_type",
                        "name_en",
                        "name_ar",
                        "slug",
                        "description_en",
                        "description_ar",
                        "resubmission_intro_en",
                        "resubmission_intro_ar",
                        "success_message_en",
                        "success_message_ar",
                    )
                },
            ),
            (
                _("Settings"),
                {
                    "fields": (
                        ("login_required", "use_verification", "enable_edit_submit"),
                        ("filter_choice", "display_choice", "in_user_profile"),
                        ("is_available_script", "not_available_template"),
                        ("entry_condition", "days_next_submit"),
                        "extra_validator",
                        "custom_action_at_creation",
                        "custom_action",
                        "page_template",
                        "success_page_template",
                        "form_template",
                        "print_template",
                        "resubmit_page_template",
                    )
                },
            ),
            (
                _("Formset"),
                {
                    "fields": (
                        "is_formset",
                        "formset_can_delete",
                        "formset_can_order",
                        "formset_max_num",
                        "formset_min_num",
                    )
                },
            ),
            (_("Workflow"), {"fields": ("workflow",)}),
            (
                _("Email settings"),
                {
                    "fields": (
                        "send_email",
                        "send_email_to_submitter",
                        "send_files_email",
                        "email_from_is_email_field",
                        "notify_users",
                        "notify_groups",
                        "notify_emails",
                        ("form_builder_email_template", "email_field"),
                        "verification_code_email_template",
                        "submit_cancel_email_template",
                    )
                },
            ),
            (_("permission"), {"fields": ("permission",)}),
            (_("Tree settings"), {"fields": ("_position", "_ref_node_id")}),
            ENTRY_TIME_FIELDS,
        )

        return fieldsets

    def save_model(self, request, obj, form, change):
        super(FormAdmin, self).save_model(request, obj, form, change)
        obj.permission.add(request.user)

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == "workflow":
            kwargs["queryset"] = Workflow.objects.filter(depth=1)

        if db_field.name == "email_field":
            form_id = None
            for i in request.META["PATH_INFO"].rstrip("/").split("/"):
                try:
                    if not form_id:
                        form_id = int(i)
                except:
                    form_id = None
            if form_id:
                kwargs["queryset"] = FormField.objects.filter(form__id=form_id)
            else:
                kwargs["queryset"] = FormField.objects.all()
        return super(FormAdmin, self).formfield_for_foreignkey(
            db_field, request, **kwargs
        )


class FieldEntryInline(admin.TabularInline):
    """
    Inlines for form entries
    """

    model = FieldEntry
    fields = ("form_field", "value", "file")
    extra = 0

    # def has_add_permission(self, request, obj=None):
    #     return False


class FieldEntryAdmin(admin.ModelAdmin):
    list_display = ("__str__", "submitter", "token", "form")

    def has_add_permission(self, request, obj=None):
        return False


class FormEntryAdmin(admin.ModelAdmin):
    """
    Admin for form entries
    """

    list_display = ("__str__", "submitter", "form", "has_submission")
    readonly_fields = (
        "created",
        "modified",
        "submitter",
        "token",
        "form",
        "parent",
        "form_submit",
    )
    inlines = [FieldEntryInline]
    list_filter = ["form"]
    search_fields = ["submitter__username"]
    fieldsets = (
        (None, {"fields": (("form", "parent", "form_submit"),)}),
        (_("Submitter"), {"fields": (("submitter", "token"),)}),
        ENTRY_TIME_FIELDS,
    )

    def has_submission(self, obj):
        return obj.has_submission

    has_submission.boolean = True

    def has_add_permission(self, request, obj=None):
        return False


class ConditionInline(admin.TabularInline):
    model = Condition


class ConditionalRedirectAdmin(admin.ModelAdmin):
    inlines = [ConditionInline]
    list_display = ["__str__", "form", "force_redirect", "redirect_to"]
    # list_filter = ['form', 'force_redirect', 'redirect_to']
    search_fields = ["form__name", "redirect_to__name"]

    def get_list_filter(self, request):
        return ["form", "force_redirect", "redirect_to"]


def get_display_column_fun(field):
    def display_field(o, field=field):
        entries = o.entries.filter(
            form__field_entries__form_field=field
        )  # form entries that has this Field
        field_entry = (
            entries.first().field_entries.filter(form_field=field)
            if entries.exists()
            else None
        )
        return (
            entries.first()
            .field_entries.filter(form_field=field)
            .first()
            .get_html_value()
            if field_entry and field_entry.exists()
            else None
        )

    return display_field


def get_filter_class(field, form):
    class FilteredField(admin.SimpleListFilter):
        title = smart_truncate(field.label, 40)
        parameter_name = field.slug

        def lookups(self, request, model_admin):
            options = set(
                [
                    (c.value.split(";")[0], c.get_value().split("<br>")[0])
                    for c in FieldEntry.objects.filter(form=form, form_field=field)
                ]
            )
            return options

        def queryset(self, request, queryset):
            if self.value():
                return queryset.filter(
                    entries__field_entries__value__contains=self.value()
                )
            else:
                return queryset

    return FilteredField


CHOICE_FIELDS = [
    form_settings.ChoiceField,
    form_settings.MultipleChoiceField,
    form_settings.ChoiceWithOtherField,
]


class FormSubmitAdmin(admin.ModelAdmin):
    list_display = [
        "__str__",
        "submitter",
    ]
    list_filter = ["created", "form"]
    actions = [
        "after_submit",
        # 'approve_cancel_request', 'reject_cancel_request'
    ]
    date_hierarchy = "modified"

    def has_add_permission(self, request, obj=None):
        return False

    def _update_list_display_of_subform(self, form, list_display):
        for a_form in form.get_children():
            for form_field in FormField.objects.select_related("field").filter(
                form=a_form
            ):
                if (
                    a_form.display_choice and form_field.field_class in CHOICE_FIELDS
                ) or form_field.is_display:
                    setattr(
                        self, form_field.slug, get_display_column_fun(field=form_field)
                    )
                    dis_fun = getattr(self, form_field.slug)
                    dis_fun.short_description = form_field.label
                    list_display.append(form_field.slug)

            if a_form.get_children().count() > 0:
                list_display = self._update_list_display_of_subform(
                    form=a_form, list_display=list_display
                )

        return list_display

    def _update_list_filter_of_subform(self, form, list_filter):
        for a_form in form.get_children():
            for form_field in FormField.objects.filter(form=a_form):
                if (
                    a_form.filter_choice and form_field.field_class in CHOICE_FIELDS
                ) or form_field.is_filter:
                    list_filter.append(
                        get_filter_class(field=form_field, form=form_field.form)
                    )

            if len(a_form.get_children()) > 0:
                list_filter = self._update_list_filter_of_subform(
                    form=a_form, list_filter=list_filter
                )

        return list_filter

    def get_list_display(self, request):
        list_display_parent = list(
            super(FormSubmitAdmin, self).get_list_display(request)
        )
        list_display = []

        selected_form_id = request.GET.get("form__id__exact", None)
        form_list = []
        one_form = True
        for obj in self.get_queryset(request):
            if obj.form and obj.form.id not in form_list:
                form_list.append(obj.form.id)
            if len(form_list) > 1:
                one_form = False
                break
        if one_form and not selected_form_id and len(form_list) > 0:
            selected_form_id = form_list[0]
        if selected_form_id:
            form = Form.objects.filter(id=int(selected_form_id)).first()
            if form.workflow:
                list_display_parent.append("workflow_status")
            use_verification = form.use_verification
            if not use_verification:
                for child in form.get_tree(parent=form):
                    if child.use_verification:
                        use_verification = True
                        break
            if use_verification:
                list_display_parent.append("file_number")
                list_display_parent.append("verification_code")
                list_display_parent.append("company_name")
                list_display_parent.append("verified_email")

            for form_field in FormField.objects.filter(form__id=int(selected_form_id)):
                if (
                    form.display_choice and form_field.field_class in CHOICE_FIELDS
                ) or form_field.is_display:
                    setattr(
                        self, form_field.slug, get_display_column_fun(field=form_field)
                    )
                    dis_fun = getattr(self, form_field.slug)
                    dis_fun.short_description = smart_truncate(form_field.label, 40)
                    list_display.append(form_field.slug)

            list_display = self._update_list_display_of_subform(
                form=form, list_display=list_display
            )

        return (
            list_display_parent
            + ["view_submission", "export_to", "download_attachments"]
            + list_display
        )

    def workflow_status(self, obj):
        return obj.get_workflow_state()

    def file_number(self, obj):
        try:
            return (
                obj.entries.first()
                .field_entries.filter(field_name="file_number")
                .first()
                .get_value()
            )
        except:
            pass

    def verification_code(self, obj):
        try:
            return (
                obj.entries.first()
                .field_entries.filter(field_name="verified_code")
                .first()
                .get_value()
            )
        except:
            pass

    def company_name(self, obj):
        try:
            return (
                obj.entries.first()
                .field_entries.filter(field_name="verified_company")
                .first()
                .get_value()
            )
        except:
            pass

    def verified_email(self, obj):
        try:
            return (
                obj.entries.first()
                .field_entries.filter(field_name="verified_email")
                .first()
                .get_value()
            )
        except:
            pass

    def get_list_filter(self, request):
        list_filter = list(super(FormSubmitAdmin, self).get_list_filter(request))
        selected_form_id = request.GET.get("form__id__exact", None)
        if selected_form_id:
            form = Form.objects.filter(id=int(selected_form_id)).first()
            for form_field in FormField.objects.select_related("field").filter(
                form__id=int(selected_form_id)
            ):
                if (
                    form.filter_choice and form_field.field_class in CHOICE_FIELDS
                ) or form_field.is_filter:
                    list_filter.append(
                        get_filter_class(field=form_field, form=form_field.form)
                    )

            list_filter = self._update_list_filter_of_subform(
                form=form, list_filter=list_filter
            )
        return list_filter

    def get_changelist(self, request, **kwargs):
        """
        Returns the ChangeList class for use on the changelist page.
        """

        return MyChangeList

    def get_queryset(self, request):
        queryset = super(FormSubmitAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return queryset.filter(entries__isnull=False).distinct()
        else:
            queryset = queryset.filter(entries__form__permission=request.user)
            return queryset.filter(entries__isnull=False).distinct()

    def export_to(self, obj):
        if obj.form:
            # return '<a href="%s">%s</a>||<a href="%s">%s</a>' % (obj.get_export_to_pdf_url(), _("PDF"), obj.get_export_to_excel_url(), _('EXCEL'))
            return format_html(
                '<a href="%s">%s</a>' % (obj.get_export_to_excel_url(), _("EXCEL"))
            )

    export_to.allow_tags = True

    def download_attachments(self, obj):
        return format_html(
            '<a href="%s">%s</a>' % (obj.get_download_attachments_url(), _("Download"))
        )

    download_attachments.allow_tags = True

    def after_submit(modeladmin, request, queryset):
        for submit in queryset.all():
            submit.after_submit()

    after_submit.short_description = "Run after submit actions"

    def status(self, obj):
        output = ""
        submitter = obj.submitter
        try:
            if submitter:
                for submission in (
                    self.model.objects.filter(
                        entries__submitter=submitter,
                        entries__form=obj.entries.first().form,
                    )
                    .values_list("id", flat=True)
                    .distinct()
                ):

                    o = self.model.objects.select_related("form").get(id=submission)
                    try:
                        output += """<p style="margin:0px;">{}<br></p>""".format(
                            o.get_cancel_status_display()
                            if o.cancel_status
                            else o.get_workflow_state()
                        )
                    except:
                        continue

            elif obj.form:
                output += """<p style="margin:0px;">{}<br></p>""".format(
                    obj.get_cancel_status_display()
                    if obj.cancel_status
                    else obj.get_workflow_state()
                )
        except:
            pass

        return format_html(output)

    def view_submission(self, obj):
        output = ""
        submitter = obj.submitter_user

        o = self.model.objects.select_related("form").get(id=obj.id)
        if o.form and o.entries.all().exists():
            output += """<a href="{}" target="_blank">{}</a><br>""".format(
                reverse(
                    "form_builder:submission_form_detail_admin",
                    args=(o.form.slug, o.id),
                ),
                _("View This Submission on").__str__()
                + " {}".format(o.modified.strftime("%B %d, %Y")),
            )

            try:
                if submitter:
                    for submission in (
                        self.model.objects.filter(
                            Q(
                                entries__submitter=submitter,
                                entries__form=obj.entries.first().form,
                            )
                            & ~Q(id=obj.id)
                        )
                        .values_list("id", flat=True)
                        .distinct()
                    ):
                        o = self.model.objects.select_related("form").get(id=submission)
                        try:
                            output += """<a href="{}" target="_blank">{}</a><br>""".format(
                                reverse(
                                    "form_builder:submission_form_detail_admin",
                                    args=(o.form.slug, o.id),
                                ),
                                _("View old Submission on").__str__()
                                + " {}".format(o.modified.strftime("%B %d, %Y")),
                            )
                        except:
                            continue

                elif obj.form:
                    output += """<a href="{}" target="_blank">{}</a>""".format(
                        reverse(
                            "form_builder:submission_form_detail_admin",
                            args=(obj.form.slug, obj.id),
                        ),
                        _("View This Submission on").__str__()
                        + " {}".format(obj.modified.strftime("%B %d, %Y")),
                    )
            except:
                pass

        return format_html(output)


class FormAdminExtend(FormAdmin):
    def get_queryset(self, req):
        queryset = super(FormAdminExtend, self).get_queryset(req)
        if req.user.is_superuser:
            return queryset
        forms = queryset.filter(permission=req.user)
        q = Q()
        forms_array = []
        for form in forms:
            forms_array += [form]
            forms_array += form.get_children()
        for a in forms_array:
            q |= Q(id=a.id)
        if q:
            return queryset.filter(q)
        return queryset.none()


class FormEntryAdminExtend(FormEntryAdmin):
    def get_queryset(self, req):
        queryset = super(FormEntryAdminExtend, self).get_queryset(req)
        if req.user.is_superuser:
            return queryset
        return queryset.filter(form__permission=req.user)


# class FormSubmitAdminExtend(FormSubmitAdmin):
#     def get_queryset(self, req):
#         queryset = super(FormSubmitAdminExtend, self).get_queryset(req)
#         if req.user.is_superuser:
#             return queryset
#         return queryset.filter(entries__submitter=req.user)


# if 'jet' in settings.INSTALLED_APPS or 'kn_jet' in settings.INSTALLED_APPS:
#     try:
#         from kn_jet.jet_dashboard import modules
#         from kn_jet.jet_dashboard.dashboard import Dashboard
#     except ImportError:
#         from jet.jet_dashboard import modules
#         from jet.jet_dashboard.dashboard import Dashboard
#
#
#     class RestFormDashboard(Dashboard):
#         columns = 3
#
#         def init_with_context(self, context):
#             forms = Form.objects.all()
#             submitted_forms = FormSubmit.objects.all()
#             childen_forms = []
#             children_submit_forms = []
#             for form in forms:
#                 childen_forms.append(
#                     {
#                         'title': _(form.name),
#                         'url': reverse('admin:form_builder_form_change', args=(str(form.id),)),
#                     },
#                 )
#             for submit_form in submitted_forms:
#                 children_submit_forms.append(
#                     {
#                         'title': _("[{0}] - {1}".format(submit_form.form, submit_form.submitter)),
#                         'url': reverse('admin:form_builder_formsubmit_change', args=(str(submit_form.id),)),
#                     },
#                 )
#             self.available_children.append(modules.LinkList)
#             self.children.append(modules.LinkList(
#                 _('ALL Forms'),
#                 children=childen_forms,
#                 column=0,
#                 order=0
#             ))
#             self.children.append(modules.LinkList(
#                 _('Add New Form and Field'),
#                 children=[
#                     {
#                         'title': _('Add New Form'),
#                         'url': reverse('admin:form_builder_form_add'),
#                     },
#                     {
#                         'title': _('Add New Field'),
#                         'url': reverse('admin:form_builder_field_add'),
#                     },
#                 ],
#                 column=1,
#                 order=0
#             ))
#             self.children.append(modules.LinkList(
#                 _('All Submitted Forms'),
#                 children=children_submit_forms,
#                 column=2,
#                 order=0,
#             ))


class IsUsedFilter(admin.SimpleListFilter):
    title = _("Submission")
    parameter_name = "submission"

    def lookups(self, request, model_admin):
        return [(0, _("Not submitted")), (1, _("Submitted"))]

    def queryset(self, request, queryset):
        if self.value() == "0":
            ids = [obj.id for obj in queryset if not obj.is_used()]
            return queryset.filter(id__in=ids)
        if self.value() == "1":
            ids = [obj.id for obj in queryset if obj.is_used()]
            return queryset.filter(id__in=ids)


class FormVerificationCodeAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = [
        "form",
        "file_number",
        "verification_code",
        "company_name",
        "email",
        # 'max_usage', 'actual_usage',
        "view_submission",
        "export_submission",
        "email_sent",
        "submitted",
        "is_active",
    ]
    list_filter = [
        ("form", admin.RelatedOnlyFieldListFilter),
        "is_active",
        "email_sent",
        IsUsedFilter,
    ]
    search_fields = ["file_number", "verification_code", "company_name", "email"]
    list_editable = [
        "is_active",
    ]
    resource_class = FormVerificationCodeResource
    change_list_template = "admin/form_builder/import_export/change_list.html"
    readonly_fields = ("verification_code",)
    exclude = ("email_template",)
    actions = [
        "send_verification_code_email",
    ]
    import_template_name = "admin/form_builder/import_export/import.html"

    def submitted(self, obj):
        return obj.is_used()

    submitted.short_description = _("Submitted")
    submitted.boolean = True

    def send_verification_code_email(self, request, queryset):
        for obj in queryset:
            # for verification in FormVerificationCode.objects.filter(form=obj):
            domain = Site.objects.get_current().domain
            if hasattr(settings, "LANGUAGE_CODE"):
                submit_link = "".join(
                    [
                        "http://",
                        domain,
                        "/{}".format(settings.LANGUAGE_CODE),
                        obj.form.get_absolute_url_no_i18n(),
                    ]
                )
            else:
                submit_link = "".join(
                    ["http://", domain, obj.form.get_absolute_url_no_i18n()]
                )
            context = {
                "form": obj.form,
                "verification_code": obj.verification_code,
                "company_name": obj.company_name,
                "email": obj.email,
                "submit_link": submit_link,
            }
            if obj.form.verification_code_email_template:
                email_template = obj.form.verification_code_email_template
                try:
                    email_template.send_mail(ctx=context, send_to=[obj.email])
                    obj.email_sent = True
                    obj.save()
                except:
                    pass

    send_verification_code_email.short_description = _(
        "Send Verification Codes by Email"
    )

    def get_changelist(self, request, **kwargs):
        """
        Returns the ChangeList class for use on the changelist page.
        """
        from .utils import MyChangeList  # show filter even if it has only one choice

        return MyChangeList

    def changelist_view(self, request, extra_context=None):
        queryset = self.get_queryset(request)
        selected_form_id = request.GET.get("form__id__exact", None)
        if (
            not selected_form_id
            and len(queryset.values_list("form", flat=True).distinct()) == 1
        ):
            selected_form_id = queryset.values_list("form", flat=True).distinct()[0]
        selected_form = (
            queryset.filter(form__id=selected_form_id).first()
            if selected_form_id
            else None
        )
        export_all_url = (
            reverse(
                "form_builder:dashboard:export_all", args=(selected_form.form.slug,)
            )
            if selected_form
            else None
        )
        extra_context = {
            "selected_form": selected_form,
            "export_all_url": export_all_url,
        }
        return super(FormVerificationCodeAdmin, self).changelist_view(
            request, extra_context=extra_context
        )

    def get_actions(self, request):
        actions = super(FormVerificationCodeAdmin, self).get_actions(request)
        if "delete_selected" in actions and not request.user.is_superuser:
            del actions["delete_selected"]
        return actions

    def get_queryset(self, request):
        queryset = super(FormVerificationCodeAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return queryset
        else:
            return queryset.filter(form__permission=request.user)

    def view_submission(self, obj):
        value = ""
        i = 0
        for entry in FieldEntry.objects.filter(
            value=obj.verification_code, form=obj.form
        ):
            i += 1
            if entry.entry and entry.entry.form_submit:
                value += """<a href="{}" target="_blank">{} - {}</a><br>""".format(
                    reverse(
                        "form_builder:dashboard:submission_form_detail_admin",
                        args=(entry.form.slug, entry.entry.form_submit.id),
                    ),
                    _("Submission"),
                    i,
                )

        return format_html(value)

    def export_submission(self, obj):
        # if obj.is_root():
        # try:
        id_list = []
        for entry in FieldEntry.objects.filter(
            value=obj.verification_code, form=obj.form
        ):
            if entry.entry and entry.entry.form_submit:
                id_list.append(str(entry.entry.form_submit.id))

        ids = ",".join(id_list)

        url = format_html(
            """
        <a href="{}" target="_blank" class="button">{}</a>
        """.format(
                reverse("form_builder:export_all", args=(obj.form.slug,))
                + "?ids={}".format(ids),
                _("Export Submissions"),
            )
        )
        return url
        # except:
        #     pass

    def process_import(self, request, *args, **kwargs):
        """
        Perform the actual import action (after the user has confirmed he
        wishes to import)
        """
        from import_export.results import RowResult
        from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION
        from django.contrib.contenttypes.models import ContentType
        from django.template.defaultfilters import pluralize
        from django.contrib import messages
        from django.http import HttpResponseRedirect

        opts = self.model._meta
        resource = self.get_import_resource_class()()

        confirm_form = ConfirmImportForm(request.POST)
        if confirm_form.is_valid():
            import_formats = self.get_import_formats()
            input_format = import_formats[
                int(confirm_form.cleaned_data["input_format"])
            ]()
            tmp_storage = self.get_tmp_storage_class()(
                name=confirm_form.cleaned_data["import_file_name"]
            )
            data = tmp_storage.read(input_format.get_read_mode())
            if not input_format.is_binary() and self.from_encoding:
                data = force_text(data, self.from_encoding)
            dataset = input_format.create_dataset(data)

            if request and request.GET.get("form_id", None):
                try:
                    form_id = request.GET.get("form_id", None)
                    form_id_list = [form_id for row_number in range(len(dataset.dict))]
                    dataset.append_col(form_id_list, header="form_id")
                except:
                    pass

            result = resource.import_data(
                dataset,
                dry_run=False,
                raise_errors=True,
                file_name=confirm_form.cleaned_data["original_file_name"],
                user=request.user,
            )

            if not self.get_skip_admin_log():
                # Add imported objects to LogEntry
                logentry_map = {
                    RowResult.IMPORT_TYPE_NEW: ADDITION,
                    RowResult.IMPORT_TYPE_UPDATE: CHANGE,
                    RowResult.IMPORT_TYPE_DELETE: DELETION,
                }
                content_type_id = ContentType.objects.get_for_model(self.model).pk
                for row in result:
                    if row.import_type != row.IMPORT_TYPE_SKIP:
                        LogEntry.objects.log_action(
                            user_id=request.user.pk,
                            content_type_id=content_type_id,
                            object_id=row.object_id,
                            object_repr=row.object_repr,
                            action_flag=logentry_map[row.import_type],
                            change_message="%s through import_export" % row.import_type,
                        )

            success_message = (
                "Import finished, with {} new {}{} and "
                "{} updated {}{}.".format(
                    result.totals[RowResult.IMPORT_TYPE_NEW],
                    opts.model_name,
                    pluralize(result.totals[RowResult.IMPORT_TYPE_NEW]),
                    result.totals[RowResult.IMPORT_TYPE_UPDATE],
                    opts.model_name,
                    pluralize(result.totals[RowResult.IMPORT_TYPE_UPDATE]),
                )
            )

            messages.success(request, success_message)
            tmp_storage.remove()

            # url = reverse('admin:%s_%s_changelist' % self.get_model_info(),
            #               current_app=self.admin_site.name)
            form_id = request.GET.get("form_id", None)
            if form_id:
                url = (
                    reverse("admin:form_builder_form_change", args=(int(form_id),))
                    + "#/tab/inline_0/"
                )
            else:
                url = reverse("admin:form_builder_form_changelist")
            return HttpResponseRedirect(url)

    def import_action(self, request, *args, **kwargs):
        """
        Perform a dry_run of the import to make sure the import will not
        result in errors.  If there where no error, save the user
        uploaded file to a local temp file that will be used by
        'process_import' for the actual import.
        """
        resource = self.get_import_resource_class()(request=request)

        context = {}

        import_formats = self.get_import_formats()
        form = ImportForm(import_formats, request.POST or None, request.FILES or None)

        if request.POST and form.is_valid():
            input_format = import_formats[int(form.cleaned_data["input_format"])]()
            import_file = form.cleaned_data["import_file"]
            # first always write the uploaded file to disk as it may be a
            # memory file or else based on settings upload handlers
            tmp_storage = self.get_tmp_storage_class()()
            data = bytes()
            for chunk in import_file.chunks():
                data += chunk

            tmp_storage.save(data, input_format.get_read_mode())

            # then read the file, using the proper format-specific mode
            # warning, big files may exceed memory
            try:
                data = tmp_storage.read(input_format.get_read_mode())
                if not input_format.is_binary() and self.from_encoding:
                    data = force_text(data, self.from_encoding)
                dataset = input_format.create_dataset(data)
            except UnicodeDecodeError as e:
                return HttpResponse(_("<h1>Imported file has a wrong encoding</h1>"))
            except Exception as e:
                return HttpResponse(
                    _("<h1>%s encountered while trying to read file</h1>")
                )
            result = resource.import_data(
                dataset,
                dry_run=True,
                raise_errors=False,
                file_name=import_file.name,
                user=request.user,
            )

            context["result"] = result

            if not result.has_errors():
                context["confirm_form"] = ConfirmImportForm(
                    initial={
                        "import_file_name": tmp_storage.name,
                        "original_file_name": import_file.name,
                        "input_format": form.cleaned_data["input_format"],
                    }
                )

        if django.VERSION >= (1, 8, 0):
            context.update(self.admin_site.each_context(request))
        elif django.VERSION >= (1, 7, 0):
            context.update(self.admin_site.each_context())

        context["form"] = form
        context["opts"] = self.model._meta
        context["fields"] = [f.column_name for f in resource.get_fields()]
        context["form_id"] = request.GET.get("form_id", None)

        request.current_app = self.admin_site.name
        return TemplateResponse(request, [self.import_template_name], context)

    def export_action(self, request, *args, **kwargs):
        from import_export.forms import ExportForm

        formats = self.get_export_formats()
        form = ExportForm(formats, request.POST or None)
        if form.is_valid():
            file_format = formats[int(form.cleaned_data["file_format"])]()
            form_id = request.GET.get("form_id", None)
            if form_id:
                queryset = FormVerificationCode.objects.filter(form__id=int(form_id))
                form_model = Form.objects.filter(id=int(form_id))
            else:
                queryset = self.get_export_queryset(request)
            export_data = self.get_export_data(file_format, queryset)
            content_type = file_format.get_content_type()
            # Django 1.7 uses the content_type kwarg instead of mimetype
            try:
                response = HttpResponse(export_data, content_type=content_type)
            except TypeError:
                response = HttpResponse(export_data, mimetype=content_type)
            # response['Content-Disposition'] = 'attachment; filename=%s' % (
            #     self.get_export_filename(file_format),
            # )

            if form_id and form_model.exists():
                response["Content-Disposition"] = "attachment; filename=%s.%s" % (
                    form_model.first().name,
                    file_format.get_extension(),
                )
            else:
                response["Content-Disposition"] = "attachment; filename=%s" % (
                    self.get_export_filename(file_format),
                )
            return response

        context = {}

        if django.VERSION >= (1, 8, 0):
            context.update(self.admin_site.each_context(request))
        elif django.VERSION >= (1, 7, 0):
            context.update(self.admin_site.each_context())

        context["form"] = form
        context["opts"] = self.model._meta
        request.current_app = self.admin_site.name
        return TemplateResponse(request, [self.export_template_name], context)

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == "form" and not request.user.is_superuser:
            kwargs["queryset"] = Form.objects.filter(permission=request.user)
        return super(FormVerificationCodeAdmin, self).formfield_for_foreignkey(
            db_field, request, **kwargs
        )


class EmailTemplateAdmin(admin.ModelAdmin):
    list_display = ["name", "subject", "email_from"]
    fields = [
        "name",
        # 'description',
        "email_from",
        "subject",
        "notify_users",
        "notify_emails",
        # 'content',
        "html_content",
    ]

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == "notify_users":
            kwargs["queryset"] = User.objects.filter(is_staff=True)
        return super(EmailTemplateAdmin, self).formfield_for_manytomany(
            db_field, request, **kwargs
        )


class FormFilter(admin.SimpleListFilter):
    title = _("Form")
    parameter_name = "form"

    def lookups(self, request, model_admin):
        forms = set([c for c in Form.objects.all()])
        return [(c.id, c.name) for c in forms]

    def queryset(self, request, queryset):
        if self.value():
            root_workflow = Form.objects.get(id=self.value()).workflow
            if root_workflow:
                children_id = root_workflow.get_descendants().values_list(
                    "id", flat=True
                )
                return queryset.filter(Q(id__in=children_id) | Q(id=root_workflow.id))
            return Workflow.objects.none()
        else:
            return queryset


class WorkflowAdmin(TreeAdmin, TabbedDjangoJqueryTranslationAdmin):
    form = movenodeform_factory(Workflow,)
    list_filter = (FormFilter,)
    list_display = ["name", "next_step", "state", "get_exit_email_template"]
    list_editable = ["next_step"]

    # class Media:
    #     js = ('js/admin/collapse_all.js',)

    def get_prepopulated_fields(self, request, obj=None):
        if form_settings.EDITABLE_SLUG:
            return {"slug": ("name_en",)}

    def get_exit_email_template(self, obj):
        the_return = ""
        for template in obj.exit_email_template.all():
            href = reverse(
                "admin:form_builder_emailtemplate_change", args=(template.id,)
            )
            the_return += f"<a href='{href}' target='_blank'>{template.name}</a><br>"
        return format_html(the_return)

    def get_fieldsets(self, request, obj=None):
        fieldsets = (
            (
                None,
                {
                    "fields": (
                        "name_en",
                        "name_ar",
                        "state_en",
                        "state_ar",
                        "slug",
                        "exit_email_template",
                        "email_template_changed_by_user",
                        "next_step",
                        "custom_script",
                        "is_active",
                        "is_sent_resubmit",
                        "is_state_after_resubmit",
                        "action_form",
                    )
                },
            ),
            (
                _("permission"),
                {
                    "fields": (
                        "apply_permission",
                        "permitted_users",
                        "permitted_groups",
                    )
                },
            ),
            (_("Tree settings"), {"fields": ("_position", "_ref_node_id")}),
        )

        return fieldsets

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == "permitted_users":
            kwargs["queryset"] = User.objects.filter(is_staff=True)

        return super(WorkflowAdmin, self).formfield_for_manytomany(
            db_field, request, **kwargs
        )


if not form_settings.FORK:
    admin.site.register(EmailTemplate, EmailTemplateAdmin)
    admin.site.register(Field, FieldAdmin)
    admin.site.register(Form, FormAdminExtend)
    admin.site.register(FormEntry, FormEntryAdminExtend)
    admin.site.register(FieldEntry, FieldEntryAdmin)
    admin.site.register(FormSubmit, FormSubmitAdmin)
    admin.site.register(ConditionalRedirect, ConditionalRedirectAdmin)
    admin.site.register(FormVerificationCode, FormVerificationCodeAdmin)

    admin.site.register(Workflow, WorkflowAdmin)
    admin.site.register(FormType, FormTypeAdmin)
