# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.utils.translation import ugettext_lazy as _


APP_LABEL = getattr(settings, "FORM_BUILDER_APP_LABEL", "form_builder")

serializer_base = "rest_framework.serializers."
form_base = "django.forms.fields."

CharField = getattr(settings, "FORM_BUILDER_CharField", "CharField")
DynamicChoice = getattr(settings, "FORM_BUILDER_DynamicChoice", "DynamicChoice")
TextAreaField = getattr(settings, "FORM_BUILDER_TextAreaField", "TextAreaField")
RichTextField = getattr(settings, "FORM_BUILDER_RichTextField", "RichTextField")
EmailField = getattr(settings, "FORM_BUILDER_EmailField", "EmailField")
URLField = getattr(settings, "FORM_BUILDER_URLField", "URLField")
IntegerField = getattr(settings, "FORM_BUILDER_IntegerField", "IntegerField")
PhoneNumberField = getattr(
    settings, "FORM_BUILDER_PhoneNumberField", "PhoneNumberField"
)
KwPhoneNumberField = getattr(
    settings, "FORM_BUILDER_KwPhoneNumberField", "KwPhoneNumberField"
)
DecimalField = getattr(settings, "FORM_BUILDER_DecimalField", "DecimalField")
BooleanField = getattr(settings, "FORM_BUILDER_BooleanField", "BooleanField")
DateField = getattr(settings, "FORM_BUILDER_DateField", "DateField")
DateTimeField = getattr(settings, "FORM_BUILDER_DateTimeField", "DateTimeField")
TimeField = getattr(settings, "FORM_BUILDER_TimeField", "TimeField")
ChoiceField = getattr(settings, "FORM_BUILDER_ChoiceField", "ChoiceField")
RadioChoiceField = getattr(
    settings, "FORM_BUILDER_RadioChoiceField", "RadioChoiceField"
)
MultipleChoiceField = getattr(
    settings, "FORM_BUILDER_MultipleChoiceField", "MultipleChoiceField"
)
RegexField = getattr(settings, "FORM_BUILDER_RegexField", "RegexField")
FileField = getattr(settings, "FORM_BUILDER_FileField", "FileField")
ImageField = getattr(settings, "FORM_BUILDER_ImageField", "ImageField")
ChoiceWithOtherField = getattr(
    settings, "FORM_BUILDER_ChoiceWithOtherField", "ChoiceWithOtherField"
)
KWCivilIDNumberField = getattr(
    settings, "FORM_BUILDER_KWCivilIDNumberField", "KWCivilIDNumberField"
)
CaptchaField = getattr(
    settings, "FORM_BUILDER_KWCivilIDNumberField", "NoReCaptchaField"
)
SmileFaceRatingField = getattr(
    settings, "FORM_BUILDER_SmileFaceRatingField", "SmileFaceRatingField"
)
StarsRatingField = getattr(
    settings, "FORM_BUILDER_StarsRatingField", "StarsRatingField"
)
MultipleChoiceWithInputField = getattr(
    settings,
    "FORM_BUILDER_MultipleChoiceWithInputField",
    "MultipleChoiceWithInputField",
)
# LegendField = getattr(settings, 'FORM_BUILDER_LegendField', 'LegendField')

FILE_CLASSES = (FileField, ImageField)


FIELD_CLASSES = (
    (CharField, _("Text")),
    (DynamicChoice, _("Dynamic Choice")),
    (TextAreaField, _("Text Area")),
    (RichTextField, _("Rich Text Area")),
    (EmailField, _("Email")),
    (URLField, _("URL")),
    (IntegerField, _("Number")),
    (PhoneNumberField, _("Phone Number")),
    (KwPhoneNumberField, _("KW Phone Number")),
    (DecimalField, _("Decimal")),
    # (BooleanField, _('Yes/No')),
    (BooleanField, _("Checkbox")),
    (DateField, _("Date")),
    (DateTimeField, _("Date & time")),
    (TimeField, _("Time")),
    (ChoiceField, _("Choice")),
    (RadioChoiceField, _("Radio Choice")),
    (MultipleChoiceField, _("Multiple choices")),
    (RegexField, _("Regex")),
    (FileField, _("File")),
    (ImageField, _("Image")),
    (ChoiceWithOtherField, _("Choice field with optional answer")),
    (KWCivilIDNumberField, _("Kuwait Civil ID")),
    (CaptchaField, _("Captcha")),
    (SmileFaceRatingField, _("Smile Face Rating")),
    (StarsRatingField, _("Stars Rating")),
    (MultipleChoiceWithInputField, _("Multiple Choice field with input answer")),
    # (LegendField, _('Legend Field')),
)

FIELD_BASES = {
    "django.forms.fields.": [
        CharField,
        EmailField,
        URLField,
        IntegerField,
        DecimalField,
        BooleanField,
        DateField,
        DateTimeField,
        TimeField,
        ChoiceField,
        MultipleChoiceField,
        RegexField,
        FileField,
        ImageField,
    ],
    "form_builder.forms.forms.": [
        ChoiceWithOtherField,
        SmileFaceRatingField,
        StarsRatingField,
        KwPhoneNumberField,
        MultipleChoiceWithInputField,
        RadioChoiceField,
        TextAreaField,
        RichTextField,
        DynamicChoice,
    ],
    "localflavor.kw.forms.": [KWCivilIDNumberField,],
    "phonenumber_field.formfields.": [PhoneNumberField,],
    "nocaptcha_recaptcha.fields.": [CaptchaField],
}

# The maximum allowed length for field values.
FIELD_MAX_LENGTH = getattr(settings, "FORM_BUILDER_FIELD_MAX_LENGTH", 2000)

# The maximum allowed length for field labels.
LABEL_MAX_LENGTH = getattr(settings, "FORM_BUILDER_LABEL_MAX_LENGTH", 2000)

# Upload function to handle uploaded files path
UPLOAD_FUNCTION = getattr(
    settings, "FORM_BUILDER_UPLOAD_FUNCTION", "form_builder.utils.unique_upload"
)

# Boolean controlling whether form slug is editable in the admin.
EDITABLE_SLUG = getattr(settings, "FORM_BUILDER_EDITABLE_SLUG", True)

# Char to use as a field delimiter when exporting form responses as CSV.
CSV_DELIMITER = getattr(settings, "FORM_BUILDER_CSV_DELIMITER", ",")

# The maximum allowed length for field help text
HELP_TEXT_MAX_LENGTH = getattr(settings, "FORM_BUILDER_HELPTEXT_MAX_LENGTH", 100)

# The maximum allowed length for field choices
CHOICES_MAX_LENGTH = getattr(settings, "FORM_BUILDER_CHOICES_MAX_LENGTH", 1000)

# Action URL, which is processing the form submission
ACTION_VIEW = getattr(settings, "FORM_BUILDER_ACTION_VIEW", "form_builder:api:submit")

# File upload URL used to upload files async during form completion
FILE_UPLOAD_VIEW = getattr(
    settings, "FORM_BUILDER_UPLOAD_URL", "form_builder:api:file_upload"
)

# Form options for form model instance
FORM_OPTIONS = getattr(settings, "FORM_BUILDER_FORM_OPTIONS", {})

FORM_BUILDER_FORM_TEMPLATES = (
    ("rest_framework/vertical", _("Bootstrap Vertical")),
    ("rest_framework/horizontal", _("Bootstrap Horizontal")),
    ("rest_framework/inline", _("Bootstrap Inline")),
)
if hasattr(settings, "FORM_BUILDER_FORM_TEMPLATES"):
    FORM_BUILDER_FORM_TEMPLATES += getattr(settings, "FORM_BUILDER_FORM_TEMPLATES")

FORM_BUILDER_PAGE_TEMPLATES = [
    ("form_builder/single_form/single_form_page.html", _("Default")),
    ("form_builder/multiple_form/multiple_form_page.html", _("Multiple Form")),
]

FORM_BUILDER_RESUBMIT_PAGE_TEMPLATES = [
    ("form_builder/resubmit_form/single_form_resubmit.html", _("Single Form Page")),
]

FORM_BUILDER_FORM_TEMPLATES = [
    ("form_builder/form.html", _("Default")),
]

FORM_BUILDER_SUCCESS_PAGE_TEMPLATES = [
    ("form_builder/success.html", _("Default")),
]
if hasattr(settings, "FORM_BUILDER_SUCCESS_PAGE_TEMPLATES"):
    FORM_BUILDER_SUCCESS_PAGE_TEMPLATES += getattr(
        settings, "FORM_BUILDER_SUCCESS_PAGE_TEMPLATES"
    )

if hasattr(settings, "FORM_BUILDER_RESUBMIT_PAGE_TEMPLATES"):
    FORM_BUILDER_RESUBMIT_PAGE_TEMPLATES += getattr(
        settings, "FORM_BUILDER_RESUBMIT_PAGE_TEMPLATES"
    )

if hasattr(settings, "FORM_BUILDER_PAGE_TEMPLATES"):
    FORM_BUILDER_PAGE_TEMPLATES += getattr(settings, "FORM_BUILDER_PAGE_TEMPLATES")

if hasattr(settings, "FORM_BUILDER_FORM_TEMPLATES"):
    FORM_BUILDER_FORM_TEMPLATES += getattr(settings, "FORM_BUILDER_FORM_TEMPLATES")

# Designated if form models are to be inherited in local app.
FORK = getattr(settings, "FORM_BUILDER_FORK", False)

FIELD_ATTRS = (
    "help_text",
    "max_length",
    "min_length",
    "max_value",
    "min_value",
    "max_digits",
    "decimal_places",
    "min_size",
    "max_size",
)

FORM_FACTORY_MODULE = getattr(
    settings, "FORM_BUILDER_FORM_FACTORY_MODULE", "form_builder.forms.utils"
)

# {'first': {
#     'serializer_class': None, # serializer path of FormEntrySerializer
#                               # replacement. It overrides the rest of the
#                               # keys
#     'validators': [], # path of the validate functions to be attached on
#                       # the serializer instance,
#                       # validate_field_name(self, value)
#     'actions': {
#         'create': 'form_builder.utils.create', # path to create function
#         'update': 'form_builder.utils.create', # Bulk update function
#         'post_create': None, # function to be called after create
#                              # method ends. args FormEntry instance
#         'post_update': None,
#     },
#     'submit': False, # ie. login form does not require a submit action.
#     'redirect_url': None,
#     }
# }

DASHBOARD_SUBMISSION_FILTER_FIELDS = getattr(
    settings, "FORM_BUILDER_DASHBOARD_SUBMISSION_FILTER_FIELDS", []
)

EXTRA_FORM_MODEL_FIELDS = getattr(settings, "FORM_BUILDER_EXTRA_FORM_MODEL_FIELDS", [])

EXTRA_FIELD_MODEL_FIELDS = getattr(
    settings, "FORM_BUILDER_EXTRA_FIELD_MODEL_FIELDS", []
)

# FORM_BUILDER_FORM_TEMPLATES = getattr(settings, 'FORM_BUILDER_FORM_TEMPLATES', ())

FORM_BUILDER_WIDGET_TO_TEMPLATE_MAP = {
    "ChoiceField": "form_builder/fields/checkbox.html",
    "CheckboxSelectMultiple": "form_builder/fields/checkbox_multiple.html",
    "SelectMultiple": "form_builder/fields/select_multiple.html",
    "ClearableFileInput": "form_builder/fields/file_input.html",
    "EmailInput": "form_builder/fields/email.html",
    "DateInput": "form_builder/fields/date.html",
    "DateTimeInput": "form_builder/fields/date_time.html",
    "TextInput": "form_builder/fields/input.html",
    "NumberInput": "form_builder/fields/number.html",
    "RadioSelect": "form_builder/fields/radio.html",
    "Select": "form_builder/fields/select.html",
    "Textarea": "form_builder/fields/textarea.html",
    "CKEditorWidget": "form_builder/fields/richtext.html",
    "ChoiceWithOtherWidget": "form_builder/fields/choice_with_other.html",
    "PasswordInput": "form_builder/fields/password.html",
    "URLInput": "form_builder/fields/url.html",
    "NoReCaptchaWidget": "form_builder/fields/captcha.html",
    "SmileFaceRatingFieldWidget": "form_builder/fields/smile_face_rating.html",
    "StarsRatingFieldWidget": "form_builder/fields/stars_rating.html",
    "MultipleChoiceWithInputWidget": "form_builder/fields/multiple_choice_with_input.html",
    "CustomCheckboxSelectMultiple": "form_builder/fields/checkbox_multiple.html",
    "DynamicChoiceWidget": "form_builder/fields/dynamic_choice.html",
}

FORM_BUILDER_WIDGET_TO_TEMPLATE_MAP_SUBMIT_DETAILS = {
    "RadioSelect": "form_builder/submit_edit_fields/radio.html",
    "MultipleChoiceWithInputWidget": "form_builder/fields/multiple_choice_with_input.html",
}

FORM_BUILDER_PRINT_TEMPLATES = getattr(settings, "FORM_BUILDER_PRINT_TEMPLATES", [])

# FIELD_CLASSES_validation_help = {
#     'CharField': _("Accepy any character with max length as defined in the field attribute"),
#     'EmailField': _("Email format only"),
#     'URLField': _('Accept url format that starts by http://'),
#     'IntegerField': _('Integer number from -2147483648 to 2147483647'),
#     'PhoneNumberField': _('Valid phone number with Country code'),
#     'KwPhoneNumberField': _('Valid Kuwait phone number without Country code'),
#     'DecimalField': _('Decimal number with Max. digits as defined in field attribute'),
#     # (BooleanField, _('Yes/No')),
#     'BooleanField': _(' True or False'),
#     'DateField': _('Date Input from Date widget'),
#     'DateTimeField': _('Date & time input from Datetime widget'),
#     'TimeField': _('Time input from time widget'),
#     'ChoiceField': _('Choice Field'),
#     'MultipleChoiceField': _('Multiple choices Field'),
#     'RegexField': _('Regex'),
#     'FileField': _('File'),
#     'ImageField': _('Image'),
#     'ChoiceWithOtherField': _('Choice field with optional answer'),
#     'KWCivilIDNumberField': _('Valid Kuwait Civil ID'),
#     'CaptchaField': _('Captcha'),
#     'MultipleChoiceWithInputField': _('Multiple choices with input field')
# }

# FORM_TYPE = getattr(settings, 'FORM_BUILDER_EXTRA_FIELD_MODEL_FIELDS',
#                     (
#                         ("general", _("General Form")),
#                         ("survey", _("Survey")),
#                     )
#                     )

FIELD_CLASS_WITH_SINGLE_CHOICE = ["ChoiceField", "ChoiceWithOtherField"]
FIELD_CLASS_WITH_MULTI_CHOICE = ["MultipleChoiceWithInputField", "MultipleChoiceField"]

FIELD_EXTRA_VALIDATOR = [
    ("form_builder.validators.field_validation_example", "Example - Do Nothing"),
]

if hasattr(settings, "FIELD_EXTRA_VALIDATOR"):
    FIELD_EXTRA_VALIDATOR += getattr(settings, "FIELD_EXTRA_VALIDATOR")

FORM_EXTRA_VALIDATOR = [
    ("form_builder.validators.form_validation_example", "Example - Do Nothing"),
]

if hasattr(settings, "FORM_EXTRA_VALIDATOR"):
    FORM_EXTRA_VALIDATOR += getattr(settings, "FORM_EXTRA_VALIDATOR")

WORKFLOW_CUSTOM_SCRIPT = []
if hasattr(settings, "WORKFLOW_CUSTOM_SCRIPT"):
    WORKFLOW_CUSTOM_SCRIPT += getattr(settings, "WORKFLOW_CUSTOM_SCRIPT")

FORM_CUSTOM_ACTION = []
if hasattr(settings, "FORM_CUSTOM_ACTION"):
    FORM_CUSTOM_ACTION += getattr(settings, "FORM_CUSTOM_ACTION")

FORM_CUSTOM_ACTION_AT_CREATION = []
if hasattr(settings, "FORM_CUSTOM_ACTION_AT_CREATION"):
    FORM_CUSTOM_ACTION_AT_CREATION += getattr(
        settings, "FORM_CUSTOM_ACTION_AT_CREATION"
    )

GET_FIELD_CHOICE_DYNAMIC = []
if hasattr(settings, "GET_FIELD_CHOICE_DYNAMIC"):
    GET_FIELD_CHOICE_DYNAMIC += getattr(settings, "GET_FIELD_CHOICE_DYNAMIC")

FORM_IS_AVAILABLE = []
if hasattr(settings, "FORM_IS_AVAILABLE"):
    FORM_IS_AVAILABLE += getattr(settings, "FORM_IS_AVAILABLE")

FORM_BUILDER_NOT_AVAILABLE_PAGE_TEMPLATES = [
    ("form_builder/form_not_available_page.html", _("Default")),
]
if hasattr(settings, "FORM_BUILDER_NOT_AVAILABLE_PAGE_TEMPLATES"):
    FORM_BUILDER_NOT_AVAILABLE_PAGE_TEMPLATES += getattr(
        settings, "FORM_BUILDER_NOT_AVAILABLE_PAGE_TEMPLATES"
    )
