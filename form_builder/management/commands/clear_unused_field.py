from django.core.management.base import BaseCommand, CommandError
from form_builder.models import Field


class Command(BaseCommand):
    help = "Remove the unused Field Settings"

    def handle(self, *args, **options):
        for field in Field.objects.all():
            if not field.formfield.all().exists():
                self.stdout.write(
                    self.style.SUCCESS(
                        'Successfully Deleted Field "%s"' % field.display_name
                    )
                )
                field.delete()
