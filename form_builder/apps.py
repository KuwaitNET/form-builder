from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class RestFormConfig(AppConfig):
    name = "form_builder"
    verbose_name = _("Form Builder")
