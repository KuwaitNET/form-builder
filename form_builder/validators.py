# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import re

from django.forms import ValidationError
from django.utils.translation import ugettext_lazy as _


def validate_regex_expr(value):
    try:
        re.compile(value)
    except Exception as e:
        raise ValidationError(e)
    return value


def form_validation_example(request):
    """Do you validation on the form submission"""
    is_valid = True
    message = ""

    field_name_1 = request.POST.get("field_name_1", None)
    field_name_2 = request.POST.get("field_name_2", None)
    # Add your validation to the form POST and return an error message as well

    return is_valid, message


def field_validation_example(value):
    """Do you validation on the value"""
    is_valid = True

    if not is_valid:
        raise ValidationError(_("This Field is not Valid"))
    return value
