# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.http import Http404

try:
    from django.shortcuts import reverse
except ImportError:
    from django.core.urlresolvers import reverse
from django.db.models import QuerySet, Q
from django.utils.translation import ugettext as _
from django.utils.timezone import now

from rest_framework import serializers

from form_builder import settings as form_settings
from form_builder.models import Form as FormModel

# from form_builder.models.field import FieldModel
# from form_builder.models.entry import FieldEntryModel, FormEntryModel, FormSubmitModel
from form_builder.models import (
    Choice as ChoiceModel,
    Field as FieldModel,
    FieldEntry as FieldEntryModel,
    FormEntry as FormEntryModel,
    FormSubmit as FormSubmitModel,
)


# http://stackoverflow.com/questions/1823898/overload-a-method-with-a-function-at-runtime
# class overloadable(object):
#     def __init__(self, func):
#         print func
#         print '1'
#         self._default_func = func
#         self._name = func.__name__
#
#     def __get__(self, obj, type=None):
#         print '2'
#
#         print self._name
#         def create(obj, b):
#             print 'create'
#         print obj.__dict__
#         func = obj.__dict__.get(self._name, self._default_func)
#         # return types.MethodType(create, obj, type)
#
#     def __set__(self, obj, value):
#         print '3'
#         if hasattr(value, 'im_func'): value = value.im_func
#         obj.__dict__[self._name] = value
#
#     def __delete__(self, obj):
#         print '4'
#         del obj.__dict__[self._name]


class ChoiceSerializer(serializers.ModelSerializer):
    """
    Choices serializer
    """

    class Meta:
        model = ChoiceModel
        fields = ("value",)


class FieldAttributesSerializer(serializers.ModelSerializer):
    class Meta:
        model = FieldModel
        fields = (
            "placeholder_text",
            "initial",
            "required",
            "max_length",
            "min_length",
            "max_value",
            "min_value",
            "max_digits",
            "decimal_places",
        )


class FieldSerializer(serializers.ModelSerializer):
    """
    Field serializer
    """

    choices = serializers.SerializerMethodField()
    attributes = serializers.SerializerMethodField()

    class Meta:
        model = FieldModel
        fields = (
            "id",
            "name",
            "field_class",
            "widget",
            "label",
            "help_text",
            "regex",
            "attributes",
            "choices",
        )

    def get_attributes(self, field):
        return FieldAttributesSerializer(field).data

    def get_choices(self, field):
        return ChoiceSerializer(field.choices.all(), many=True).data


class FieldEntrySerializer(serializers.ModelSerializer):
    """
    Field entry serializer.
    Used only to store plain values.
    For files check FileFieldEntrySerializer
    """

    class Meta:
        model = FieldEntryModel
        fields = ("id", "field", "value")

    def validate(self, attrs):
        field = attrs["field"]
        field_pks = list(
            self.context["form"].fields.active().values_list("id", flat=True)
        )
        if field.pk not in field_pks:
            raise serializers.ValidationError(
                {field.label: _("Field does not belong to this form.")}
            )
        return attrs


class FileFieldEntrySerializer(serializers.ModelSerializer):
    """
    Serializer for saving files.
    """

    field = serializers.PrimaryKeyRelatedField(
        queryset=FieldModel.objects.all(), required=True  # TODO: Update here,
    )

    class Meta:
        model = FieldEntryModel
        fields = ("id", "field", "value", "file")

    # TODO: validate file upload against admin attrs


class FormEntrySerializer(serializers.ModelSerializer):
    field_entries = FieldEntrySerializer(required=True, many=True)
    files = serializers.ListField(required=False)

    class Meta:
        model = FormEntryModel
        fields = (
            "id",
            "token",
            "submitter",
            "field_entries",
            "files",
        )

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        kwargs.pop("fields", None)
        super(FormEntrySerializer, self).__init__(*args, **kwargs)
        # TODO: check required fields
        self.fields["files"].required = self.context["form"].has_required_files()
        self.form = self.context["form"]
        # replace and attach the defined methods for this form instance
        for method_name, func in self.form.opts.actions.items():
            setattr(self, method_name, func)

    def validate_files(self, values):
        file_field_ids = self.form.get_fields().files().values_list("id", flat=True)

        # exclude from the beginning wrong IDs
        file_entries = FieldEntryModel.objects.filter(
            id__in=values, field__id__in=list(file_field_ids)
        ).select_related("field")

        # check if files field is empty or not against the required fields
        if not all([file_entries.required().exists(), file_entries.exists()]):
            raise serializers.ValidationError(_("File IDs are missing."))

        # check if uploaded ids are part of required fields
        if not set(file_field_ids.required()).issubset(
            file_entries.required().values_list("field__id", flat=True)
        ):
            raise serializers.ValidationError(_("Required file IDs are missing."))

        return file_entries

    def validate_field_entries(self, raw_field_entries):
        fields = {field["field"].name: field["value"] for field in raw_field_entries}
        serializer_class = serializer_factory(
            self.form, funcs=self.form.opts.validators
        )
        serializer = serializer_class(data=fields)
        serializer.is_valid(raise_exception=True)
        field_entries = [FieldEntryModel(**data) for data in raw_field_entries]
        return field_entries

    def create(self, validated_data):
        form = self.form

        field_entries = validated_data.get("field_entries")
        file_entries = validated_data.get("files", None)

        request = self.context.get("request")

        entry = FormEntryModel.objects.create_for_form(
            form=self.form,
            submitter=request.user if request.user.is_authenticated else None,
            token=request.get("token"),
            request=self.context["request"],
        )
        [setattr(f, "entry", entry) for f in field_entries]
        FieldEntryModel.objects.bulk_create(field_entries)

        # attach the files
        if file_entries is not None:
            file_entries.update(entry=entry)

        # if the form is not wizard, submit it to the admin.
        if form.opts.submit or entry.can_be_submitted():
            self.submit(entry.all_entries)

        return self.post_create(entry, validated_data)

    def post_create(self, instance, validated_data):
        self.response_payload = {
            "redirect_url": self.form.get_redirect_url(
                submitter=instance.submitter, token=instance.token
            ),
            "success_message": self.form.success_message,
        }
        return instance

    def update(self, instance, validated_data):
        form_entry = FormEntryModel.objects.get(
            submitter=self.context.get("request").user,
            token=self.context.get("request").data.get("token"),
            form=instance,
        )

        for entry in validated_data.get("field_entries"):
            FieldEntryModel.objects.filter(entry=form_entry, field=entry.field,).update(
                value=entry.value
            )

        # TODO: add updates for files

        return self.post_update(instance, form_entry, validated_data)

    def post_update(self, instance, form_entry, validated_data):
        self.response_payload = {
            "redirect_url": self.form.get_redirect_url(
                submitter=form_entry.submitter, token=form_entry.token
            ),
            "success_message": self.form.success_message,
        }
        return instance

    def submit(self, entries):
        submit = FormSubmitModel.objects.create()
        entries.update(form_submit=submit)
        submit.submit(self.context["request"])


class FormDefinitionSerializer(serializers.ModelSerializer):
    """
    Form serializer
    """

    action_url = serializers.SerializerMethodField()
    file_upload_url = serializers.SerializerMethodField()
    token = serializers.SerializerMethodField()
    method = serializers.SerializerMethodField()

    class Meta:
        model = FormModel
        fields = (
            "id",
            "name",
            "token",
            "action_url",
            "file_upload_url",
            "method",
            "fields",
        )

    def get_action_url(self, obj):
        if self.context.get("method", "POST") == "POST":
            return reverse(form_settings.ACTION_VIEW, args=(obj.url_path,))
        return reverse("form_builder:api:update", args=(obj.url_path,))

    def get_file_upload_url(self, obj):
        # return reverse(form_settings.FILE_UPLOAD_VIEW, args=(obj.url_path,))
        return reverse("{}-list".format(form_settings.FILE_UPLOAD_VIEW))

    def get_token(self, obj):
        return self.context.get("token")

    def get_method(self, obj):
        return self.context.get("method", "POST")
