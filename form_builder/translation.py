from modeltranslation.translator import translator, TranslationOptions

from .models import Form, Field, Choice, Workflow, FormType, FormField


class FormTranslation(TranslationOptions):
    fields = ("name", "description", "success_message", "resubmission_intro")


class FieldTranslation(TranslationOptions):
    fields = (
        "display_name",
        "help_text",
        "placeholder_text",
        "initial",
        "max_date_message",
        "min_date_message",
        "above_field_paragraph",
        "below_field_paragraph",
    )


class ChoiceTranslation(TranslationOptions):
    fields = ("value",)


class WorkflowTranslation(TranslationOptions):
    fields = ("name", "state")


class FormTypeTranslation(TranslationOptions):
    fields = ("name",)


class FormFieldTranslation(TranslationOptions):
    fields = ("label",)


translator.register(Form, FormTranslation)
translator.register(Field, FieldTranslation)
translator.register(Choice, ChoiceTranslation)
translator.register(Workflow, WorkflowTranslation)
translator.register(FormType, FormTypeTranslation)
translator.register(FormField, FormFieldTranslation)
