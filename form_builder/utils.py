# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import re
import uuid
import sys
from importlib import import_module

import django
from django.apps import apps

try:
    from django.utils.functional import keep_lazy as allow_lazy
except ImportError:
    from django.utils.functional import allow_lazy

from django.utils.module_loading import import_string
from django.utils.safestring import mark_safe

try:
    from django.utils.encoding import force_text
except ImportError:
    from django.utils.text import force_text
try:
    from django.shortcuts import reverse
except ImportError:
    from django.core.urlresolvers import reverse

from django.contrib.admin.views.main import ChangeList
from django.contrib.admin.exceptions import DisallowedModelAdminLookup
from django.contrib.admin import FieldListFilter
from django.contrib.admin.utils import (
    get_fields_from_path,
    lookup_needs_distinct,
    prepare_lookup_value,
)
from django.db import models
from django.utils import six
from django.contrib.admin.options import IncorrectLookupParameters
from django.core.exceptions import FieldDoesNotExist
import re


from form_builder import settings as form_settings


def unique_upload(instance, filename):
    ext = filename.split(".").pop()
    return "{}.{}".format(uuid.uuid4(), ext)


def get_upload_path(instance, filename):
    return import_string(form_settings.UPLOAD_FUNCTION)(instance, filename)


def get_class(class_path):
    """
    dynamically get a class from a string
    """
    class_data = class_path.split(".")
    module_path = ".".join(class_data[:-1])
    class_str = class_data[-1]
    module = import_module(module_path)
    # Finally, we retrieve the Class
    return getattr(module, class_str)


def snake_case(value):
    """
    Converts spaces to underscore. Removes characters that
    aren't alphanumerics, underscores, or hyphens. Converts to lowercase.
    Also strips leading and trailing whitespace.
    """
    value = force_text(value)
    value = re.sub("[^\w\s-]", "", value).strip().lower()
    return mark_safe(re.sub("[-\s]+", "_", value))


snake_case = allow_lazy(snake_case, str)


def get_submitter(request):
    submitter = request.user
    if not user_is_authenticated(request.user):
        submitter = None
    return submitter


def user_is_authenticated(user):
    if django.VERSION >= (1, 10):
        return user.is_authenticated
    else:
        return user.is_authenticated()


def reverse_no_i18n(viewname, *args, **kwargs):
    result = reverse(viewname, *args, **kwargs)
    m = re.match(r"(/[^/]*)(/.*$)", result)
    return m.groups()[1]


class MyChangeList(ChangeList):  # show filter even if it has only one choice
    def get_filters(self, request):
        lookup_params = self.get_filters_params()
        use_distinct = False

        for key, value in lookup_params.items():
            if not self.model_admin.lookup_allowed(key, value):
                raise DisallowedModelAdminLookup("Filtering by %s not allowed" % key)

        filter_specs = []
        if self.list_filter:
            for list_filter in self.list_filter:
                if callable(list_filter):
                    # This is simply a custom list filter class.
                    spec = list_filter(
                        request, lookup_params, self.model, self.model_admin
                    )
                else:
                    field_path = None
                    if isinstance(list_filter, (tuple, list)):
                        # This is a custom FieldListFilter class for a given field.
                        field, field_list_filter_class = list_filter
                    else:
                        # This is simply a field name, so use the default
                        # FieldListFilter class that has been registered for
                        # the type of the given field.
                        field, field_list_filter_class = (
                            list_filter,
                            FieldListFilter.create,
                        )
                    if not isinstance(field, models.Field):
                        field_path = field
                        field = get_fields_from_path(self.model, field_path)[-1]
                    spec = field_list_filter_class(
                        field,
                        request,
                        lookup_params,
                        self.model,
                        self.model_admin,
                        field_path=field_path,
                    )
                    # Check if we need to use distinct()
                    use_distinct = use_distinct or lookup_needs_distinct(
                        self.lookup_opts, field_path
                    )
                # if spec and spec.has_output():
                filter_specs.append(spec)
        # At this point, all the parameters used by the various ListFilters
        # have been removed from lookup_params, which now only contains other
        # parameters passed via the query string. We now loop through the
        # remaining parameters both to ensure that all the parameters are valid
        # fields and to determine if at least one of them needs distinct(). If
        # the lookup parameters aren't real fields, then bail out.
        try:
            for key, value in lookup_params.items():
                lookup_params[key] = prepare_lookup_value(key, value)
                use_distinct = use_distinct or lookup_needs_distinct(
                    self.lookup_opts, key
                )
            return filter_specs, bool(filter_specs), lookup_params, use_distinct
        except FieldDoesNotExist as e:
            six.reraise(
                IncorrectLookupParameters,
                IncorrectLookupParameters(e),
                sys.exc_info()[2],
            )


def smart_truncate(content, length=100, suffix="..."):
    if len(content) <= length:
        return content
    else:
        return " ".join(content[: length + 1].split(" ")[0:-1]) + suffix


def send_mail(
    subject,
    message,
    message_html,
    from_email,
    recipient_list,
    attachments=None,
    priority=None,
    fail_silently=False,
    auth_user=None,
    auth_password=None,
    headers={},
):
    """
    Function to queue HTML e-mails
    """
    from django.utils.encoding import force_text
    from django.core.mail import EmailMultiAlternatives
    from django.conf import settings

    if "mailer" in settings.INSTALLED_APPS:
        from mailer import get_priority
        from mailer.models import make_message

        priority = get_priority(priority)

        # need to do this in case subject used lazy version of ugettext
        subject = force_text(subject)
        message = force_text(message)

        msg = make_message(
            subject=subject,
            body=message,
            from_email=from_email,
            to=recipient_list,
            priority=priority,
        )
        email = msg.email
        email = EmailMultiAlternatives(
            email.subject, email.body, email.from_email, email.to, headers=headers
        )
        email.attach_alternative(message_html, "text/html")
        if attachments:
            for attachment in attachments:
                email.attach(filename=attachment[0], content=attachment[1])
        msg.email = email
        msg.save()
        return 1
    else:
        from django.core.mail import get_connection

        connection = get_connection(
            username=auth_user, password=auth_password, fail_silently=fail_silently,
        )
        mail = EmailMultiAlternatives(
            subject, message, from_email, recipient_list, connection=connection
        )
        if message_html:
            mail.attach_alternative(message_html, "text/html")

        if attachments:
            for attachment in attachments:
                mail.attach(filename=attachment[0], content=attachment[1])

        return mail.send()
