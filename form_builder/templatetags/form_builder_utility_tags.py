import os
import uuid
import time

from django import template
from django.shortcuts import reverse
from django.urls import resolve, Resolver404

# from django.template.defaultfilters import removetags
from django.utils.safestring import mark_safe
from django.utils.translation import get_language, activate
from django.conf import settings

try:
    from django.utils.datastructures import OrderedDict
except ImportError:
    from collections import OrderedDict
from django.utils.timezone import now

register = template.Library()


@register.simple_tag
def order_models_list(models, app_label):
    order = OrderedDict(settings.ADMIN_REORDER)
    order_index = []
    unorder_index = []
    if app_label in list(order):
        for model in models:
            if model["object_name"] in list(order[app_label]):
                order_index.append(model)
            else:
                unorder_index.append(model)
        order_index.sort(key=lambda x: order[app_label].index(x["object_name"]))
        return order_index + unorder_index
    return models


@register.simple_tag(takes_context=True)
def change_lang(context, lang=None, *args, **kwargs):
    """
    Get active page's url by a specified language
    Usage: {% change_lang 'en' %}
    """
    path = context["request"].path
    full_path = context["request"].get_full_path()
    try:
        url_parts = resolve(path)
        cur_language = get_language()
        try:
            activate(lang)
            url = reverse(url_parts.view_name, kwargs=url_parts.kwargs)
            activate(cur_language)
            parameters = (
                "?{0}".format(full_path.split("?")[1])
                if len(full_path.split("?")) == 2
                else ""
            )
            return "{0}{1}".format(url, parameters)
        except Exception:
            pass
    except Resolver404:
        pass
    return full_path


@register.simple_tag
def get_filesize_extension(file):
    if file:
        filename, file_extension = os.path.splitext(file.url)
        file_extention_stripped = file_extension.split("?", 1)[0]
        file_extention_stripped = file_extention_stripped[1:]
        try:
            size = file.file.size
        except IOError:
            size = None
        return {"size": size, "extension": file_extention_stripped}
    return None


@register.simple_tag
def get_verbose_field_name(instance, field_name):
    """
    Returns verbose_name for a field.
    """
    return instance._meta.get_field(field_name).verbose_name.title()


@register.filter
def split_filename(filename):
    if filename:
        return filename.split("/")[-1]


@register.simple_tag
def check_applied_vacancy(vacancy, user):
    status = False
    for user_application in user.career_profile.career_applications.all():
        if user_application.vacancy.pk == vacancy.pk:
            status = True
            break
    return status


@register.simple_tag
def get_ancestors_ids(current_page):
    if current_page:
        return current_page.get_ancestors().values_list("id", flat=True)
    return []


@register.simple_tag
def get_score_and_note(field_value):
    if field_value:
        score, note = field_value.split("~")
        return {"score": int(score), "note": note}


@register.simple_tag(name="cache_bust")
def cache_bust():
    if not settings.DEBUG:
        version = uuid.uuid1()
    else:
        version = os.environ.get("PROJECT_VERSION")
        if version is None:
            version = "1"

    return "__v__={version}".format(version=version)


@register.simple_tag
def get_version_param(version=None):
    if getattr(settings, "AWS_S3_CUSTOM_DOMAIN", None):
        delimiter = "?"
    else:
        delimiter = "&"

    if version is None:
        version = time.time()

    return mark_safe(u"{}v={}".format(delimiter, version))


@register.filter
def subtract(value, arg):
    return value - arg


@register.filter
def add(value, arg):
    return value + arg


@register.simple_tag
def update_variable(value):
    """Allows to update existing variable in template"""
    return value


@register.simple_tag
def template_dir(this_object, its_name=""):
    if settings.DEBUG:
        output = dir(this_object)
        return "<pre>" + str(its_name) + " " + str(output) + "</pre>"
    return ""


@register.simple_tag
def template_type(this_object, its_name=""):
    if settings.DEBUG:
        output = type(this_object)
        return "<pre>" + str(its_name) + " " + str(output) + "</pre>"
    return ""


@register.filter
def get_attr(value, arg):
    return getattr(value, arg, None)


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)
