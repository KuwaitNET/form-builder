# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os, copy

from django import template
from django.apps import apps
from django.conf import settings

from form_builder.models import FieldEntry, Form, ReSubmitFields, PDFForForm
from form_builder.utils import get_submitter
from form_builder.settings import (
    FORM_BUILDER_WIDGET_TO_TEMPLATE_MAP,
    FORM_BUILDER_WIDGET_TO_TEMPLATE_MAP_SUBMIT_DETAILS,
)


register = template.Library()


@register.simple_tag
def get_template_for_field(field):
    try:
        widget_name = field.field.widget.__class__.__name__
        template_name = FORM_BUILDER_WIDGET_TO_TEMPLATE_MAP[widget_name]
    except KeyError:
        template_name = "form_builder/fields/dummy.html"
    return template_name


@register.simple_tag
def get_template_for_field_submit_detail(field):
    widget_name = field.field.widget.__class__.__name__
    try:
        template_name = FORM_BUILDER_WIDGET_TO_TEMPLATE_MAP_SUBMIT_DETAILS[widget_name]
    except KeyError:
        template_name = "form_builder/submit_edit_fields/dummy.html"
    return template_name


@register.simple_tag
def get_file_url(
    field, request, object
):  # todo we already get rid of this, should be removed
    if isinstance(object, Form):
        AllField = FieldEntry.objects.filter(
            submitter=get_submitter(request), form=object
        )
        for file in AllField:
            if field.html_name == file.field_name:
                fileName, fileExtension = os.path.splitext(file.file.name)
                return {"file_name": fileName, "file_extension": fileExtension}


@register.simple_tag
def get_filled_form_id(user_entries):
    return [f.form.id for f in user_entries]


@register.simple_tag
def is_formbuilder_form(object):
    if isinstance(object, Form):
        return True
    return False


@register.simple_tag
def get_form_object(form, token, request):
    from form_builder.forms import form_factory

    form = form_factory(form, token=token, request=request, is_formset=False,)
    return form


@register.simple_tag
def field_is_resubmit(form_id, field_name, submit_id, field_entry):
    return ReSubmitFields.objects.filter(
        submission__id=submit_id,
        form__id=form_id,
        form_field__slug=field_name,
        field_entry__id=field_entry,
    ).exists()


@register.filter
def get_attr(value, arg):
    return getattr(value, arg, None)


@register.simple_tag
def add_field_entry_to_field_name(field, field_entry_id):
    if field_entry_id:
        field.html_name = field.html_name + "_resubmitprefix_" + str(field_entry_id)
    return field


@register.simple_tag
def add_entry_to_field_name(field, entry):
    if entry:
        field.html_name = field.html_name + "_resubmitentryprefix_" + str(entry.id)
    return field


@register.simple_tag
def has_workflow_permission(request, workflow):
    if request.user.is_superuser or (
        not workflow.apply_permission and not workflow.get_root().apply_permission
    ):
        return True
    else:
        if request.user in workflow.permitted_users.all():
            return True
        users_group = request.user.groups.all()
        for a_group in workflow.permitted_groups.all():
            if a_group in users_group:
                return True
    return False


@register.filter
def subtract(value, arg):
    return value - arg


@register.filter
def add(value, arg):
    return value + arg


@register.simple_tag
def update_variable(value):
    """Allows to update existing variable in template"""
    return value


@register.simple_tag
def get_field_widget_for_field(field):
    widget_name = field.field.widget.__class__.__name__
    return widget_name


@register.simple_tag
def get_field_profile_initial(field, user):
    if hasattr(user, "profile"):
        if field.name == "civil_id":
            return user.profile.kuwait_civil_id

        if field.name == "full_name":
            return "{} {}".format(user.profile.first_name, user.profile.last_name)

        if field.name == "mobile":
            return user.profile.mobile

        if field.name == "dob":
            return user.profile.dob

    return None


@register.simple_tag
def is_field_hidden_by_default(form, field):
    if (
        hasattr(form, "fields")
        and form.fields.filter(slug=field.name, is_active=True).exists()
    ):  # means it is not a formset
        return (
            form.fields.filter(slug=field.name, is_active=True)
            .first()
            .hidden_by_default
        )
    else:
        # for a_form in form.get_children():
        #     if a_form.fields.filter(form_field__slug=field.name, is_active=True).exists():
        #         return a_form.fields.filter(form_field__slug=field.name, is_active=True).first().hidden_by_default

        return False  # Feature of showing dependent fields doesn't supported in formset yet


@register.simple_tag
def is_field_unique_form_tree(form, field):
    if (
        hasattr(form, "fields")
        and form.fields.filter(slug=field.name, is_active=True).exists()
    ):  # means it is not a formset
        return (
            form.fields.filter(slug=field.name, is_active=True).first().unique_form_tree
        )
    else:
        # for a_form in form.get_children():
        #     if a_form.fields.filter(field__name=field.name, is_active=True).exists():
        #         return a_form.fields.filter(field__name=field.name, is_active=True).first().hidden_by_default

        return False  # Feature of showing dependent fields doesn't supported in formset yet


@register.simple_tag
def get_dependent_choices(form, field):
    Choice = apps.get_model("form_builder", "Choice")
    choices = []
    for a_choice in Choice.objects.filter(trigger_field__slug=field.name):
        choices.append(str(a_choice.id))

    return " ".join(choices)


@register.simple_tag
def template_dir(this_object, its_name=""):
    if settings.DEBUG:
        output = dir(this_object)
        return "<pre>" + str(its_name) + " " + str(output) + "</pre>"
    return ""


@register.simple_tag
def template_type(this_object, its_name=""):
    if settings.DEBUG:
        output = type(this_object)
        return "<pre>" + str(its_name) + " " + str(output) + "</pre>"
    return ""


@register.simple_tag
def get_files_for_step(form):
    return PDFForForm.objects.filter(form=form)


@register.filter
def get_filename(value):
    if value and type(value) == str:
        return value.split("/")[-1].split("?")[0]
    return value
