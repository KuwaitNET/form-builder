import uuid

from django import template
from django.utils.translation import ugettext_lazy as _
from form_builder.models import FormEntry, Form
from form_builder.models import ConditionalRedirect
from django.utils.html import format_html

register = template.Library()


@register.simple_tag
def get_previous_url(request, token, form):
    if type(form) != Form:
        return
    if form.is_root():
        return
    # Reverse check for conditional redirects, tries to find previous form
    redirects = ConditionalRedirect.objects.filter(redirect_to=form)
    if not token:
        token = uuid.uuid4()
    if redirects:
        for redirect in redirects:
            try:
                form_entry = FormEntry.objects.get(
                    submitter=request, token=token, form=redirect.form
                )
            except FormEntry.DoesNotExist:
                pass
            else:
                redirect_to = (
                    redirect.redirect_to
                    if redirect.force_redirect
                    else redirect.check_fields_conditions(
                        form_entry.get_fields_to_dict()
                    )
                )
                if redirect_to == form:
                    return redirect.form.get_absolute_url()
    # Otherwise, returns either previous sibling or parent form
    prev = form.get_prev_sibling()
    if prev:
        return prev.get_absolute_url()
    else:
        prev = form.get_parent()
        if prev:
            return prev.get_absolute_url()
    return


@register.simple_tag
def get_value_by_pk(dict_instance, pk, index):
    """
    Returns verbose_name for a field.
    """
    return format_html(dict_instance[pk][index])


@register.filter("get_value_from_dict")
def get_value_from_dict(dict_data, key):
    """
    usage example {{ your_dict|get_value_from_dict:your_key }}
    """
    if key and dict_data and key in dict_data:
        return dict_data.get(key) if key in dict_data else _("N/A")
