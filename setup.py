#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from setuptools import setup, find_packages


def read(fname):
    path = os.path.join(os.path.dirname(__file__), fname)
    try:
        file = open(path, encoding='utf-8')
    except TypeError:
        file = open(path)
    return file.read()


CLASSIFIERS = [
    'Environment :: Web Environment',
    'Intended Audience :: Developers',
    'License :: OSI Approved :: GNU General Public License (GPL)'
    'Development Status :: 2 - Pre-Alpha',
    'Operating System :: OS Independent',
    'Topic :: Software Development :: Libraries',
    'Topic :: Internet :: WWW/HTTP :: Dynamic Content'
    'Programming Language :: Python',
]


def get_install_requires():
    install_requires = [
        'django',
        'djangorestframework',
        # # Important due to emtpy request.data https://github.com/tomchristie/django-rest-framework/issues/3951
        'django-treebeard',
        # 'django-simple-captcha',
        'django-ipware',
        'django-jsonfield',
        # 'six>=1.10.0,<1.20',
        'django-admin-sortable',
        'django-autoslug',
        'django-ckeditor',
        # 'django-fsm',
        # 'django-fsm-admin',
        # 'django-db-mailer',
        # 'django-material',
        # 'django-nocaptcha-recaptcha',
        'django-wkhtmltopdf',
        # "django-post_office",
        "django-extensions",
        'django-polymorphic',
        'django-import-export',
        'django-phonenumber-field',
        'phonenumberslite',
        'django-modeltranslation',
        'django-filter',
        'django-sekizai',
        'django-inline-actions',
        'django-localflavor',
        'django_compressor',
        'plotly',


        'django-cms',

    ]
    return install_requires


setup(
    author='Dacian Popute',
    author_email='dacian@kuwaitnet.com',
    name='form_builder',
    version=__import__('form_builder').__version__,
    description='Dynamic django forms through REST API.',
    platforms=['OS Independent'],
    classifiers=CLASSIFIERS,
    long_description=read('README.rst'),
    url='https://bitbucket.org/KuwaitNET/rest_form',
    packages=find_packages(exclude=['example', 'docs', 'tests*']),
    zip_safe=False,
    include_package_data=True,
    install_requires=get_install_requires()
)
