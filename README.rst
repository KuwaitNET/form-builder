Form Builder
===========

This is for Form Builder library developed by Kuwaitnet

Installation
------------
* Download and install latest version of form-builder::

    pip installpip install -e git+https://bitbucket.org/KuwaitNET/form-builder.git@master#egg=form_builder

* Download and install latest version of kn-django-jet::

    pip installpip install -e git+https://bitbucket.org/KuwaitNET/django-jet-kn.git@master#egg=jet


* Add the following to the INSTALLED_APPS setting of your Django project settings.py file (note it should be before 'django.contrib.admin and after auth application django.contrib.auth or custom User app')::

    ...
    'django.contrib.sites',
    <User App>
    ...
    'modeltranslation',
    'django_extensions',
    'solo',
    'jet',
    'jet.jet_dashboard',
    "django.contrib.admin",
    'import_export',
    'form_builder',
    'sekizai',
    'inline_actions',
    'ckeditor',
    'treebeard',
    'adminsortable',
    "compressor",
    'localflavor',

* Add sekizai.context_processors.sekizai to your TEMPLATES['OPTIONS']['context_processors'] setting and use django.template.RequestContext when rendering your templates.

* In the template base.html, remember to add the following::

    {% load static i18n sekizai_tags %}
    {% render_block "css" %} After main css block
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    {% render_block "js" %} After main JS block

* In setting.py, make sure we have the following setting::

    CSRF_COOKIE_HTTPONLY = False


* For installing Django CMS by hand, please follow the guide in the following link:


* Remember to configure your Language settings::

    LANGUAGE_CODE = 'en'
    LANGUAGES = (
        ('en', 'en'),
        ('ar', 'ar'),
    )

* In urls.py, add the following after path('admin/', admin.site.urls),::

    path(r'^jet/', include('jet.urls', 'jet')),  # Django JET URLS
    path(r'^jet/dashboard/', include('jet.jet_dashboard.urls', 'jet-dashboard')),  # Django JET dashboard URLS
    path(r'^form_builder/', include(('form_builder.urls', 'form_builder'), namespace='form_builder')),

